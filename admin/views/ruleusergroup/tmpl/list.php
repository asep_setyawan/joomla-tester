<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior
JHtml::_('behavior.tooltip');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
Joomla.submitbutton = function(pressbutton) {
	if(pressbutton=='ruleusergroup.create'){
		window.location.href = "index.php?option=com_awardpackage&view=ruleusergroup&command=1&package_id=<?php echo $this->package_id; ?>&var_id=<?php echo $this->max_var_id; ?>";
	/*##### Do Other Things What ever you Want#########*/
	}else{
		document.adminForm.task.value=pressbutton;
		submitform(pressbutton);
	}
}
	 
</script>

<div class="span3">
	<div id="sidebar">
		<div class="sidebar-nav">
			<ul class="nav nav-list" id="submenu">
				<li><a href="index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id=<?php echo JRequest::getVar('package_id'); ?>">Free giftcode rewards list</a></li>
				<li  class="active"><a href="index.php?option=com_awardpackage&controller=ruleusergroup&task=list&package_id=<?php echo JRequest::getVar('package_id'); ?>">Free giftcode user groups list</a></li>
				<li><a href="index.php?option=com_awardpackage&controller=giftcoderule&package_id=<?php echo JRequest::getVar('package_id'); ?>">Giftcode rules list</a></li>

			</ul>
		</div>
	</div>
</div>

<div id="j-main-container" class="span8">
<form method="post" action="<?php echo JRoute::_('index.php?option=com_awardpackage&controller=ruleusergroup.delete&package_id='.JRequest::getVar('package_id'));?>" name="adminForm" id="adminForm">
<table align="center" border="0" class="table table-striped" width="70%">
	<thead>
		<tr style="text-align:center; background-color:#CCCCCC">
			<!-- <th><input type="checkbox" name="toggle" value=""
				onclick="checkAll(<?php echo count($this->items); ?>);" /></th> -->
			<th width="1%" class="hidden-phone"><?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th><?php echo JText::_('Free giftcode user group');?></th>
			<th><?php echo JText::_('Created');?></th>
			<th><?php echo JText::_('Modified');?></th>

		</tr>
	</thead>
	<?php
	foreach($this->usergroups as $i => $item):
		$rows = $this->ug->get_group_var_id($item->id);
		$var_id = $rows[0]->var_id;
	?>
	<tr class="row<?php echo $i % 2; ?>">
		<td align="center"><input type="hidden"
			value="<?php echo $item->id;?>" name="setting_id[]"> <?php echo JHtml::_('grid.id', $i, $item->id); ?>
		</td>
		<td align="center"><a href="<?php echo JRoute::_('index.php?option=com_awardpackage&controller=ruleusergroup&command=1&group_id='.$item->id.'&package_id='.$item->package_id.'&var_id='.$var_id); ?>"><?php echo $item->title;?></a></td>

		<td align="center"><?php echo $item->created;?></td>
		<td align="center"><?php echo $item->modified;?></td>

		</td>
	</tr>
	<?php endforeach; ?>
</table>
<div><input type="hidden" id="task" name="task" value="ruleusergroup.delete" /> 
	<input type="hidden" id="controller" name="controller" value="ruleusergroup" /> 
	<input type="hidden" id="boxchecked" name="boxchecked" value="0" /> 
	<?php echo JHtml::_('form.token'); ?>
</div>
</form>
</div>
