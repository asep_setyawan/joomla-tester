<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class awardpackageViewGiftcoderulerewardslist extends JViewLegacy {
    
    function __construct($config = array()) {
      
        parent::__construct($config);
        
        $this->model = & JModelLegacy::getInstance('Main', 'AwardpackageModel');
        
        $this->package_id = JRequest::getVar('package_id');
        
    }
    function display($tpl = null) {
        // Load the submenu.
        //AwardpackagesHelper::addSubmenu(JRequest::getCmd('view', 'referral'), 'donation');
		
		$document= &JFactory::getDocument();
		JHtml::_('bootstrap.framework');
		$document->addStyleSheet(JURI::base(true).'/components/com_awardpackage/assets/css/bootstrap-datepicker.min.css');
		$document->addStyleSheet(JURI::base(true).'/components/com_awardpackage/assets/css/custom.css');
		$document->addScript(JURI::base(true).'/components/com_awardpackage/assets/js/bootstrap-datepicker.min.js');
		
		$pagination = $this->get('Pagination');

		$task = JRequest::getVar('task');
		
		$model = $this->getModel('giftcoderulerewardslist');
								$get_all_rewards=$model->get_all_free_gift_rewards_list($this->package_id);								$this->assignRef('get_all_rewards',$get_all_rewards);
				$rules = $model->get_rules($this->package_id);
			
		$this->assignRef('rules',$rules);
		
		$freeUserGroups = $model->get_free_usergroups($this->package_id);
		
		$this->assignRef('freeUserGroups',$freeUserGroups);
		//echo $task;die;
		if ($task =='create') {
			JToolBarHelper::title('Award Category - Free Giftcode User Group List');
			$this->addToolbarForCreatePage();
			$this->setLayout('create');
		}
		else if($task =='createcal'){
			JToolBarHelper::title('Award Category - New Giftcode Rule');
			$this->addToolbarForCreatePage();
			$this->setLayout('createcalendar');			
		}
		else if($task =='selectgiftcode'){
			JToolBarHelper::title('Award Category - Add Gift code');
			$this->addToolbarForGiftCodePage();
			$this->setLayout('giftcode');			
		}else if($task =='giftcoderulerewardslist.delete')
		{						
			$model->delete($_POST);			
			JToolBarHelper::title('Award Category - Free Giftcode User Group List');			
			$this->addToolbarForListPage();				
		}
		else{			
			$model->update_reward_status_to_expire();
			$this->addToolbarForListPage();
			JToolBarHelper::title('Award Category - Free Giftcode User Group List');
		}

		$this->pagination = $pagination;


        parent::display($tpl);
    }

	protected function addToolbarForListPage()
	{
		// assuming you have other toolbar buttons ...
 
		JToolBarHelper::custom('giftcoderulerewardslist.create', 'new.png', 'new.png', 'Add', false,false);
		
		JToolBarHelper::custom('giftcoderulerewardslist.delete', 'delete.png', 'delete.png', 'Delete', false);
	}
	
	protected function addToolbarForGiftCodePage()
	{
		// assuming you have other toolbar buttons ...
 
		JToolBarHelper::custom('giftcoderulerewardslist.save', 'save.png', 'save.png', 'Save & Close', false,false);
		JToolBarHelper::custom('giftcoderulerewardslist.back', 'delete.png', 'delete.png', 'Back', true);
 
	}
	
	protected function addToolbarForCreatePage()
	{
		// assuming you have other toolbar buttons ...
		JToolBarHelper::custom('giftcoderule.create', 'new.png', 'new.png', 'Add', false,false);
		JToolBarHelper::custom('giftcoderule.delete', 'delete.png', 'delete.png', 'Delete', true);
		JToolBarHelper::custom('giftcoderule.save', 'save.png', 'save.png', 'Save & Close', false,false);
		JToolBarHelper::custom('giftcoderule.back', 'delete.png', 'delete.png', 'Back', true);
 
	}

}
