<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior
JHtml::_('behavior.tooltip');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
Joomla.submitbutton = function(pressbutton) {
	if(pressbutton=='giftcoderulerewardslist.create'){		
	var start_date=jQuery('#start_date').val();		
	var end_date=jQuery('#end_date').val();		
	var group_id=jQuery('#group_id').val();		
	var rule_id=jQuery('#rule_id').val();		
		if(start_date=="")		
		{				
			alert("Please Select Start Date.");		
		}		
		else if(end_date=="")		
		{				
			alert("Please Select End Date.");		
		}		
		else if(group_id=="")		
		{				
			alert("Please Select A User Group.");		
		}		
		else if(rule_id=="")		
		{				
			alert("Please Select A Gift Code Rule.");		
		}		
		else{				
			jQuery('#create_award_list_form').submit();		
		}
	}else{
		
		jQuery('#adminForm').submit();		
	}
}


	 
</script>

<div class="span3">
	<div id="sidebar">
		<div class="sidebar-nav">
			<ul class="nav nav-list" id="submenu">
				<li  class="active"><a href="index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id=<?php echo JRequest::getVar('package_id'); ?>">Free giftcode rewards list</a></li>
				<li><a href="index.php?option=com_awardpackage&controller=ruleusergroup&task=list&package_id=<?php echo JRequest::getVar('package_id'); ?>">Free giftcode user group list</a></li>
				<li><a href="index.php?option=com_awardpackage&controller=giftcoderule&package_id=<?php echo JRequest::getVar('package_id'); ?>">Giftcode rules list</a></li>

			</ul>
		</div>
	</div>
</div>







<div id="j-main-container" class="span8"><form method="post" action="<?php echo JRoute::_('index.php?option=com_awardpackage&controller=giftcoderulerewardslist.create&package_id='.JRequest::getVar('package_id'));?>" name="adminForm" id="create_award_list_form">
<input type="hidden" name="package_id" value="<?php echo JRequest::getVar('package_id');?>"><div id="top_table">
	<table align="center" border="0" class="table table-striped" width="70%">
		<thead>
			<tr style="text-align:center; background-color:#CCCCCC">
				<th><?php echo JText::_('Start Date');?></th>
				<th><?php echo JText::_('End Date');?></th>
				<th><?php echo JText::_('Free giftcode user group');?></th>
				<th><?php echo JText::_('Giftcode rules');?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td align="center"><a id="start" href="#" data-toggle="modal" data-target="#date_modal">New</a>					<input type="hidden" name="start_date" id="start_date" >				</td>
				<td align="center"><a id="end" href="#" data-toggle="modal" data-target="#date_modal1">New</a>					<input type="hidden" name="end_date" id="end_date" >				</td>
				<td align="center"><a id="group" href="#" data-toggle="modal" data-target="#date_modal3">New</a>					<input type="hidden" name="group_id" id="group_id" >				</td>
				<td align="center"><a id="rule" href="#" data-toggle="modal" data-target="#date_modal2">New</a>					<input type="hidden" name="rule_id" id="rule_id" >				</td>
			</tr>
		</tbody>
	</table>
</div>	<input type="hidden" id="task" name="task" value="create" /> 	<input type="hidden" id="controller" name="controller" value="giftcoderulerewardslist" /> 	<input type="hidden" id="boxchecked" name="boxchecked" value="0" /> 	<?php echo JHtml::_('form.token'); ?>	</form>	
<form method="post" action="<?php echo JRoute::_('index.php?option=com_awardpackage&controller=giftcoderulerewardslist.delete&package_id='.JRequest::getVar('package_id'));?>" name="adminForm" id="adminForm">
<table align="center" border="0" class="table table-striped" width="70%">
	<thead>
		<tr style="text-align:center; background-color:#CCCCCC">
			
			<th width="1%" class="hidden-phone"><?php echo JHtml::_('grid.checkall'); ?>
			</th>

			<th><?php echo JText::_('Start Date');?></th>
			<th><?php echo JText::_('End Date');?></th>
			<th><?php echo JText::_('Free giftcode user group');?></th>
			<th><?php echo JText::_('Giftcode rules');?></th>
			<th><?php echo JText::_('Created');?></th>
			<th><?php echo JText::_('Status');?></th>

		</tr>
	</thead>	<tbody>
	<?php
	foreach($this->get_all_rewards as $i => $reward):
	if($reward->status==1)	{
		$status = '<a href="index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id').'&task=unpublish&id='.$reward->id.'" title="Click To Unpublish" onClick="confirm(\'Are You Sure To Un Publish This Record.\');">Published</a>';
	}else if($reward->status==2)	{
		$status = '<a href="index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id').'&task=publish&id='.$reward->id.'" title="Click To Publish" onClick="confirm(\'Are You Sure To Publish This Record.\');">Unpublished</a>';
	}	
	else{
			$status = 'Expired';	
		}
?>
	<tr>			
		<td><?php echo JHtml::_('grid.id', $i, $reward->id); ?>		</td>
		<td align="center"><?php echo date('d F Y',strtotime($reward->start_date));?></td>
		<td align="center"><?php echo date('d F Y',strtotime($reward->end_date));?></td>
		<td align="center"><?php echo $reward->group_title;?></td>
		<td align="center"><?php echo $reward->rule_title;?></td>				
		<td align="center"><?php echo date('d F Y',strtotime($reward->created));?></td>
		<td align="center"><?php echo $status; ?></td>
	</tr>
	<?php endforeach; ?></tbody>
</table>
<div><input type="hidden" id="task" name="task" value="delete" /> 
	<input type="hidden" id="controller" name="controller" value="giftcoderulerewardslist" /> 
	<input type="hidden" id="boxchecked" name="boxchecked" value="0" /> 
	<?php echo JHtml::_('form.token'); ?>
</div>
</form>
</div>


			  <!-- Modal -->
			  <div class="modal fade" id="date_modal" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Select date</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<div id="datepicker" data-date="<?php echo date("m/d/Y") ?>"></div>
						<input type="hidden" id="start-date" value="<?php echo date("m/d/Y") ?>">
						
						<script>
							var startDate = new Date('01/01/2012');
							var FromEndDate = new Date();
							var ToEndDate = new Date();

							ToEndDate.setDate(ToEndDate.getDate()+365);
							jQuery('#datepicker').datepicker({
								"setDate": new Date(),
								"startDate": new Date()
							});
							jQuery('#datepicker').on("changeDate", function(selected) {
								jQuery('#start-date').val(
									jQuery('#datepicker').datepicker('getFormattedDate')
								);
								startDate = new Date(selected.date.valueOf());
								startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
								jQuery('#datepicker1').datepicker('setStartDate', startDate);
							});
						</script>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-start-date">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  

			  <!-- Modal -->
			  <div class="modal fade" id="date_modal1" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Select date</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<div id="datepicker1" data-date="<?php echo date("m/d/Y") ?>"></div>
						<input type="hidden" id="end-date" value="<?php echo date("m/d/Y") ?>">
						
						<script>
							jQuery('#datepicker1').datepicker({
								"setDate": new Date()
							});
							jQuery('#datepicker1').on("changeDate", function() {
								jQuery('#end-date').val(
									jQuery('#datepicker1').datepicker('getFormattedDate')
								);
							});
						</script>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-end-date">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
			  
			  <!-- Modal3 -->
			  <div class="modal fade" id="date_modal2" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Giftcode rules</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<ul class="weekdays" id="weekdays">
						<?php 
							foreach($this->rules as $rule){
						?>
							<li><input type="radio" name="weekday" value="<?php echo $rule->id."-".$rule->title; ?>"><label><?php echo $rule->title; ?></label></li>
						<?php 
							}
						?>
						</ul>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-rule">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
			  <!-- Modal3 -->
			  <div class="modal fade" id="date_modal3" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">User groups</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<ul class="weekdays" id="groups">
						<?php 
							foreach($this->freeUserGroups as $group){
						?>
							<li><input type="radio" name="weekday" value="<?php echo $group->id."-".$group->title; ?>"><label><?php echo $group->title; ?></label></li>
						<?php 
							}
						?>
						</ul>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-group">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
<script>
	jQuery(document).ready(function(){

		jQuery(document).on("click","#save-start-date",function(){
			var selDate = jQuery(this).parent().parent().find("#start-date").val();			jQuery('#start_date').val(selDate);
			selDate = selDate.split("/");
			var month = GetMonthName(selDate[0]);
			if (selDate.length > 0) {
				
				selectedVal = selDate[1]+" "+month;
				jQuery("#top_table").find("tbody #start").text(selectedVal);
				jQuery('#date_modal').modal('hide') 
			}
			else{
				alert("Please select any date.")
			}
		});
		
		jQuery(document).on("click","#save-end-date",function(){
			var selDate = jQuery(this).parent().parent().find("#end-date").val();			jQuery('#end_date').val(selDate);
			selDate = selDate.split("/");
			var month = GetMonthName(selDate[0]);
			if (selDate.length > 0) {
				
				selectedVal = selDate[1]+" "+month;

				jQuery("#top_table").find("tbody #end").text(selectedVal);
				jQuery('#date_modal1').modal('hide') 
			}
			else{
				alert("Please select any date.")
			}
		});
		
		jQuery(document).on("click","#save-rule",function(){
			var selectedVal = "";
			var selected = jQuery("#weekdays input[type='radio']:checked");
			if (selected.length > 0) {
				
				selectedVal = selected.val();
				selectedVal = selectedVal.split("-");
				jQuery('#rule_id').val(selectedVal[0]); 
				jQuery("#top_table").find("tbody #rule").text(selectedVal[1]);
				jQuery('#date_modal2').modal('hide') 
			}
			else{
				alert("Please select any rule.")
			}
		});
		
		jQuery(document).on("click","#save-group",function(){
			var selectedVal = "";
			var selected = jQuery("#groups input[type='radio']:checked");
			if (selected.length > 0) {
				
				selectedVal = selected.val();
				selectedVal = selectedVal.split("-");
				jQuery('#group_id').val(selectedVal[0]);
				jQuery("#top_table").find("tbody #group").text(selectedVal[1]);
				jQuery('#date_modal3').modal('hide') 
			}
			else{
				alert("Please select any group.")
			}
		});
		
		function GetMonthName(monthNumber) {
		  var months = ['January', 'February', 'March', 'April', 'May', 'June',
		  'July', 'August', 'September', 'October', 'November', 'December'];
		  return months[monthNumber-1];
		} 
	});
</script>