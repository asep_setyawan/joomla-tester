<?php 
//redirect
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
require_once JPATH_COMPONENT_ADMINISTRATOR . '/helpers/awardpackages.php';

class awardpackageViewfundreceiver extends JViewLegacy {
	
	function display($tpl = null) {
		CommunitySurveysHelper::initiate();
		$app = JFactory::getApplication();		
		$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
		$package_id = JRequest::getVar("package_id");	
		switch ($this->action){
			case 'list':
			
				$list_category = $model->get_fund_receiver1($package_id );
				$this->assignRef('list_category', $list_category);
				JToolBarHelper::title(JText::_('Fund Receiver Plan List'), 'logo.png');
				JToolbarHelper::back('Close', 'index.php?option=com_awardpackage&package_id=' . JRequest::getVar('package_id'));

				break;
			case 'create':
				JToolBarHelper::title(JText::_('New Fund Receiver Plan'), 'logo.png');
				//$title = '';
				if(JRequest::getVar('id') != null) {
				       $id=JRequest::getVar('id');
				}
				$this->ug          = JModelLegacy::getInstance('usergroup', 'AwardpackageModel');
      			$CountryList = new AwardpackagesHelper;
$countries = $CountryList->Countries_list();
				$this->assignRef('countries', $countries);
		
				$result = $model->get_fund_receiver_byid($id);
				foreach ($result as $rows){
				 $title = $rows->title;
				 $filter = $rows->filter;
				}
				
				$listfundreceiver_age = $model->get_fund_receiver_list_byid_age($id);
				$listfundreceiver_gender = $model->get_fund_receiver_list_byid_gender($id);
				$listfundreceiver_location = $model->get_fund_receiver_list_byid_location($id);

				$this->assignRef('result_age', $listfundreceiver_age);
				$this->assignRef('result_gender', $listfundreceiver_gender);
				$this->assignRef('result_location', $listfundreceiver_location);

				$this->assignRef('title', $title);
				$this->assignRef('filter', $filter);

				JToolBarHelper::custom('fundreceiver.save_create', 'copy', 'copy', 'Save & Close', false);
				JToolbarHelper::back('Cancel', 'index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar('package_id'));
				 $document = JFactory::getDocument();

        $document->addScript(JURI::base() . 'components/com_awardpackage/assets/js/jquery.min.js');
        $document->addStyleSheet(JURI::base() . 'components/com_awardpackage/assets/css/jquery.ui.all.css');
        $document->addScript(JURI::base() . 'components/com_awardpackage/assets/js/jquery.ui.core.js');
        $document->addScript(JURI::base() . 'components/com_awardpackage/assets/js/jquery.ui.widget.js');
        $document->addScript(JURI::base() . 'components/com_awardpackage/assets/js/jquery.ui.tabs.js');
        $document->addScript(JURI::base() . 'components/com_awardpackage/assets/js/tabs.js');
				break;				
		}
       
		parent::display($tpl);
		
	}	
}