<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class awardpackageViewGiftcoderule extends JViewLegacy {
    
    function __construct($config = array()) {
        
        parent::__construct($config);
        
        $this->model = & JModelLegacy::getInstance('Main', 'AwardpackageModel');
        
        $this->package_id = JRequest::getVar('package_id');
        
    }
    function display($tpl = null) {
        // Load the submenu.
        //AwardpackagesHelper::addSubmenu(JRequest::getCmd('view', 'referral'), 'donation');
		
		$document= &JFactory::getDocument();
		JHtml::_('bootstrap.framework');
		$document->addStyleSheet(JURI::base(true).'/components/com_awardpackage/assets/css/bootstrap-datepicker.min.css');
		$document->addStyleSheet(JURI::base(true).'/components/com_awardpackage/assets/css/custom.css');
		$document->addScript(JURI::base(true).'/components/com_awardpackage/assets/js/bootstrap-datepicker.min.js');
		
		$pagination = $this->get('Pagination');

		$task = JRequest::getVar('task');
		
		//echo $task; die;

		$model = $this->getModel('giftcoderule');
		
		$categories = $model->get_categories();
		
		if(isset($_REQUEST['rid']) && !empty($_REQUEST['rid']))
		{
			$get_rule_details = $model->get_rule_details_by_id($_REQUEST['rid']);

			$this->assignRef('rule_details',$get_rule_details);
			
			$get_rule_settings = $model->get_rule_settings_by_rule_id($_REQUEST['rid']);
			
			$this->assignRef('rule_settings',$get_rule_settings);
		}
		
		
		$this->assignRef('categories',$categories);
		

		if ($task =='create') {
			JToolBarHelper::title('Award Category - New Giftcode Rule');
			$this->addToolbarForCreatePage();
			$this->setLayout('create');
		}
		else if($task =='createcal'){
			JToolBarHelper::title('Award Category - New Giftcode Rule');
			$this->addToolbarForCreatePage();
			$this->setLayout('createcalendar');			
		}
		else if($task =='selectgiftcode'){
			JToolBarHelper::title('Award Category - Add Gift code');
			$this->addToolbarForGiftCodePage();
			$this->setLayout('giftcode');			
		}
		else if($task =='giftcoderule.delete'){
			$delete=$model->delete($_POST);
			$this->addToolbarForListPage();
			JToolBarHelper::title('Award Category - Giftcode Rules List');	
		}
		else if ($task =='update') {
			JToolBarHelper::title('Award Category - New Giftcode Rule');
			$this->addToolbarForUpdatePage();
			$this->setLayout('update');
		}
		else{
			$rules = $model->get_rules($this->package_id);
			$this->assignRef('rules',$rules);
			$this->addToolbarForListPage();
			JToolBarHelper::title('Award Category - Giftcode Rules List');
		}

		$this->pagination = $pagination;


        parent::display($tpl);
    }

	protected function addToolbarForListPage()
	{
		// assuming you have other toolbar buttons ...
 
		JToolBarHelper::custom('giftcoderule.create', 'new.png', 'new.png', 'Add', false,false);
		JToolBarHelper::custom('giftcoderule.delete', 'delete.png', 'delete.png', 'Delete', true);
 
	}
	
	protected function addToolbarForGiftCodePage()
	{
		// assuming you have other toolbar buttons ...
 
		JToolBarHelper::custom('giftcoderule.save', 'save.png', 'save.png', 'Save & Close', false,false);
		JToolBarHelper::custom('giftcoderule.back', 'delete.png', 'delete.png', 'Back', true);
 
	}
	
	protected function addToolbarForCreatePage()
	{
		// assuming you have other toolbar buttons ...
		JToolBarHelper::custom('giftcoderule.create', 'new.png', 'new.png', 'Add', false,false);
		//JToolBarHelper::custom('giftcoderule.delete', 'delete.png', 'delete.png', 'Delete', true);
		JToolBarHelper::custom('giftcoderule.save', 'save.png', 'save.png', 'Save & Close', false,false);
		JToolBarHelper::custom('giftcoderule.back', 'delete.png', 'delete.png', 'Back', false);
 
	}
	
	protected function addToolbarForUpdatePage()
	{
		// assuming you have other toolbar buttons ...
		JToolBarHelper::custom('giftcoderule.create', 'new.png', 'new.png', 'Add', false,false);
		JToolBarHelper::custom('giftcoderule.delete', 'delete.png', 'delete.png', 'Delete', false,false);
		JToolBarHelper::custom('giftcoderule.save', 'save.png', 'save.png', 'Save & Close', false,false);
		JToolBarHelper::custom('giftcoderule.back', 'delete.png', 'delete.png', 'Back', false);
 
	}

}
