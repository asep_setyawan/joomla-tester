<?php defined('_JEXEC') or die('Restricted access'); ?>
<style>
table tbody tr td, table thead tr th, a {
	text-align: center;
	font-size:90%;
}
</style>
<?php

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
/*Joomla.submitbutton = function(pressbutton) {alert(pressbutton);
	if(pressbutton=='giftcoderule.back'){
		window.location.href = "index.php?option=com_awardpackage&controller=giftcoderule";
	}
}*/

Joomla.submitbutton = function(pressbutton) {
	if(pressbutton=='giftcoderule.create'){
		var selID = jQuery(".selection").find(".active").attr("id");
		
		if(selID == "tab1"){
			var count = jQuery("#gift_code_rule_list_week_days").find(".bigbox").length;
			
			jQuery('#gift_code_rule_list_week_days').append('<div style="float:left;width:100%;" id="group'+(count+1)+'" class="bigbox"><div class="span10"><input type="checkbox" value="" name="check_complete_block" class="check_complete_block" /></div><div class="span4"><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th>Week days</th></tr><tr class="actions"><input type="hidden" name="group-id[]" value="'+(count+1)+'"><td align="right" colspan="3"><a data-target="#myModalWeekdays" data-toggle="modal" href="#" data-group-id="'+(count+1)+'">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td></tr></thead></table><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th width="1%" class="hidden-phone"><input type="checkbox" onclick="Joomla.checkAll(this)" title="" class="hasTooltip" value="" name="checkall-toggle" data-original-title="Check All"></th><th>Week day</th></tr></thead><tbody>	</tbody></table></div><div class="span7"><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th align="center" colspan="3">Giftcodes</th></tr><tr class="actions"><input type="hidden" name="group-id[]" value="'+(count+1)+'"><td align="right" colspan="3"><a data-target="#myModalWeekdays2" data-toggle="modal" href="#" data-group-id="'+(count+1)+'">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td></tr></thead></table><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th width="1%" class="hidden-phone"><input type="checkbox" onclick="Joomla.checkAll(this)" title="" class="hasTooltip" value="" name="checkall-toggle" data-original-title="Check All"></th><th>Gift code</th><th>Award symbol</th><th>Quantity</th></tr></thead><tbody></tbody></table></div><div class="span1"><a class="remove" href="javascript:;">Remove</a></div></div>');
					
		}
		else{
			var count = jQuery("#gift_code_rule_list_calendar").find(".bigbox").length;
			
			jQuery('#gift_code_rule_list_calendar').append('<div style="float:left;width:100%;" id="groupcal'+(count+1)+'" class="bigbox"><div class="span10"><input type="checkbox" value="" name="check_complete_block" class="check_complete_block" /></div><div class="span4"><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th>Calendar dates</th></tr><tr class="actions"><input type="hidden" name="group-id[]" value="'+(count+1)+'"><td align="right" colspan="3"><a data-target="#myModalCalendar" data-toggle="modal" href="#" data-group-id="'+(count+1)+'">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td></tr></thead></table><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th width="1%" class="hidden-phone"><input type="checkbox" onclick="Joomla.checkAll(this)" title="" class="hasTooltip" value="" name="checkall-toggle" data-original-title="Check All"></th><th>Calendar date</th></tr></thead><tbody>	</tbody></table></div><div class="span7"><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th align="center" colspan="3">Giftcodes</th></tr><tr class="actions"><input type="hidden" name="group-id[]" value="'+(count+1)+'"><td align="right" colspan="3"><a data-target="#myModalCalendar2" data-toggle="modal" href="#" data-group-id="'+(count+1)+'">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td></tr></thead></table><table width="70%" border="0" align="center" class="table table-striped"><thead><tr style="text-align:center; background-color:#CCCCCC"><th width="1%" class="hidden-phone"><input type="checkbox" onclick="Joomla.checkAll(this)" title="" class="hasTooltip" value="" name="checkall-toggle" data-original-title="Check All"></th><th>Gift code</th><th>Award symbol</th><th>Quantity</th></tr></thead><tbody></tbody></table></div><div class="span1"><a class="remove" href="javascript:;">Remove</a></div></div>');
					
		}

		jQuery(document).on('click','.remove',function(){
			jQuery(this).parent().parent().remove();
		});
	/*##### Do Other Things What ever you Want#########*/
	}	else if(pressbutton=='giftcoderule.back'){
		window.location.href = "index.php?option=com_awardpackage&controller=giftcoderule&package_id=<?php echo $this->rule_details[0]->package_id; ?>";
		}	
		else if(pressbutton=='giftcoderule.delete'){
			jQuery('#gift_code_rule_list_week_days .check_complete_block').each(function(){
		if (jQuery(this).is(':checked')==true){ 	
				jQuery(this).parent().parent().remove();
		}		});	
				alert("Your Settings Are Temporary Saved. Please Click Save & Close To Save Permanently.");
		}	
		else{
		document.adminForm.task.value=pressbutton;
		submitform(pressbutton);
	}
}
</script>
<form method="post" action="<?php echo JRoute::_('index.php?option=com_awardpackage&controller=giftcoderule&task=save');?>" name="adminForm" id="adminForm">

		<div>
				<div class="span10">
					<div class="selection">
						<a class="active" href="javascript:void(0)" id="tab1">Week days</a>
						<a href="javascript:void(0)" id="tab2">Calendar dates</a>
					</div>
				</div>
				
				<div class="span10">					
					<label class="span1">Title</label>
					<div class="span8">
						<input type="text" name="title" id="title" value="<?php if(!empty($this->rule_details[0]->title)) echo $this->rule_details[0]->title;?>" >
					</div>
				</div>
			<div class="span10" id="gift_code_rule_list_week_days">				
			<?php
			$data_value=[];
			$gift_codes=[];
			if(!empty($this->rule_settings))
			{	//echo "<pre>";print_r($this->rule_settings);echo "</pre>";
				$i=1;
				foreach($this->rule_settings as $key=>$value)
				{
					if($value->data_type == "date") continue;
					if(!empty($value->data_value))
					{
						
						$data_value=json_decode($value->data_value);	
					}
					
					if(!empty($value->giftcodes))
					{
						$gift_codes=json_decode($value->giftcodes);	
					}	
			?>					
				<div style="float:left;width:100%;" id="group<?php echo $i; ?>" class="bigbox">				<div class="span10">					<input type="checkbox" value="" name="check_complete_block" class="check_complete_block" />				</div>					
					<div class="span4">
						<table align="center" border="0" class="table table-striped" width="70%">
							<thead>
								<tr style="text-align:center; background-color:#CCCCCC">
									<th><?php echo JText::_('Week days');?></th>

								</tr>
								<tr class="actions">
									<input type="hidden" name="group-id[]" value="<?php echo $i; ?>">
									<td colspan="3" align="right"><a href="#" data-toggle="modal" data-target="#myModalWeekdays" data-group-id="<?php echo $i; ?>">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td>
								</tr>
							</thead>
							<table align="center" border="0" class="table table-striped" width="70%">
								<thead>
									<tr style="text-align:center; background-color:#CCCCCC">
										<!-- <th><input type="checkbox" name="toggle" value=""
											onclick="checkAll(<?php echo count($this->items); ?>);" /></th> -->
										<th width="1%" class="hidden-phone"><?php echo JHtml::_('grid.checkall'); ?>
										</th>
										<th><?php echo JText::_('Week day');?></th>

									</tr>
								</thead>
								<tbody>
								<?php
									if(!empty($data_value))
									{
										
										foreach($data_value as $day_key=>$day_value)
										{	
											if(!empty($day_value) && $day_value!='null')
											{
									?>
										<tr>
										<td><input type="checkbox" value="<?php echo $day_value; ?>" name="ruleweekday[]"><input type="hidden" value="<?php echo $day_value; ?>" name="weekdays[<?php echo $i; ?>][day][]"></td>
										<td><?php echo $day_value; ?></td>	
										</tr>										
								<?php
											}
										}
									}
								?>	
								</tbody>

							</table>
						</table>
					</div>
					<div class="span7">
						<table align="center" border="0" class="table table-striped" width="70%">
						<thead>
							<tr style="text-align:center; background-color:#CCCCCC">

								<th colspan="3" align="center"><?php echo JText::_('Giftcodes');?></th>

							</tr>
							<tr class="actions">
								<input type="hidden" name="group-id[]" value="<?php echo $i; ?>">
								<td colspan="3" align="right"><a href="#" data-toggle="modal" data-target="#myModalWeekdays2" data-group-id="<?php echo $i; ?>">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td>
							</tr>
						</thead>
						<table align="center" border="0" class="table table-striped" width="70%">
							<thead>
								<tr style="text-align:center; background-color:#CCCCCC">
									<!-- <th><input type="checkbox" name="toggle" value=""
										onclick="checkAll(<?php echo count($this->items); ?>);" /></th> -->
									<th width="1%" class="hidden-phone"><?php echo JHtml::_('grid.checkall'); ?></th>
									<th><?php echo JText::_('Gift code');?></th>
									<th><?php echo JText::_('Award symbol');?></th>
									<th><?php echo JText::_('Quantity');?></th>

								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($gift_codes))
								{
									foreach($gift_codes as $code_key=>$code_value)
									{
										
										if(!empty($code_value) && $code_value!='null')
										{
											$code_data=explode(',',$code_value);
								?>
										<tr>
										<td><input type="checkbox"  name="rulegiftcode[]"><input type="hidden" value="<?php echo $code_value; ?>" name="weekdays[<?php echo $i; ?>][giftcode][]"></td>
										<td><?php echo $code_data[1]; ?></td>
										<td><?php echo $code_data[0]; ?></td>
										<td><?php echo $code_data[2]; ?></td>	
										</tr>										
								<?php
										}
									}
								}
								?>	
							</tbody>
						</table></table>
					</div>
				</div>
		<?php 
				$i++;
				}
			}
		?>
			</div>
			
			<div class="span10" id="gift_code_rule_list_calendar" style="display:none;">
			<?php
			$data_value=[];
			$gift_codes=[];
			if(!empty($this->rule_settings))
			{	
				$i=1;
				foreach($this->rule_settings as $key=>$value)
				{
					if($value->data_type == "day") continue;
					if(!empty($value->data_value))
					{
						
						$data_value=json_decode($value->data_value);	
					}
					
					if(!empty($value->giftcodes))
					{
						$gift_codes=json_decode($value->giftcodes);	
					}	
			?>
				<div style="float:left;width:100%;" id="groupcal<?php echo $i; ?>" class="bigbox">					<div class="span10"><input type="checkbox" value="" name="check_complete_block" class="check_complete_block" /></div>
					<div class="span4">
						<table align="center" border="0" class="table table-striped" width="70%">
							<thead>
								<tr style="text-align:center; background-color:#CCCCCC">
									<th><?php echo JText::_('Calendar dates');?></th>

								</tr>
								<tr class="actions">
									<input type="hidden" name="group-id[]" value="<?php echo $i; ?>">
									<td colspan="3" align="right"><a href="#" data-toggle="modal" data-target="#myModalCalendar" data-group-id="<?php echo $i; ?>">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td>
								</tr>
							</thead>
							<table align="center" border="0" class="table table-striped" width="70%">
								<thead>
									<tr style="text-align:center; background-color:#CCCCCC">
										<!-- <th><input type="checkbox" name="toggle" value=""
											onclick="checkAll(<?php echo count($this->items); ?>);" /></th> -->
										<th width="1%" class="hidden-phone"><?php echo JHtml::_('grid.checkall'); ?>
										</th>
										<th><?php echo JText::_('Calendar date');?></th>

									</tr>
								</thead>
								<tbody>
								<?php
									if(!empty($data_value))
									{
										
										foreach($data_value as $day_key=>$day_value)
										{	
											if(!empty($day_value) && $day_value!='null')
											{
									?>
										<tr>
										<td><input type="checkbox" value="<?php echo $day_value; ?>" name="ruleweekday[]"><input type="hidden" value="<?php echo $day_value; ?>" name="calendar[<?php echo $i; ?>][date][]"></td>
										<td><?php echo $day_value; ?></td>	
										</tr>										
								<?php
											}
										}
									}
								?>	
								</tbody>

							</table>
						</table>
					</div>
					<div class="span7">
						<table align="center" border="0" class="table table-striped" width="70%">
						<thead>
							<tr style="text-align:center; background-color:#CCCCCC">

								<th colspan="3" align="center"><?php echo JText::_('Giftcodes');?></th>

							</tr>
							<tr class="actions">
								<input type="hidden" name="group-id[]" value="<?php echo $i; ?>">
								<td colspan="3" align="right"><a href="#" data-toggle="modal" data-target="#myModalCalendar2" data-group-id="<?php echo $i; ?>">New</a><a href="javascript:void(0)" class="deleteAll">Delete</a></td>
							</tr>
						</thead>
						<table align="center" border="0" class="table table-striped" width="70%">
							<thead>
								<tr style="text-align:center; background-color:#CCCCCC">
									<!-- <th><input type="checkbox" name="toggle" value=""
										onclick="checkAll(<?php echo count($this->items); ?>);" /></th> -->
									<th width="1%" class="hidden-phone"><?php echo JHtml::_('grid.checkall'); ?></th>
									<th><?php echo JText::_('Gift code');?></th>
									<th><?php echo JText::_('Award symbol');?></th>
									<th><?php echo JText::_('Quantity');?></th>

								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($gift_codes))
								{
									foreach($gift_codes as $code_key=>$code_value)
									{
										
										if(!empty($code_value) && $code_value!='null')
										{
											$code_data=explode(',',$code_value);
								?>
										<tr>
										<td><input type="checkbox"  name="rulegiftcode[]"><input type="hidden" value="<?php echo $code_value; ?>" name="calendar[<?php echo $i; ?>][giftcode][]"></td>
										<td><?php echo $code_data[1]; ?></td>
										<td><?php echo $code_data[0]; ?></td>	
										<td><?php echo $code_data[2]; ?></td>	
										</tr>										
								<?php
										}
									}
								}
								?>	
							</tbody>
						</table></table>
					</div>
				</div>
		<?php
				$i++;
				}
			}
		?>
			</div>
			  <!-- Modal -->
			  <div class="modal fade" id="myModalWeekdays" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Week days</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<ul class="weekdays" id="weekdays">
							<li><input type="radio" name="weekday" value="Monday"><label>Monday</label></li>
							<li><input type="radio" name="weekday" value="Tuesday"><label>Tuesday</label></li>
							<li><input type="radio" name="weekday" value="Wednesday"><label>Wednesday</label></li>
							<li><input type="radio" name="weekday" value="Thrusday"><label>Thrusday</label></li>
							<li><input type="radio" name="weekday" value="Friday"><label>Friday</label></li>
							<li><input type="radio" name="weekday" value="Saturday"><label>Saturday</label></li>
							<li><input type="radio" name="weekday" value="Sunday"><label>Sunday</label></li>
						</ul>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-day">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
			  <!-- Modal -->
			  <div class="modal fade" id="myModalCalendar" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Select date</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<div id="datepicker" data-date="<?php echo date("m/d/Y") ?>"></div>
						<input type="hidden" id="my_hidden_input" value="<?php echo date("m/d/Y") ?>">
						
						<script>
							jQuery('#datepicker').datepicker({
								"setDate": new Date()
							});
							jQuery('#datepicker').on("changeDate", function() {
								jQuery('#my_hidden_input').val(
									jQuery('#datepicker').datepicker('getFormattedDate')
								);
							});
						</script>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-date">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
			  
			  
			  <!-- Modal -->
			  <div class="modal fade" id="myModalWeekdays2" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Select Gift code</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<table class="table table-striped" id="giftcodes">
							<thead>
								<tr style="text-align:center; background-color:#CCCCCC">
									<!-- <th><input type="checkbox" name="toggle" value=""
										onclick="checkAll(<?php echo count($this->categories); ?>);" /></th> -->
									<th width="1%" class="hidden-phone">
									</th>					
										<th><?php echo JText::_('COM_AWARD_PACKAGE_CATEGORY')?></th>
										<th><?php echo JText::_('COM_AWARD_PACKAGE_CATEGORY_NAME');?></th>
										<th><?php echo JText::_('Quantity');?></th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($this->categories as $i=>$category) { ?>
								<tr class="row<?php echo $i%2;?>">
									<td><input type="radio" name="category_id" value="<?php echo $category->category_id."-".$category->name; ?>"></td>
									<td width="60px" align="center" height="40px">
										<table>
											<tr>
												<td style="padding-top:14px;width:40px;height:30px;text-align:center;background-color:<?php echo $category->color_code;?>" valign="center">
												<font color="white" size="5"><b><?php echo $category->category_id; ?></b></font>
												</td>
											</tr>
										</table>
									</td>
									<td><?php echo $category->name; ?></td>
									<td><input type="text" class="quantity" value="0" name="quantity" /></td>

								</tr>
								<?php } ?>
							</tbody>
							<input type="hidden" id="controller" name="controller" value="quiz" />
							<input type="hidden" name="boxchecked" value="1" />
							<input type="hidden" name="task" value="" />
						</table>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-giftcode-day">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
			  <!-- Modal -->
			  <div class="modal fade" id="myModalCalendar2" role="dialog" style="display:none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Select Gift code</h4>
					</div>
					<div class="modal-body" style="overflow-y:scroll">
						<table class="table table-striped" id="giftcodes">
							<thead>
								<tr style="text-align:center; background-color:#CCCCCC">
									<!-- <th><input type="checkbox" name="toggle" value=""
										onclick="checkAll(<?php echo count($this->categories); ?>);" /></th> -->
									<th width="1%" class="hidden-phone">
									</th>					
										<th><?php echo JText::_('COM_AWARD_PACKAGE_CATEGORY')?></th>
										<th><?php echo JText::_('COM_AWARD_PACKAGE_CATEGORY_NAME');?></th>
										<th><?php echo JText::_('Quantity');?></th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($this->categories as $i=>$category) { ?>
								<tr class="row<?php echo $i%2;?>">
									<td><input type="radio" name="category_id" value="<?php echo $category->category_id."-".$category->name; ?>"></td>
									<td width="60px" align="center" height="40px">
										<table>
											<tr>
												<td style="padding-top:14px;width:40px;height:30px;text-align:center;background-color:<?php echo $category->color_code;?>" valign="center">
												<font color="white" size="5"><b><?php echo $category->category_id; ?></b></font>
												</td>
											</tr>
										</table>
									</td>
									<td><?php echo $category->name; ?></td>
									<td><input type="text" class="quantity" value="0" name="quantity" /></td>

								</tr>
								<?php } ?>
							</tbody>
							<input type="hidden" id="controller" name="controller" value="quiz" />
							<input type="hidden" name="boxchecked" value="1" />
							<input type="hidden" name="task" value="" />
						</table>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="box-id" value="" class="box-id">
					   <button type="button" class="btn btn-default" id="save-giftcode-date">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
			  
	<input type="hidden" id="controller" name="controller" value="giftcoderule" />
	<input type="hidden" name="boxchecked" value="1" />
	<input type="hidden" name="rule_id" value="<?php echo isset($_REQUEST['rid'])?$_REQUEST['rid']:0; ?>">
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="package_id" value="<?php if(!empty($this->rule_details[0]->package_id)) echo $this->rule_details[0]->package_id;?>" />
		
</form>
<script>
	jQuery(document).ready(function(){
		
		jQuery(".selection a").click(function(){
			var seldiv = jQuery(this).attr("id");
			if(seldiv == "tab2")
			{
				jQuery("#gift_code_rule_list_week_days").hide();
				jQuery("#gift_code_rule_list_calendar").show();
			}
			else{
				jQuery("#gift_code_rule_list_calendar").hide();
				jQuery("#gift_code_rule_list_week_days").show();				
			}
			
			jQuery(".selection").find("a").removeClass("active");
			jQuery(this).addClass("active");
			
		});
		
		jQuery(document).on("click", "a[data-toggle=modal]", function () {
			 var groupId = jQuery(this).data('group-id');
			 jQuery(".modal-footer").find(".box-id").val(groupId);
		});

		jQuery(document).on("click","#save-day",function(){
			var selectedVal = "";
			var selected = jQuery("#weekdays input[type='radio']:checked");
			if (selected.length > 0) {
				
				selectedVal = selected.val();
				var boxid = jQuery(this).parent().find("input").val();
				jQuery("#group"+boxid).find(".span4 table tbody").append("<tr><td><input type='checkbox' name='ruleweekday[]' value='"+selectedVal+"'><input type='hidden' name='weekdays["+boxid+"][day][]' value='"+selectedVal+"'></td><td>"+selectedVal+"</td></tr>");
				jQuery('#myModalWeekdays').modal('hide') 
			}
			else{
				alert("Please select any day.")
			}
		});
		
		jQuery(document).on("click","#save-date",function(){
			var selDate = jQuery(this).parent().parent().find("#my_hidden_input").val();
			selDate = selDate.split("/");
			var month = GetMonthName(selDate[0]);
			if (selDate.length > 0) {
				
				selectedVal = selDate[1]+" "+month;
				var boxid = jQuery(this).parent().find("input").val();
				jQuery("#groupcal"+boxid).find(".span4 table tbody").append("<tr><td><input type='checkbox' name='ruledate[]' value='"+selectedVal+"'><input type='hidden' name='calendar["+boxid+"][date][]' value='"+selectedVal+"'></td><td>"+selectedVal+"</td></tr>");
				jQuery('#myModalCalendar').modal('hide') 
			}
			else{
				alert("Please select any date.")
			}
		});
		
		function GetMonthName(monthNumber) {
		  var months = ['January', 'February', 'March', 'April', 'May', 'June',
		  'July', 'August', 'September', 'October', 'November', 'December'];
		  return months[monthNumber-1];
		} 
		
		
		jQuery(document).on("click","#save-giftcode-day",function(){
			var selectedVal = "";
			var selected = jQuery("#giftcodes input[type='radio']:checked");
			var quantity = jQuery("#giftcodes input[type='radio']:checked").parent().parent().find("td input[type='text']").val();
			if (selected.length > 0) {
				
				selectedVal = selected.val();
				selectedVal = selectedVal.split("-");
				var boxid = jQuery(this).parent().find(".box-id").val();
				jQuery("#group"+boxid).find(".span7 table tbody").append("<tr><td><input type='checkbox' name='rulegiftcode[]' value='"+selectedVal[0]+"'><input type='hidden' name='weekdays["+boxid+"][giftcode][]' value='"+selectedVal[0]+","+selectedVal[1]+","+quantity+"'></td><td>"+selectedVal[1]+"</td><td>"+selectedVal[0]+"</td><td>"+quantity+"</td></tr>");
				jQuery('#myModalWeekdays2').modal('hide') 
			}
			else{
				alert("Please select any giftcode.")
			}
		});
		
		jQuery(document).on("click","#save-giftcode-date",function(){
			var selectedVal = "";
			var selected = jQuery("#giftcodes input[type='radio']:checked");
			var quantity = jQuery("#giftcodes input[type='radio']:checked").parent().parent().find("td input[type='text']").val();
			if (selected.length > 0) {
				
				selectedVal = selected.val();
				selectedVal = selectedVal.split("-");
				var boxid = jQuery(this).parent().find(".box-id").val();
				jQuery("#groupcal"+boxid).find(".span7 table tbody").append("<tr><td><input type='checkbox' name='rulegiftcode[]' value='"+selectedVal[0]+"'><input type='hidden' name='calendar["+boxid+"][giftcode][]' value='"+selectedVal[0]+","+selectedVal[1]+","+quantity+"'></td><td>"+selectedVal[1]+"</td><td>"+selectedVal[0]+"</td><td>"+quantity+"</td></tr>");
				jQuery('#myModalCalendar2').modal('hide') 
			}
			else{
				alert("Please select any giftcode.")
			}
		});
		
		jQuery(".hasTooltip").click(function(){
			
			if(this.checked) { // check select status
				jQuery(this).parents().closest("table").find("tbody tr input[type='checkbox']").each(function(index, element){
					this.checked = true;  //select all checkboxes with class "checkbox1"              
				});
			}else{
				jQuery(this).parents().closest("table").find("tbody tr input[type='checkbox']").each(function(index, element){
					this.checked = false; //deselect all checkboxes with class "checkbox1"                      
				});        
			}
				
		});
		
		jQuery(".deleteAll").click(function(){
			
			jQuery(this).parents().closest("table").next("table").find("tbody tr input[type='checkbox']").each(function(index, element){
				if(this.checked){
					jQuery(element).parent().parent().remove();
				}            
			});
				
		});
		
	});
</script>