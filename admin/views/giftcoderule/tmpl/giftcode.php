<style>
table tbody tr td, table thead tr th, a {
	text-align: center;
	font-size:90%;
}
</style>
<?php

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
Joomla.submitbutton = function(pressbutton) {
	if(pressbutton=='giftcoderule.back'){
		history.go(-1);
	/*##### Do Other Things What ever you Want#########*/
	}else{
		document.adminForm.task.value=pressbutton;
		submitform(pressbutton);
	}
}
</script>
<div id="j-main-container" class="span6">
<form method="post"
	action="<?php echo JRoute::_('index.php?option=com_awardpackage&view=quiz&package_id='.JRequest::getVar("package_id"));?>"
	name="adminForm" id="adminForm">
<table class="table table-striped">
	<thead>
		<tr style="text-align:center; background-color:#CCCCCC">
			<!-- <th><input type="checkbox" name="toggle" value=""
				onclick="checkAll(<?php echo count($this->categories); ?>);" /></th> -->
			<th width="1%" class="hidden-phone">
			</th>					
				<th><?php echo JText::_('COM_AWARD_PACKAGE_CATEGORY')?></th>
				<th><?php echo JText::_('COM_AWARD_PACKAGE_CATEGORY_NAME');?></th>
				<th><?php echo JText::_('Quantity');?></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($this->categories as $i=>$category) { 
	if($category->unlocked==0){

		$this->readonly = ' disabled';
	}else{
		$this->readonly = ' ';
	}
	?>
		<tr class="row<?php echo $i%2;?>">
			<td><input type="radio" name="category_id" value="<?php echo $category->category_id; ?>"></td>
			<td width="60px" align="center" height="40px">
				<table>
					<tr>
						<td style="padding-top:14px;width:40px;height:30px;text-align:center;background-color:<?php echo $category->color_code;?>" valign="center">
						<font color="white" size="5"><b><?php echo $category->category_id; ?></b></font>
						</td>
					</tr>
				</table>
			</td>
			<td><?php echo $category->name; ?></td>
			<td><input type="text" onkeypress="return isNumberKey(event)" value="0" name="quantity" /></td>

		</tr>
		<?php } ?>
	</tbody>
	<input type="hidden" id="controller" name="controller" value="quiz" />
	<input type="hidden" name="boxchecked" value="1" />
	<input type="hidden" name="task" value="" />
</table>
</form>
					

</div>                            
