<?php
defined('_JEXEC') or die('Restricted access');

$categoryData =& $this->categoryData;

$color = array();

$color[] = "Merah";

foreach ($categoryData as $row) {
	$color[$row->id] = $row->name;
}

?>
<form id="adminForm" 
				action="<?php echo 'index.php?option=com_awardpackage&view=showgiftcode&gcid='.$this->gcid.'&package_id='.$this->package_id.' ';?>" method="post" name="adminForm">
<input type="hidden" id="gcid" name="gcid" value=<?php echo $this->gcid;?> >         
<input type="hidden" id="package_id" name="package_id" value=<?php echo $this->package_id;?> > 
<div id="j-main-container" class="span10">
        
    <table class="table table-hover table-striped table-bordered">
    	<thead>
<tr>     <td colspan="4" style="text-align:right;">
                                                                        <?php echo $this->pagination->getLimitBox(); ?>
                                                                        </td></tr>
                                                                        
		<tr style="text-align:center; background-color:#CCCCCC">
			<th>#</th>
			<th>Giftcode Category</th>
			<th>Created</th>
			<th>Gift Code</th>
		</tr>
	</thead>
	<?php
	$i = 1;
	foreach ($this->data as $row ) 
	{
		?>
	<tr>
		<td><?php echo $i++; ?></td>
		<td><?php echo $color[$row->giftcode_category_id]; ?></td>
		<td><?php echo date("d-M-Y",strtotime($row->created_date_time)); ?></td>
		<td><?php echo $row->giftcode; ?></td>
	</tr>
	<?php
	}
	?>
    <tr><td colspan="4" style="text-align:right;">
    <div class="pagination">
    <?php 
echo $this->pagination->getListFooter();;
echo '<br/><br/>'. $this->pagination->getPagesCounter(); ?>
        </div>
        </td></tr>
</table>
</div>
</form>
