<?php
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );
jimport('joomla.html.pagination');
class AwardPackageViewshowgiftcode extends JViewLegacy
{
	function display($tpl = null)
	{
	    // Load the submenu.
	    AwardpackagesHelper::addSubmenuGiftcode(JRequest::getCmd('view', 'referral'), 'giftcodecollection');
	    
	    JToolBarHelper::title(JText::_('View Giftcode Collection List'),'generic.png');
        
	    JToolBarHelper::back();	   
		$gcid = JRequest::getVar('gcid'); 
		$package_id = JRequest::getVar('package_id'); 
		$model =& JModelLegacy::getInstance('Giftcodecode','AwardPackageModel');           
	    $categoryModel =& JModelLegacy::getInstance('GiftcodeCategory','AwardPackageModel');
		$total = $model->getTotalGiftcode($gcid);
		$app = JFactory::getApplication();		
		$limitstart = $app->getUserStateFromRequest( '', 'limitstart', $limitstart, 'int' );
		$limit = $app->input->getInt('limit', $limit);
		$limit = (!empty($limit) ? $limit : 5);		
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
    	$this->pager = new JPagination($total, $limitstart, $limit);
     	$this->pagination = $this->pager;            
	    $categoryData = $categoryModel->getData();        
		$document= &JFactory::getDocument();	
		$this->assignRef('categoryData', $categoryData);        	
		$data = $model->getGiftcode($gcid, $limit, $limitstart);
		$this->assignRef('data',$data);		
		$this->assignRef('gcid',$gcid);		
		$this->assignRef('package_id',$package_id);		

		parent::display($tpl);        
	}

    function add_submenu(){
        JSubMenuHelper::addEntry(JText::_('Giftcode Collection List'), 'index.php?option=com_giftcode');        
        JSubMenuHelper::addEntry(JText::_('Giftcode Queue List'), 'index.php?option=com_giftcode&view=queue' );        
        JSubMenuHelper::addEntry(JText::_('Giftcode Category'), 'index.php?option=com_giftcode&view=category' );
	}

}