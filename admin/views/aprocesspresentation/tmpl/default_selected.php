<div style="display:inline;">

	<div style="width: 20%; float:left;padding:10px 0 0 0;font-size:11px;">

								<table class="table table-striped table-hover table-bordered" width="100%">  
                                <thead>                              	
                                <tr>
		                                <th colspan="4"><span>Fund prize plan</span>
        		                        <span style="float:right;">
                                        <button type="button" class="btn btn-primary btn-invite-reg-groups"
												onclick="onDeleteStartFundPrize();" id="addNewProcessBtn"><i></i> <?php echo JText::_('Delete');?></button>
										</span>	
                                       
                                        </th>
                                	</tr>       
                                    <th colspan="4">Start fund prize when award funds plan is above: $
									<?php echo (!empty($presentation->fund_amount) ? $presentation->fund_amount : 0 );	 ?>
                                        </th>
                                	</tr>                           	
                                    <tr>
		                                <th><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
        		                        </th>
                                        <th>No
        		                        </th>
                		                <th>Funding Value
                        		        </th>
                                       
                                	</tr>
                                    </thead>
                                    <?php foreach ($this->startfundprize as $startfundprizes){
									$k++;
									?>
                                    <tbody>
                                    <tr>
                                    <td><?php echo JHTML::_( 'grid.id', $i, $startfundprizes->id );?></td>

		                                <td><?php echo $k; ?> </td>
                		                <td><?php echo $startfundprizes->value_from.' to '.$startfundprizes->value_to;?></td>
                                        

                                	</tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
	</div>
    
    <div style="width: 79%; float:right;padding:10px 0 0 0;font-size:11px;">

<div class="scroller">				
                        <table class="table">
                        <tr><td>
                        <table class="table-striped table-bordered">
						<?php 
				$urutan = 0;
				foreach ($this->presentations as $rows) { 
				$urutan++; 
				$j = 0;  
//					echo '<h4>Selected Presentation # '.JText::_( $urutan ).'</h4>'; 
					echo '<h4>Selected Presentation </h4>'; ?>

						
												<input type="hidden" name="presentations[]" value="<?php echo $this->selectedPresentation; ?>">
                                                                    <thead>
																	<tr>
																		<th><?php echo JText::_( 'Selected presentation' ); ?></th>
																		<th><?php echo JText::_( 'Prize value range' ); ?></th>														
																		<th><?php echo JText::_( 'Report' ); ?></th>										
																	</tr>						
                                                                </thead>
                                                                <tbody>
																<tr>
																	<td><?php echo (!empty($this->PrizeSelected) && count($this->PrizeSelected) > 0 ? count($this->PrizeSelected) . ' prizes' : 'New' ); ?></td>
																	<td><?php echo '$'.$this->valuePrizeFrom.' to $'.$this->valuePrizeTo; ?></td>
																	<td><span style="cursor:pointer;">View</span></td>
																	
																</tr>	
                                                                <?php } ?>															
                                                                </tbody>
															</table>
                        </td></tr>
                        <tr><td>
                       
						<table class="table-bordered" >
                          <tr style="background-color:#f9f9f9;">
                            <th colspan="12" style="font-size:12px;"><?php echo JText::_('Process Symbol');?></th>
                            <th colspan="4" style="font-size:12px;"><?php echo JText::_('Price Symbol');?></th>
                            <th colspan="2" style="font-size:12px;"><?php echo JText::_('Distribute Symbol');?></th>
                            <th colspan="3" style="font-size:12px;"><?php echo JText::_('Fund Receiver');?></th>
                          </tr>
                          <tr>
<td colspan="12" style="padding:0;margin:0;">
<table width="400px;">
<tr>                         
<td width="100px;"><?php echo JText::_('Extract Pieces (EP)');?></td>
<td width="200px;"><input type="text" class="input-medium" name="extract_pieces[]" maxlength="4" value="" onkeyup="numbersOnly(this);" /></td>
<td><button type="button" class="btn btn-primary btn-invite-reg-groups" onclick="onAddExtractPieces('<?php echo $selectedPresentation->id; ?>', '<?php echo $j; ?>');" id="addBtn"><i></i> <?php echo JText::_('Add');?></button></td>
</tr>
</table>
</td>
                            <td colspan="4">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
<td colspan="12" style="padding:0;margin:0;">
<table width="400px;">
<tr>                           
<td width="100px;"><?php echo JText::_('Value Pieces (VP)');?></td>
<td width="200px;"><input type="text" class="input-medium" name="value_pieces[]" maxlength="4" value="" onkeyup="numbersOnly(this);" /></td>
<td><button type="button" class="btn btn-primary btn-invite-reg-groups" onclick="onAddValuePieces('<?php echo $selectedPresentation->id; ?>', '<?php echo $j; ?>');" id="addBtn"><i></i> <?php echo JText::_('Add');?></button></td>
</tr>
</table>
</td>
                            <td colspan="4">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
<td colspan="12" style="padding:0;margin:0;">
<table width="400px;">
<tr>                           
<td width="100px;"><?php echo JText::_('Set each VP to');?></td>
<td width="200px;"><input type="text" class="input-medium" name="num_clone_val[]" maxlength="4" value="" onkeyup="numbersOnly(this);" /></td>
<td><button type="button" class="btn btn-primary btn-invite-reg-groups" onclick="onAddNumCloneVal('<?php echo $selectedPresentation->id ?>', '<?php echo $j; ?>');" id="addBtn"><i></i> <?php echo JText::_('Add');?></button></td>
</tr>
</table>
</td>

<td colspan="4" style="padding:0;margin:0;">
<table width="400px;">
<td width="100px;"><?php echo JText::_('Price of each EP');?></td>
<td width="200px;"><input type="text" class="input-medium" name="price_of_each_extracted_pieces[]" maxlength="4" 																						value="" onkeyup="numbersOnly(this);" /></td>
<td><button type="button" class="btn btn-primary btn-invite-reg-groups" onclick="onPriceOfExtractedPieces('<?php echo $selectedPresentation->id ?>', '<?php echo $j; ?>');" id="addBtn"><i></i> <?php echo JText::_('Add');?></button></td>
</tr>
</table>
</td>
                            <th colspan="2" style="text-align:center;"><?php echo JText::_('Filled symbol queue');?></th>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
<td colspan="12" style="padding:0;margin:0;">
<table width="400px;">
<tr>                                                    
<td width="100px;"><?php echo JText::_('Set each FP to');?></td>
<td width="200px;"><input type="text" class="input-medium" name="num_clone_free[]" maxlength="4" value="" onkeyup="numbersOnly(this);" /></td>
<td><button type="button" class="btn btn-primary btn-invite-reg-groups" onclick="onAddNumCloneFree('<?php echo $selectedPresentation->id ?>', '<?php echo $j; ?>');" id="addBtn"><i></i> <?php echo JText::_('Add');?></button></td>   
</tr>
</table>
</td>

<td colspan="4" style="padding:0;margin:0;">
<table width="400px;">
<td width="100px;"><?php echo JText::_('Price of each VP');?></td>
<td width="200px;"><input type="text" class="input-medium" name="price_of_selected_rpc[]" maxlength="4" 																						value="" onkeyup="numbersOnly(this);" /></td>
<td><button type="button" class="btn btn-primary btn-invite-reg-groups" onclick="onPriceOfSelectedRPC('<?php echo $selectedPresentation->id ?>', '<?php echo $j; ?>');" id="addBtn"><i></i> <?php echo JText::_('Add');?></button></td>
</tr>
</table>
</td>

                            <th colspan="2" style="text-align:center;"><?php 
																				if (!empty($symbolName)){
																				$result = $this->model->getUserGroupName($presentationId);
																				if($result != null){
																						$usergroupname = $result->usergroup;
																								}
																				$symbolCount = count($this->model->getSymbolQueueCount($usergroupname));
																				echo (!empty($symbolCount) ? $symbolCount : 0 );}else{
																				echo 0;
																				}
																				?></th>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr style="background-color: #f9f9f9;">
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( '#' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Prize value' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Prize' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Symbol set' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Symbol pieces to Collect' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Extract pieces (EP)' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Value pieces (VP) ' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Free pieces (FP) ' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Set each VP to ' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Set each FP to ' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Total VP' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Total FP' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Price of each EP' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Price of each VP' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Total Price of EP' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Total Price of VP' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Total VP and FP for each symbol queue' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Symbol queue number to insert VP and FP' ); ?></th>
                            <th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Fund receiver plan' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Prizes unlocked' ); ?></th>
							<th nowrap="nowrap" style="text-align:center;">	<?php echo JText::_( 'Prizes unlocked value' ); ?></th>
                          </tr>
                          <?php 
foreach ($this->list_presentations as $rows) { 
?> 
<input type="hidden" id="presentationId" name="presentationId" value="<?php echo $rows->presentation; ?>" >
 <?php
	$symbol = $this->model->getSymbolPresentationList($rows->presentation,$rows->selected_presentation);		

		foreach ($symbol as $simbol){ ?>
                          <tr>
<td style="text-align:center;height:30px;"	>
<input type="checkbox" id="cb<?php echo $j; ?>" name="cid1<?php echo $j; ?>[]"value="<?php echo $rows->id//$rows->selected_presentation; ?>" 																											onclick="Joomla.isChecked(this.checked);">  
</td>
<td style="text-align:center;"	>	<?php echo (!empty($simbol->prize_value) ? '$'. number_format($simbol->prize_value,2) : ''); ?></td>
<td>
<img src="./components/com_awardpackage/asset/prize/<?php echo $simbol->prize_image;?>?>" style="height:30px;" />
</td>		
<td>
<img src="./components/com_awardpackage/asset/symbol/<?php echo $simbol->symbol_image;?>?>" style="height:30px;" />
</td>
<td style="text-align:center;">	<?php echo $simbol->pieces; ?></td>
<td style="text-align:center;">	<?php echo $simbol->extra_from; ?></td>
<td style="text-align:center;">	<?php echo $simbol->extra_to; ?></td>
<td style="text-align:center;">	<?php $free_piece = $simbol->pieces - $simbol->extra_from - $simbol->extra_to; 
echo ( $free_piece < 0 ? 0 : $free_piece);
?></td>
<td style="text-align:center;">	<?php echo $simbol->clone_from; ?></td>
<td style="text-align:center;">	<?php echo $simbol->clone_to; ?></td>
<td style="text-align:center;">	<?php echo $simbol->extra_to * $simbol->clone_from; ?></td>
<td style="text-align:center;">	<?php  $total_fp = ($simbol->pieces - $simbol->extra_from - $simbol->extra_to )* $simbol->clone_to; 
echo ( $total_fp < 0 ? 0 : $total_fp);
?></td>

<td style="text-align:center;"	>	<?php echo (!empty($simbol->prize_value_from) ? '$' . number_format($simbol->prize_value_from,2) : '$0')  ; ?></td>
<td style="text-align:center;"	>	<?php echo (!empty($simbol->prize_value_to) ? '$' . number_format($simbol->prize_value_to,2) : '$0')  ; ?></td>
<td style="text-align:center;"	>	<?php $price_extract = $simbol->extra_from * $simbol->prize_value_from;
echo (!empty($price_extract) ? '$' . number_format($price_extract,2) : '$0')  ; ?></td>
<td style="text-align:center;"	>	<?php $price_vpc = $simbol->extra_to * $simbol->prize_value_to;
echo (!empty($price_vpc) ? '$' . number_format($price_vpc,2) : '$0')  ; ?></td>

<td style="text-align:center;"	><?php 
$vpc = $simbol->extra_to * $simbol->clone_from; 
$fpc = ($simbol->pieces - $simbol->extra_from - $simbol->extra_to )* $simbol->clone_to;
$shuffle = $vpc + $fpc;
echo $shuffle; ?></td>
<td style="text-align:center;"	><?php echo '1-' . $shuffle; ?></td>	

<td style="text-align:center;height:30px;"	>
    <?php
    $getPrize = $this->model->getPrizeByStatus($simbol->prize_name, $status,JRequest::getVar('package_id'));
    $getPrizeUnlock = $this->model->getPrizeByUnlock($simbol->prize_name, $status,JRequest::getVar('package_id'));
    $fundRecieverCount = $this->model->getFundReceiverCount($rows->id,$rows->presentation);
$result = $this->model->getFundReceiverListQueue1(JRequest::getVar('process_id'),JRequest::getVar('package_id'),JRequest::getVar('cbfilter'));
    $count=0;
    $remaining=0;
            foreach ($result as $row):
                $valueFunded = $row->amount;//0;
                $prizevalue = $row->prize_value;
                if($valueFunded > $prizevalue){
                    $valueFunded1 = $valueFunded -  $remaining;
                    $remaining += $valueFunded - $prizevalue   ;
                    $valueFunded = $valueFunded1 - $remaining;
                }
                else{
                    $remaining +=  $valueFunded - $prizevalue ;
                }
                if($valueFunded < 0 ) $valueFunded =0;
                $shortfall = $prizevalue - $valueFunded ;
                $rate = $valueFunded*100/$prizevalue;
                $inserted = $valueFunded >= $prizevalue;
                if($rows->fund_prize == $row->prize_id){
                    if($inserted == 100){
                        $count++;
                    }
                }
            endforeach;
    ?>
<a target='_blank' href="index.php?option=com_awardpackage&task=aprocesspresentation.get_fundreceiver_list&package_id=<?php echo JRequest::getVar('package_id').'&receiver='.$rows->id.'&presentation='.$rows->presentation.'&prize='.$simbol->prize_value; ?>"><?php echo sizeof($fundRecieverCount); ?></a></span></td>
<!--	<a target='_blank' href="index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id=--><?php //echo JRequest::getVar('package_id'); ?><!--">--><?php //echo $this->presentations['0']->total_fund_counter;//count($getPrize); ?><!--</a></span></td>-->
<td style="text-align:center;"	><?php echo $count; // count($getPrizeUnlock ); ?></td>
<td style="text-align:center;"	><?php echo ($simbol->prize_value != null ? '$' . $simbol->prize_value	: ''); ?></td>
                          </tr>
                          <?php }
						  }
						  ?>
                        </table>                      
                        <!--end long table-->
                        </td>
                        </tr>
                        </table>
                        
    
                                   </div>
                                   </div>
                                   </div>
                                   

      
                      
