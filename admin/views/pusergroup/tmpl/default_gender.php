<form
	action="index.php?option=com_awardpackage&view=pusergroup&package_id=<?php echo $this->package_id; ?>"
	method="post" name="adminForm" id="adminForm"
	class="form-validate">
<input type="hidden" name="command" value="<?php echo JRequest::getVar('command') ?>">
<input type="hidden" name="package_id" value="<?php echo  JRequest::getVar("package_id"); ?>"/>
<input type="hidden" name="var_id" value="<?php echo JRequest::getVar('var_id');?>"/>
<input type="hidden" name="criteria_id" value="<?php echo JRequest::getVar('criteria_id');?>"/>
<input type="hidden" name="filter_order" value="<?php if($this->lists['order']) echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php if($this->lists['order_dir']) echo $this->lists['order_dir']; ?>" />
<input type="hidden" name="task" id="task" value="save_usergroup" /> 
<input type="hidden" name="user_selected" id="user_selected" value=""/>
<?php
//$presentationGroups = $this->ug->getNameUserGroupPresentation($this->package_id, 'gender');
/* Above code line changed to below code by Sushil on 30-11-2015 */
$presentationGroups = $this->ug->getNameUserGroupPresentation($this->package_id, 'gender',JRequest::getVar('criteria_id'));
$presentationGroup = null;
if(!empty($presentationGroups)){
	$presentationGroup = $presentationGroups[0];
}

$parents = $this->ug->getParentUserGroup($this->package_id,'gender');
$parent = null;
if(!empty($parents)){
	$parent = $parents[0];
}
?>


<table width="100%" cellpadding="1" cellspacing="1"
	class="table-striped"
	style="border: 1px solid #cccccc; font-size: 10pt;">
	<thead>
		<tr>
			<td colspan="3" align="center" height="50" class="td-group"
				style="text-align: center; background-color: #CCCCCC"><strong><?php echo JText::_('COM_REFUND_TAB_GENDER'); ?></strong></td>
		</tr>
		<tr>
			<th align="center"><?php echo JText::_('COM_REFUND_LBL_POPULATION'); ?></th>
			<th><?php echo JText::_('COM_REFUND_TAB_GENDER'); ?></th>
			<th><?php echo JText::_('COM_REFUND_LBL_TO_AC'); ?></th>
		</tr>
	</thead>
	<?php
	$rows = $this->ug->filter_field($this->package_id,'gender');
	?>
	<tbody>
	<?php
	foreach ($rows as $row):
	?>
		<tr>
			<td align="center"><?php echo $row->population;?> %</td>
			<td align="center"><?php echo $row->gender;?>&nbsp;</td>
			<td align="center">
            <?php if ($this->published == 1) { ?>
            <a href="javascript: clickA()"><?php echo JText::_('COM_REFUND_EDIT'); ?></a>&nbsp;&nbsp;
			 <a href="javascript: clickA()"><?php echo JText::_('COM_REFUND_DELETE'); ?></a>
			<?php }else { ?>
            <a href="<?php echo JRoute::_('index.php?option=com_awardpackage&controller=pusergroup&field=gender&task=edit&package_id=' . $this->package_id . '&criteria_id=' . $row->criteria_id  . '&command=' . JRequest::getVar('command')); ?>"><?php echo JText::_('COM_REFUND_EDIT'); ?></a>&nbsp;&nbsp;
			<a href="<?php
           if(JRequest::getVar('command') == '1'){$deltask = "deletepres";} else {$deltask = "delete";} 
           echo JRoute::_('index.php?option=com_awardpackage&controller=pusergroup&task='.$deltask.'&field=gender&package_id=' . $this->package_id . '&criteria_id=' . $row->criteria_id  . '&command=' . JRequest::getVar('command')); ?>"
				onclick="return window.confirm('Are you sure?');"><?php echo JText::_('COM_REFUND_DELETE'); ?></a>
                <?php } ?>
			<?php if( JRequest::getVar('command') == '1' ) { ?>
      &nbsp;&nbsp;
			<?php
/*              if($row->is_presentation!="1"){
                $linkcheckbox = JRoute::_("index.php?option=com_awardpackage&task=pusergroup.centang&field=gender&package_id=" . $this->package_id . '&id=' . $row->criteria_id);
                $cs="";
              } else {
                $linkcheckbox = JRoute::_("index.php?option=com_awardpackage&task=pusergroup.uncentang&field=gender&package_id=" . $this->package_id . '&id=' . $row->criteria_id);
                $cs="checked"; 
              };*/
                if(!in_array($row->criteria_id,$this->selectedCriteriaId)){
                    $linkcheckbox = JRoute::_("index.php?option=com_awardpackage&task=pusergroup.centang&field=name&package_id=" . $this->package_id . '&criteria_id='.$this->criteria_id.'&id=' . $row->criteria_id);
                    $cs="";
                } else {
                    $linkcheckbox = JRoute::_("index.php?option=com_awardpackage&task=pusergroup.uncentang&field=name&package_id=" . $this->package_id . '&criteria_id='.$this->criteria_id.'&id=' . $row->criteria_id);
                    $cs="checked";
                };
              echo "&nbsp;&nbsp;<input type=checkbox id='cbl{$row->criteria_id}' name='cbl[]' value='1' onchange='javascript:window.location=\"$linkcheckbox\"' $cs>";
      ?>
      <!--
      <a
				href="<?php echo JRoute::_('index.php?option=com_awardpackage&view=apresentationlist&task=apresentationlist.initiate&package_id='.$this->package_id.'&idUserGroupsId='.$row->criteria_id.'&processPresentation='.JRequest::getVar('processPresentation')); ?>"
				style="color:blue;text-decoration:underline;"><?php echo JText::_('Select'); ?></a>
			-->
      <?php } ?>
			</td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>
<div><input type="hidden" name="jform[package_id]"
	value="<?php echo $this->package_id; ?>" /> <input type="hidden"
	name="jform[criteria_id]" value="<?php echo $this->criteria_id; ?>" />
<input type="hidden" name="option" value="com_awardpackage" /><input
	type="hidden" name="controller" value="pusergroup" /> <input
	type="hidden" value="gender" name="jform[field]"> <input type="hidden"
	name="command" value="<?php echo ($command == '1' ? '1' : '0'); ?>"> <?php echo JHtml::_('form.token'); ?>
</div>
</form>
