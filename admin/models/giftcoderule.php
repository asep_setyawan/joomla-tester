<?php
/**
 * @version		$Id: categories.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Quiz
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
require_once JPATH_SITE.'/components/com_cjlib/tree/nestedtree.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/communitysurveys.php';
class AwardpackageModelGiftcoderule extends JModelLegacy {
	
	function __construct() {
		 
		parent::__construct();
	}
	
	
	function get_categories($node=0){
	
		$query = 'SELECT * FROM #__giftcode_category group by category_id';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  
	}
	
	function get_rules($package_id){
		$query = 'SELECT * FROM #__ap_free_giftcode_rules where `package_id`='.$package_id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	
	function get_rule_details_by_id($rid=""){
		$query = 'SELECT * FROM #__ap_free_giftcode_rules where id="'.$rid.'"';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	function get_rule_settings_by_rule_id($rid=""){
		$query = 'SELECT * FROM #__ap_free_giftcode_rules_settings where rule_id="'.$rid.'"';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	
	function delete($POST){
		if(!empty($POST))
		{
			foreach($POST['cid'] as $key=>$value)
			{
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				// delete all custom keys for user 1001.
				$conditions = array(
					$db->quoteName('rule_id') . ' = ' . $db->quote($value)
				);
				$query->delete($db->quoteName('#__ap_free_giftcode_rules_settings'));
				$query->where($conditions);
				$db->setQuery($query);
				$result = $db->execute(); 
				if($result)
				{
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					// delete all custom keys for user 1001.
					$conditions = array(
						$db->quoteName('id') . ' = ' . $db->quote($value)
					);
					$query->delete($db->quoteName('#__ap_free_giftcode_rules'));
					$query->where($conditions);
					$db->setQuery($query);
					$result = $db->execute(); 
					return $result;
				}  
			}
		}
		
	}
	

}
?>