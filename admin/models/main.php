<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class AwardpackageModelMain extends JModelLegacy {

    public function save($data) { 
    	$fields = $this->show_fields('ap_awardpackages');
    	foreach ($data as $k => $v) {
            if (in_array($k, $fields)) {
                $input[$k] = htmlentities($v);
            }
        }
        $db = JFactory::getDBO();
        if ($input['package_id'] > 0) {
            $query = "UPDATE `#__ap_awardpackages` SET package_name = '$input[package_name]', start_date = '$input[start_date]',
			end_date = '$input[end_date]', published = '$input[published]' WHERE package_id = '$input[package_id]'";
            $db->setQuery($query);
            $db->query();
        } else {
            unset($input['package_id']);
            $query = "INSERT INTO `#__ap_awardpackages` (" . implode(',', array_keys($input)) . ") VALUES ('" . implode('\',\'', array_values($input)) . "')";
            $db->setQuery($query);
            $db->query();
            $package_id = $db->insertid();
            $this->create_setting($db->insertid());
            $this->creategiftcodecat($package_id);
        }
    }

    public function doclone($package_id) {

        $package = $this->info($package_id);

        unset($package->package_id);

        $package->package_name = $package->package_name . ' (clone)';

        $package->created = date("Y-m-d H:i:s");

        foreach ($package as $k => $v) {

            $key[] = $k;

            $value[] = $v;
        }
        $db = JFactory::getDBO();

        $query = "INSERT INTO `#__ap_awardpackages` (" . implode(",", $key) . ") VALUES ('" . implode('\',\'', $value) . "')";
		$db->setQuery($query);

        $db->query();

        $insertid = $db->insertid();
        $idpackage = $insertid;
        if ($insertid > 0) {
            $query = "SELECT * FROM `#__ap_categories` WHERE package_id = '$package_id'";
            $db->setQuery($query);
            $rs = $db->loadObjectList();
            if (count($rs) > 0) {
                foreach ($rs as $k => $v) {
                    $q = "INSERT INTO `#__ap_categories` (package_id,category_id,colour_code,category_name,donation_amount,published)
						VALUES ('$insertid','$v->category_id','$v->colour_code','$v->category_name','$v->donation_amount','$v->published')";
                    $db->setQuery($q);
                    if ($db->query()) {
                        $return = 1;
                    }
                }
            }
        }
        if ($return) {

            //create duplicate symbol prize data
            $this->clonePrize($package_id, $idpackage);

            //create duplicate giftcode category
            $this->cloneGiftcode($package_id, $idpackage);
			
            //create duplicate giftcode collection
            //$this->cloneGiftcodeCollection($package_id, $idpackage);

            //create duplicate giftcode giftcode
            //$this->cloneGiftcodeGiftcode($package_id, $insertid);

            //create duplicate symbol presentation
            $this->cloneSymbolPresentation($package_id, $idpackage);

            //create duplicate symbol symbol
            $this->cloneSymbolSymbol($package_id, $idpackage);

            //create duplicate funding
            $this->cloneFunding($package_id, $idpackage);
			
			//create duplicate survey Category
            $this->cloneSurveyCategory($package_id, $idpackage);

			//create duplicate survey
            $this->cloneSurvey($package_id, $idpackage);

			//create duplicate quiz Category
            $this->cloneQuizCategory($package_id, $idpackage);

			//create duplicate quiz
            $this->cloneQuiz($package_id, $idpackage);

			//create duplicate ContributionRange
            $this->cloneContributionRange($package_id, $idpackage);
			
			//create duplicate ProgressCheck
            $this->cloneProgressCheck($package_id, $idpackage);

			//create duplicate ShoppingCreditCategory
            $this->cloneShoppingCreditCategory($package_id, $idpackage);
			
			//create duplicate ShoppingCreditFromDonation
            $this->cloneShoppingCreditFromDonation($package_id, $idpackage);

			//create duplicate ShoppingCreditPlan
            $this->cloneShoppingCreditPlan($package_id, $idpackage);
			
			//create duplicate ShoppingCreditPlanDetail
            $this->cloneShoppingCreditPlanDetail($package_id, $idpackage);

			//create duplicate SymbolQueueGroup
            $this->cloneSymbolQueueGroup($package_id, $idpackage);
			
			//create duplicate FundReceiverList
            $this->cloneFundReceiverList($package_id, $idpackage);			

			//create duplicate FundReceiver
           // $this->cloneFundReceiver($package_id, $idpackage);			
			
			//create duplicate FundReceiverAge
            $this->cloneFundReceiverAge($package_id, $idpackage);			

			//create duplicate FundReceiverGender
            $this->cloneFundReceiverGender($package_id, $idpackage);			

			//create duplicate FundReceiverLocation
            $this->cloneFundReceiverLocation($package_id, $idpackage);			
			
			//create duplicate FundPrizePlan
            $this->cloneFundPrizePlan($package_id, $idpackage);			

			//create duplicate AwardFundPlan
            $this->cloneAwardFundPlan($package_id, $idpackage);	

			//create duplicate ProcessPresentation
            $this->cloneProcessPresentation($package_id, $idpackage);	

			//create duplicate ProcessPresentationList
            $this->cloneProcessPresentationList($package_id, $idpackage);	
			
			//create duplicate UsergroupPresentation
            $this->cloneUsergroupPresentation($package_id, $idpackage);	

			//create duplicate ap_symbol_process
            $this->cloneApSymbolProcess($package_id, $idpackage);	

            return true;
        }
    }

			//create duplicate ap_symbol_process
    private function cloneApSymbolProcess($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__ap_symbol_process WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__ap_symbol_process (prize_value_from, prize_value_to, clone_from, clone_to, extra_from, extra_to, shuffle_from, shuffle_to, presentation_id, selected_presentation, package_id) VALUES ('" . $row->selected_presentation . "','" .
                        $row->prize_value_from . "','" . $row->prize_value_to . "','" . $row->clone_from . "','" . $row->clone_to . "','" . $row->extra_from . "','" . $row->extra_to . "','" . $row->shuffle_from . "','" . $row->shuffle_to . "','" . $row->presentation_id . "','" . $row->selected_presentation . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }


//            $this->cloneProcessPresentation($package_id, $idpackage);	
    private function cloneProcessPresentation($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__process_presentation WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__process_presentation (selected_presentation, name, presentation, prize_name, prize_value, fund_prize_plan, funding_value_from, funding_value_to, award_fund_plan, award_fund_rate, award_fund_amount, fund_receiver, limit_receiver, symbol_queue, symbol_assign, fund_plan, fund_amount, fund_prize, date_created, package_id) VALUES ('" . $row->selected_presentation. "', '" . $row->name. "', '" . $row->presentation. "', '" . $row->prize_name. "', '" . $row->prize_value. "', '" . $row->fund_prize_plan. "', '" . $row->funding_value_from. "', '" . $row->funding_value_to. "', '" . $row->award_fund_plan. "', '" . $row->award_fund_rate. "', '" . $row->award_fund_amount. "', '" . $row->fund_receiver. "', '" . $row->limit_receiver. "', '" . $row->symbol_queue. "', '" . $row->symbol_assign. "', '" . $row->fund_plan. "', '" . $row->fund_amount. "', '" . $row->fund_prize. "', '" . $row->date_created. "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

//            $this->cloneUsergroupPresentation($package_id, $idpackage);	
    private function cloneUsergroupPresentation($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__usergroup_presentation WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__usergroup_presentation (usergroup, usergroup_id, presentation_id, process_presentation, name, funds, prize, prize_value, symbol,  date_created, package_id) VALUES ('" . $row->usergroup . "','" .
                        $row->usergroup_id . "','" . $row->presentation_id . "','" . $row->process_presentation . "','" . $row->name . "','" . $row->funds . "','" . $row->prize . "','" . $row->prize_value . "','" . $row->symbol . "','" . $row->date_created . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }



//            $this->cloneProcessPresentationList($package_id, $idpackage);	
    private function cloneProcessPresentationList($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__process_presentation_list WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__process_presentation_list (selected_presentation, name, presentation, prize_name, prize_value, fund_amount, fund_prize, date_created, package_id) VALUES ('" . $row->selected_presentation . "','" .
                        $row->name . "','" . $row->presentation . "','" . $row->prize_name . "','" . $row->prize_value . "','" . $row->fund_amount . "','" . $row->fund_prize . "','" . $row->date_created . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }


//            $this->cloneAwardFundPlan($package_id, $idpackage);	
    private function cloneAwardFundPlan($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__award_fund_plan WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__award_fund_plan (name, usergroup, rate, speed, duration, amount, total, spent, remain, date_created, package_id) VALUES ('" .
                        $row->name . "','" . $row->usergroup . "','" . $row->rate . "','" . $row->speed . "','" . $row->duration . "','" . $row->amount . "','" . $row->total . "','" . $row->spent . "','" . $row->remain . "','" . $row->date_created . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }


//            $this->cloneFundPrizePlan($package_id, $idpackage);			
    private function cloneFundPrizePlan($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__fund_prize_plan WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__fund_prize_plan (name, value_from, value_to, date_created, package_id) VALUES ('" .
                        $row->name . "','" . $row->value_from . "','" . $row->value_to . "','" . $row->date_created . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }


	//cloneSymbolQueueGroup($package_id, $idpackage);
    private function cloneSymbolQueueGroup($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__symbol_queue_group WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__symbol_queue_group (name, selected, amount, total, date_created, package_id) VALUES ('" .
                        $row->name . "','" . $row->selected . "','" . $row->amount . "','" . $row->total . "','" . $row->date_created . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

//            $this->cloneFundReceiverList($package_id, $idpackage);			
    private function cloneFundReceiverList($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__fund_receiver_list WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__fund_receiver_list (title, filter, created_time, package_id) VALUES ('" .
                        $row->title . "','" . $row->filter . "','" . $row->created_time . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

//$this->cloneFundReceiverAge($package_id, $idpackage);	
  private function cloneFundReceiverAge($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__fund_receiver_age WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__fund_receiver_age (title, receiver, from_day, to_day, from_month, to_month, from_year, to_year, filter, created_time, package_id) VALUES ('" .
                        $row->title . "','" . $row->receiver . "','" . $row->from_day . "','" . $row->to_day . "','" . $row->from_month . "','" . $row->to_month . "','" . $row->from_year . "','" . $row->to_year . "','" . $row->filter . "','" . $row->created_time . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }
//$this->cloneFundReceiverGender($package_id, $idpackage);	
  private function cloneFundReceiverGender($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__fund_receiver_gender WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__fund_receiver_gender (title, receiver, gender, filter, created_time, package_id) VALUES ('" .
                        $row->title . "','" . $row->receiver . "','" . $row->gender . "','" . $row->filter . "','" . $row->created_time . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

//$this->cloneFundReceiverLocation($package_id, $idpackage);	
  private function cloneFundReceiverLocation($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__fund_receiver_location WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__fund_receiver_location (title, receiver, gender, filter, created_time, package_id) VALUES ('" .
                        $row->title . "','" . $row->receiver . "','" . $row->gender . "','" . $row->filter . "','" . $row->created_time . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneContributionRange($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__contribution_range WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__contribution_range (shopping_credit_plan_id, min_amount, max_amount, uniq_key, package_id) VALUES ('" .
                        $row->shopping_credit_plan_id . "','" . $row->min_amount . "','" . $row->max_amount . "','" . $row->uniq_key . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneProgressCheck($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__progress_check WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__progress_check (shopping_credit_plan_id, every, type, uniq_key, package_id) VALUES ('" .
                        $row->shopping_credit_plan_id . "','" . $row->every . "','" . $row->type . "','" . $row->uniq_key . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneShoppingCreditCategory($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__shopping_credit_category WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__shopping_credit_category (name, published, package_id) VALUES ('" .
                        $row->name . "','" . $row->published . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneShoppingCreditFromDonation($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__shopping_credit_from_donation WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__shopping_credit_from_donation (shopping_credit_plan_id, fee, refund, expire, uniq_key, contribution_range, progress_check, package_id) VALUES ('" .
                        $row->shopping_credit_plan_id . "','" . $row->fee . "','" . $row->refund . "','" . $row->expire . "','" . $row->uniq_key . "','" . $row->contribution_range . "','" . $row->progress_check . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneShoppingCreditPlan($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__shopping_credit_plan WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__shopping_credit_plan (category, sc_plan, published, note, package_id) VALUES ('" .
                        $row->category . "','" . $row->sc_plan . "','" . $row->published . "','" . $row->note . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneShoppingCreditPlanDetail($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__shopping_credit_plan_detail WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__shopping_credit_plan_detail (start_date, end_date, contribution_range, progress_check, uniq_key, package_id) VALUES ('" .
                        $row->start_date . "','" . $row->end_date . "','" . $row->contribution_range . "','" . $row->progress_check . "','" . $row->uniq_key . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneFunding($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__funding WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO #__funding (funding_session,funding_desc,funding_published,funding_created,funding_modify,funding_1st,package_id) VALUES ('" .
                        $row->funding_session . "','" . $row->funding_desc . "','" . $row->funding_published . "','" . $row->funding_created . "','" . $row->funding_modify . "','" .
                        $row->funding_1st . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

	 private function cloneQuizCategory($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__quiz_categories WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();


        if (count($rows) > 0) {


            foreach ($rows as $row) {

                $Query = "INSERT INTO #__quiz_categories (title, alias, package_id) VALUES ('" .
                        $row->title . "','" . $row->alias . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }


	 private function cloneQuiz($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__quiz_quizzes WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();


        if (count($rows) > 0) {


            foreach ($rows as $row) {

                $Query = "INSERT INTO #__quiz_quizzes (title, alias, created_by, created, catid, show_answers, published, duration, multiple_responses, cutoff,  package_id) VALUES ('" .
                        $row->title . "','" . $row->alias . "','" . $row->created_by . "','" . $row->created . "','" . $row->catid . "','" . $row->show_answers . "','" . $row->published . "','" . $row->duration . "','" . $row->multiple_responses . "','" . $row->cutoff . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

	 private function cloneSurveyCategory($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__survey_categories WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();


        if (count($rows) > 0) {


            foreach ($rows as $row) {

                $Query = "INSERT INTO #__survey_categories (title, alias, package_id) VALUES ('" .
                        $row->title . "','" . $row->alias . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }
	
 private function cloneSurvey($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__survey WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();


        if (count($rows) > 0) {


            foreach ($rows as $row) {

                $Query = "INSERT INTO #__survey (title, alias, catid, created_by, created, publish_up, publish_down, private_survey, max_responses, anonymous, public_permissions, survey_key, package_id) VALUES ('" .
                        $row->title . "','" . $row->alias . "','" . $row->catid . "','" . $row->created_by . "','" . $row->created . "','" . $row->publish_up . "','" . $row->publish_down . "','" . $row->private_survey . "','" . $row->max_responses . "','" . $row->anonymous . "','" . $row->public_permissions . "','" . $row->survey_key . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneSymbolSymbol($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__symbol_symbol WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();


        if (count($rows) > 0) {


            foreach ($rows as $row) {

                $Query = "INSERT INTO #__symbol_symbol (date_created,symbol_name,symbol_image,pieces,rows,cols,package_id) VALUES ('" .
                        $row->date_created . "','" . $row->symbol_name . "','" . $row->symbol_image . "','" . $row->pieces . "','" . $row->rows . "','" . $row->cols . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneSymbolPresentation($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__symbol_presentation WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            foreach ($rows as $row) {

                $Query = "INSERT INTO  #__symbol_presentation (presentation_create,presentation_modify,presentation_publish,package_id) VALUES ('" .
                        $row->presentation_create . "','" . $row->presentation_modify . "','" . $row->presentation_publish . "','" . $insertid . "')";

                $db->setQuery($Query);

                $db->query();
            }
        }
    }

    private function cloneGiftcode($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__giftcode_category WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        if (count($rows) > 0) {

            $i = 0;

            $j = 0;

            foreach ($rows as $k => $data) {

                $query = "INSERT INTO #__giftcode_category (name,image,description,published,symbol_pieces_award,created_date_time,color_code,locked,package_id,category_id) VALUES ('" .
                        $data->name . "','" . $data->image . "','" . $data->description . "','" . $data->published . "','" . $data->symbol_pieces_award . "','" . $data->created_date_time . "','" .
                        $data->color_code . "','" . $data->locked . "','" . $insertid . "','" . $data->category_id . "')";

                $db->setQuery($query);

                $db->query();

                $catId = $db->insertid();

                $this->cloneGiftcodeCollection($package_id,$insertid,$data->id,$catId);
            }
        }
    }

		function getGiftcodeCategory($package_id){
        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__giftcode_category WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();
		return $rows;
		}

    private function cloneGiftcodeCollection($package_id, $insertid,$colorId,$catId) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__giftcode_collection WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "' AND " . $db->QuoteName('color_id') . "='" . $colorId . "'";

        $db->setQuery($query);

        $rows2 = $db->loadObjectList();


        $cat = $this->getGiftcodeCategory($insertid);


        if (count($rows2) > 0) {

            $i = 0;

            $j = 0;

            foreach ($rows2 as $k => $data2) {
                /*$i =  ( $i > 6 ? 0 : $i );
                $colorId = $cat[$i];*/

                $query = "INSERT INTO #__giftcode_collection (color_id, total_giftcodes, created_date_time, modified_date_time, published, package_id ) VALUES
				('" . $catId . "','" . $data2->total_giftcodes . "','" . $data2->created_date_time . "','" . $data2->modified_date_time . "','" . $data2->published . "','" . $insertid . "')";

                $db->setQuery($query);

                $db->query();

                $collectionId = $db->insertid();

                $this->cloneGiftcodeGiftcode($package_id,$insertid,$collectionId,$data2->color_id,$colorId);
//			$i++;

            }
        }
    }

    private function cloneGiftcodeGiftcode($package_id, $insertid,$collectionId,$color_id,$catId) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__giftcode_giftcode WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "' ANd " . $db->QuoteName('giftcode_category_id') . "='" . $color_id . "'";

        $db->setQuery($query);

        $rows1 = $db->loadObjectList();


        //$cat = $this->getGiftcodeCategory($insertid);


        if (count($rows1) > 0) {

            $i = 0;

            $j = 0;

            foreach ($rows1 as $k1 => $data1) {
                /*$i =  ( $i > 6 ? 0 : $i );
                $colorId = $cat[$i];*/

                $query = "INSERT INTO #__giftcode_giftcode (giftcode_category_id, giftcode, created_date_time, giftcode_collection_id, published, package_id ) VALUES
				('" . $catId . "','" . $data1->giftcode . "','" . $data1->created_date_time . "','" . $collectionId . "','" . $data1->published . "','" . $insertid . "')";

                $db->setQuery($query);

                $db->query();
                /*$i++;*/

            }
        }
    }

    private function clonePrize($package_id, $insertid) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__symbol_prize WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "'";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        $data = "";

        if (count($rows) > 0) {

            foreach ($rows as $row) {


                $query = "INSERT INTO #__symbol_prize (date_created, prize_name, prize_value, prize_image, created_by, package_id, status ) VALUES ('$row->date_created','$row->prize_name','$row->prize_value','$row->prize_image','$row->created_by', '$insertid','$row->status')";

                $db->setQuery($query);

                $db->query();
            }
        }
    }

    public function create_setting($package_id) {
        $row[] = array('package_id' => $package_id, 'category_id' => 1, 'colour_code' => '#FF0000', 'category_name' => 'RED', 'donation_amount' => 0.1, 'published' => 1, 'poll_price' => 0.1, 'survey_price' => 0.1, 'unlocked' => 0);
        $row[] = array('package_id' => $package_id, 'category_id' => 2, 'colour_code' => '#FF6600', 'category_name' => 'ORANGE', 'donation_amount' => 0.2, 'published' => 1, 'poll_price' => 0.2, 'survey_price' => 0.2, 'unlocked' => 0);
        $row[] = array('package_id' => $package_id, 'category_id' => 3, 'colour_code' => '#FFFF00', 'category_name' => 'YELLOW', 'donation_amount' => 0.3, 'published' => 1, 'poll_price' => 0.3, 'survey_price' => 0.3, 'unlocked' => 0);
        $row[] = array('package_id' => $package_id, 'category_id' => 4, 'colour_code' => '#008000', 'category_name' => 'GREEN', 'donation_amount' => 0.4, 'published' => 1, 'poll_price' => 0.4, 'survey_price' => 0.4, 'unlocked' => 0);
        $row[] = array('package_id' => $package_id, 'category_id' => 5, 'colour_code' => '#0000FF', 'category_name' => 'BLUE', 'donation_amount' => 0.5, 'published' => 1, 'poll_price' => 0.5, 'survey_price' => 0.5, 'unlocked' => 0);
        $row[] = array('package_id' => $package_id, 'category_id' => 6, 'colour_code' => '#000080', 'category_name' => 'INDIGO', 'donation_amount' => 0.6, 'published' => 1, 'poll_price' => 0.6, 'survey_price' => 0.6, 'unlocked' => 0);
        $row[] = array('package_id' => $package_id, 'category_id' => 7, 'colour_code' => '#800080', 'category_name' => 'VIOLET', 'donation_amount' => 0.7, 'published' => 1, 'poll_price' => 0.7, 'survey_price' => 0.7, 'unlocked' => 0);
        $db = JFactory::getDBO();
        for ($i = 0; $i <= count($row) - 1; $i++) {
            //echo $row[$i]['category_id'].'<br>';
            $query = "INSERT INTO `#__ap_categories` (package_id,category_id,colour_code,category_name,donation_amount,poll_price, survey_price) 
						  VALUES('" . $row[$i]['package_id'] . "','" . $row[$i]['category_id'] . "','" . $row[$i]['colour_code'] . "','" . $row[$i]['category_name'] . "','" . $row[$i]['donation_amount'] . "','" . $row[$i]['poll_price'] . "','" . $row[$i]['survey_price'] . "')";
            $db->setQuery($query);
            $db->query();
        }
    }

    function delete($data) {
        $db = JFactory::getDBO();
        $query = "DELETE FROM `#__ap_awardpackages` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();

        //delete award package categori
        $query = "DELETE FROM `#__ap_categories` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();

        //update award package account 
        $query = "UPDATE `#__ap_useraccounts` SET package_id = 0 WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();

        //delete gifcode category 
        $query = "DELETE FROM `#__giftcode_category` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
        
        //delete giftcode giftcode collection
        $query = "DELETE FROM `#__giftcode_collection` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
        
        //delete prize 
        $query = "DELETE FROM `#__symbol_prize` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
        
        //delete presentation 
        $query = "DELETE FROM `#__symbol_presentation` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
        
        //delete symbol 
        $query = "DELETE FROM `#__symbol_symbol` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
        
        //delete funding 
        $query = "DELETE FROM `#__funding` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();

        //delete quiz_quizzes
        $query = "DELETE FROM `#__quiz_quizzes` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();

		 //delete quiz_categories 
        $query = "DELETE FROM `#__quiz_categories` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
		
		      //delete survey 
        $query = "DELETE FROM `#__survey` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();

			 //delete survey_categories
        $query = "DELETE FROM `#__survey_categories` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
		
			 //delete giftcode_collection 
        $query = "DELETE FROM `#__giftcode_collection` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
		
			 //delete giftcode_collection 
        $query = "DELETE FROM `#__ap_symbol_process` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();				
		
			 //delete contribution_range
        $query = "DELETE FROM `#__contribution_range` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
		
			 //delete progress_check 
        $query = "DELETE FROM `#__progress_check` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
			
			 //delete shopping_credit_category
        $query = "DELETE FROM `#__shopping_credit_category` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
			
			 //delete shopping_credit_from_donation 
        $query = "DELETE FROM `#__shopping_credit_from_donation` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
			
			 //delete shopping_credit_plan 
        $query = "DELETE FROM `#__shopping_credit_plan` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
			
			 //delete shopping_credit_plan_detail
        $query = "DELETE FROM `#__shopping_credit_plan_detail` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
		
            //create duplicate giftcode giftcode
        $query = "DELETE FROM `#__giftcode_giftcode` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate SymbolQueueGroup
        $query = "DELETE FROM `#__symbol_queue_group` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
			
			//create duplicate FundReceiverList
        $query = "DELETE FROM `#__fund_receiver_list` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate FundReceiver
        $query = "DELETE FROM `#__fund_receiver` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
			
			//create duplicate FundReceiverAge
        $query = "DELETE FROM `#__fund_receiver_age` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate FundReceiverGender
        $query = "DELETE FROM `#__fund_receiver_gender` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate FundReceiverLocation
        $query = "DELETE FROM `#__fund_receiver_location` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
			
			//create duplicate FundPrizePlan
        $query = "DELETE FROM `#__fund_prize_plan` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate AwardFundPlan
        $query = "DELETE FROM `#__award_fund_plan` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate ProcessPresentation
        $query = "DELETE FROM `#__process_presentation` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		

			//create duplicate ProcessPresentationList
        $query = "DELETE FROM `#__process_presentation_list` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		
			
			//create duplicate UsergroupPresentation
        $query = "DELETE FROM `#__usergroup_presentation` WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();		


    }

    function publish($data) {
        $db = JFactory::getDBO();
        $query = "UPDATE `#__ap_awardpackages` SET published = 1 WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
    }

    function unpublish($data) {
        $db = JFactory::getDBO();
        $query = "UPDATE `#__ap_awardpackages` SET published = 0 WHERE package_id IN (" . implode(',', $data) . ")";
        $db->setQuery($query);
        $db->query();
    }

    function show_fields($table) {    	    	
        $db = JFactory::getDBO();      
        $db->setQuery("SHOW FIELDS FROM #__" . $table);
        $fields = $db->loadColumn();
        return $fields;
    }

    function info($package_id, $array = false) {
        $db = JFactory::getDBO();
        $db->setQuery("SELECT * FROM #__ap_awardpackages WHERE package_id = '$package_id' LIMIT 1");
        if ($array) {
            $row = $db->loadAssoc();
        } else {
            $row = $db->loadObject();
        }
        return $row;
    }

    function save_settings($post) {
        $db = & JFactory::getDBO();
        foreach ($post as $key => $value) {
            //echo $key .' => '.$value.'<br>';
            $query = "REPLACE INTO `#__ap_donation_variables` (name,value) VALUES ('$key','$value')";
            $db->setQuery($query);
            $db->query();
        }
        $message = JText::_($query);
        JFactory::getApplication()->enqueueMessage($message, 'error');
    }

    function save_categories() {
        $db = & JFactory::getDBO();
        for ($i = 0; $i <= count(JRequest::getVar('category_id')) - 1; $i++) {
            $query = "UPDATE `#__ap_categories` SET 
				colour_code = '" . $_POST['colour_code'][$i] . "',
				category_name = '" . $_POST['category_name'][$i] . "' 
				WHERE category_id = '" . $_POST['category_id'][$i] . "'";
            $db->setQuery($query);
            $db->query();
        }
    }

    function invar($name, $value) {
        $db = & JFactory::getDBO();
        $query = "SELECT * FROM `#__ap_donation_variables` WHERE name = '$name' LIMIT 1";
        $db->setQuery($query);
        $rs = $db->loadObject();
        if ($rs->name) {
            if ($rs->value) {
                $result = $rs->value;
            } else {
                $result = $value;
            }
            return $result;
        } else {
            return $value;
        }
    }

    /* Create by kadeyasa@gmail.com */

    function creategiftcodecat($package_id) {
        $db = $this->getDBO();
        $dateYear = date('Y-m-d');
        $row[] = array('package_id' => $package_id, 'name' => 'Red', 'image' => 'red.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#FF0000', 'locked' => '1', 'category_id' => '1');
        $row[] = array('package_id' => $package_id, 'name' => 'Orange', 'image' => 'orange.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#FF6600', 'locked' => '1', 'category_id' => '2');
        $row[] = array('package_id' => $package_id, 'name' => 'Yellow', 'image' => 'yellow.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#FFFF00', 'locked' => '1', 'category_id' => '3');
        $row[] = array('package_id' => $package_id, 'name' => 'Green', 'image' => 'green.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#008000', 'locked' => '1', 'category_id' => '4');
        $row[] = array('package_id' => $package_id, 'name' => 'Blue', 'image' => 'blue.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#0000FF', 'locked' => '1', 'category_id' => '5');
        $row[] = array('package_id' => $package_id, 'name' => 'Dark Blue', 'image' => 'dark blue.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#000080', 'locked' => '1', 'category_id' => '6');
        $row[] = array('package_id' => $package_id, 'name' => 'Purple', 'image' => 'purple.jpg', 'description' => 'unique symbol', 'published' => '1', 'symbol_pieces_award' => '', 'created_date_time' => $dateYear, 'color_code' => '#800080', 'locked' => '1', 'category_id' => '7');

        if (count($row) > 0) {
            for ($c = 0; $c < count($row); $c++) {
                $query = "INSERT INTO #__giftcode_category (package_id,name,image,description,published,created_date_time,color_code,locked,category_id) VALUES ('" . $row[$c]['package_id'] . "','" . $row[$c]['name'] . "','" . $row[$c]['image'] . "','" .
                        $row[$c]['description'] . "','" . $row[$c]['published'] . "','" . $row[$c]['created_date_time'] . "','" . $row[$c]['color_code'] . "','" . $row[$c]['locked'] . "','" . $row[$c]['category_id'] . "')";

                $db->setQuery($query);

                $db->query();
            }
        }
    }

    public function getGiftCode($package_id) {

        $db = &JFactory::getDBO();

        $query = "SELECT * FROM #__giftcode_category WHERE " . $db->QuoteName('package_id') . "='" . $package_id . "' ORDER BY id ASC LIMIT 0,1";

        $db->setQuery($query);

        $rows = $db->loadObjectList();

        foreach ($rows as $row) {

            return $row;
        }
    }
	
	public function CheckPackageExpired($package_id){
		$now 	= date('Y-m-d');
		$db		= &JFactory::getDBO();
		$query	= $db->getQuery(TRUE);
		$query->select('*');
		$query->from($db->QuoteName('#__ap_awardpackages'));
		$query->where("start_date<='".$now."'");
		$query->where("end_date>='".$now."'");
		$query->where("package_id='".$package_id."'");
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
	}
}
