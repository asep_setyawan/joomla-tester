<?php
/**
 * @version		$Id: categories.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Quiz
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
require_once JPATH_SITE.'/components/com_cjlib/tree/nestedtree.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/communitysurveys.php';
class AwardpackageModelGiftcoderulerewardslist extends JModelLegacy {
	
	function __construct() {
		 
		parent::__construct();
	}
		function get_all_free_gift_rewards_list($package_id){		
			$result=[];		
			if(!empty($package_id))		
			{			
				$query = 'SELECT reward_list.*,rule_list.title as rule_title,user_group.title as group_title FROM #__ap_free_gift_code_rewards_list as reward_list join #__ap_free_giftcode_rules as rule_list on rule_list.id=reward_list.rule_id join  #__ap_free_giftcode_groups as user_group on user_group.id=reward_list.user_group_id where reward_list.`package_id`='.$package_id;			
				$this->_db->setQuery($query);			
				$result = $this->_db->loadObjectList();		
			
			}		
			return $result;  			
		}
		
		function change_reward_status($reward_id,$status){		
			$result=[];
		
			// Create an object for the record we are going to update.
			$object = new stdClass();
			 
			// Must be a valid primary key value.
			$object->id = $reward_id;
			$object->status = $status;
			 
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__ap_free_gift_code_rewards_list', $object, 'id');

			return $result;  			
		}
		
		
		
		function update_reward_status_to_expire(){		
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);		 
			// Fields to update.
			$fields = array(
				$db->quoteName('status') . '=3'
			);
			 
			// Conditions for which records should be updated.
			$conditions = array(
				$db->quoteName('end_date') . ' < ' . $db->quote(date('Y-m-d'))
			);
			 
			$test=$query->update($db->quoteName('#__ap_free_gift_code_rewards_list'))->set($fields)->where($conditions);
			//echo $test;die;
			$db->setQuery($query);
			
			$result = $db->execute(); 	
			return $result;  			
		}

	
	function get_rules($package_id){
		$query = 'SELECT * FROM #__ap_free_giftcode_rules where `package_id`='.$package_id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	function get_free_usergroups($package_id){
		$query = 'SELECT * FROM #__ap_free_giftcode_groups where `package_id`='.$package_id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	
	function get_rule_details_by_id($rid=""){
		$query = 'SELECT * FROM #__ap_free_giftcode_rules where id="'.$rid.'"';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	function get_rule_settings_by_rule_id($rid=""){
		$query = 'SELECT * FROM #__ap_free_giftcode_rules_settings where rule_id="'.$rid.'"';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;  		
	}
	
	
	function delete($POST){
		if(!empty($POST))
		{
			foreach($POST['cid'] as $key=>$value)
			{
				
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					// delete all custom keys for user 1001.
					$conditions = array(
						$db->quoteName('id') . ' = ' . $db->quote($value)
					);
					 $query->delete($db->quoteName('#__ap_free_gift_code_rewards_list'));
					$query->where($conditions);
					$db->setQuery($query);
					$result = $db->execute(); 	
			}
		}
		
	}
	

}
?>