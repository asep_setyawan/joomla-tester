<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');
require_once JPATH_COMPONENT . '/helpers/awardpackages.php';

class AwardpackageControllerPusergroup extends AwardpackageController
{
	function __construct(){ 
		parent::__construct();
	}
	function display($cachable = false, $urlparams = array()) {
		JRequest::setVar('view', 'pusergroup');
		parent::display();
	}

	function test(){
		$model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
		$query = "t.id = u.id AND email LIKE '%gmail%' ";
		echo $model->get_total($query);
	}

	function categories(){
		$app = JFactory::getApplication();
		$app->redirect('?option=com_awardpackage&controller=donation&task=categories&package_id='.JRequest::getVar('package_id').'&var_id='.JRequest::getVar('var_id'));
	}

	function donation(){
		$this->setRedirect('index.php?option=com_awardpackage&controller=donation&task=transaction&package_id='.JRequest::getVar('package_id').'&var_id='.JRequest::getVar('var_id'), $msg);
	}

	function delete(){
		$model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
		$model->delete(JRequest::getVar('criteria_id'),JRequest::getVar('package_id'));
		$app = JFactory::getApplication();
		$app->redirect('index.php?option=com_awardpackage&view=pusergroup&package_id='.JRequest::getVar('package_id').'&field='.JRequest::getVar('field') . '&command=' . JRequest::getVar('command').'&var_id='.JRequest::getVar('var_id'),'Deleted...');
	}

  function get_usergroup(){
		$package_id = JRequest::getVar('package_id');
		$criteria_id = JRequest::getVar('criteria_id');
		$title = JRequest::getVar('title');
		$this->setRedirect('index.php?option=com_awardpackage&view=apresentationlist&task=apresentationlist.initiate&package_id='.JRequest::getVar('package_id').'&idProcess='.JRequest::getVar('sname').'&idProcessValue='.JRequest::getVar('value_from').'&command=1'.'&process_id='.$process_id, JText::_('MSG_SUCCESS'));
		
		//$this->setRedirect('index.php?option=com_awardpackage&view=apresentationlist&task=apresentationlist.initiate&package_id='.JRequest::getVar('package_id'),JTEXT::_('Success add user group'));
	}
	
	function save_usergroup(){
		$model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
		$package_id = JRequest::getVar('package_id');
		$title = JRequest::getVar('title');
		$idUserGroupsId= JRequest::getVar('criteria_id');
		$var_id= JRequest::getVar('var_id');	
		$cids = JRequest::getVar('cid');
		
		if (!empty($title)){
			$model->updateUserGroup($title,$packageId);
		}			     
		
		foreach ($cids as $cid) {
			$model->UpdateUserGroupPresentation($cid);
		}
		
		$this->setRedirect('index.php?option=com_awardpackage&view=apresentationlist&task=apresentationlist.initiate&package_id=' .$package_id . '&title=' . $title . '&idUserGroupsId=' . $idUserGroupsId . '&var_id=' . $var_id. '&command=1', JText::_('MSG_SUCCESS'));
		}
		/*if(JRequest::getVar('criteria_id') !='') {
			$criteria_id = $model->insert_criteria($data);exit;
			if($criteria_id == -1){
				if(JRequest::getVar('command') == '1') {
					$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&package_id='.$data->package_id.'&field='.$data->field.'&command=1&var_id='.JRequest::getVar('var_id'), 'Your group is already registered');
				} else {
					$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&package_id='.JRequest::getVar('package_id').'&field='.$data->field,'Your group is already registered');
				}
			} else {			
					if($criteria_id == -1) {
						$msg = "Your group is already registered";
					} else
					if($criteria_id == -2){
						$msg = "Saved...";
					} else
					if($criteria_id == -3){
						$msg = "Different package";
					} else {
						$msg = "Saved...";
					}
					
				if(JRequest::getVar('command') == '1') {
					//$this->setRedirect('index.php?option=com_awardpackage&view=apresentationlist&task=apresentationlist.initiate&package_id='.JRequest::getVar('package_id').'&idUserGroupsId='.$criteria_id,'Successful update user groups');
					$this->setRedirect('index.php?option=com_awardpackage&view=pusergroup&task=edit&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.$data->package_id.'&field='.$data->field.'&command=1&var_id='.JRequest::getVar('var_id').'&title='.JRequest::getVar('title'),  $msg);
				} else {
					$this->setRedirect('index.php?option=com_awardpackage&view=pusergroup&task=edit&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.$data->package_id.'&field='.$data->field, $msg);
				}
			}
		} else {
			if(JRequest::getVar('criteria_id') != 0) {
				$criteria_id = $model->update_criteria_2($data, JRequest::getVar('criteria_id'), $data->package_id);
				if(JRequest::getVar('command') == '1') {
					$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.$data->package_id.'&field='.$data->field.'&command=1&var_id='.JRequest::getVar('var_id'), $msg);
				} else {
					$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.$data->package_id.'&field='.$data->field, $msg);
				}
			} else {
				if($data->criteria_id){
					$criteria_id = $model->update_criteria($data);
				}else{
					$criteria_id = $model->insert_criteria($data);
					
					if($criteria_id == -1) {
						$msg = "Your group is already registered";
					} else
					if($criteria_id == -2){
						$msg = "Only one group for one package";
					} else
					if($criteria_id == -3){
						$msg = "Please create main user group first";
					} else {
						$msg = "Saved...";
					}
					if(JRequest::getVar('command') == '1') {
						$this->setRedirect('index.php?option=com_awardpackage&view=pusergroup&task=edit&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.$data->package_id.'&field='.$data->field.'&command=1&var_id='.JRequest::getVar('var_id'), $msg);
					} else {
						$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.$data->package_id.'&field='.$data->field, $msg);
					}
				}
			}
		}
	}*/

	function addNewUserEmail(){
		$package_id = JRequest::getVar('package_id');
		$account_id = JRequest::getVar('user_selected');
		$model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
		$model->updatePackageForUserAccounts($package_id, substr($account_id, 0, strlen($account_id)-1));
		$msg = "Successfull update package account";
		$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.JRequest::getVar('package_id').'&field=email&command=1&var_id='.JRequest::getVar('var_id'), $msg);
	}

	function addNewUserName(){
		$package_id = JRequest::getVar('package_id');
		$account_id = JRequest::getVar('user_selected');
		$model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
		$model->updatePackageForUserAccounts($package_id, substr($account_id, 0, strlen($account_id)-1));
		$msg = "Successfull update package account";
		$this->setRedirect('index.php?option=com_awardpackage&controller=pusergroup&criteria_id='.JRequest::getVar('criteria_id').'&package_id='.JRequest::getVar('package_id').'&field=name&command=1&var_id='.JRequest::getVar('var_id'), $msg);
	}
	
	function save_create(){
		$package_id = JRequest::getVar('package_id');
		$title = JRequest::getVar('title');
		$idUserGroupsId= JRequest::getVar('criteria_id');
		$var_id= JRequest::getVar('var_id');				     
		$this->setRedirect('index.php?option=com_awardpackage&view=apresentationlist&task=apresentationlist.initiate&package_id=' .$package_id . '&title=' . $title . '&idUserGroupsId=' . $idUserGroupsId . '&var_id=' . $var_id. '&command=1', JText::_('MSG_SUCCESS'));
	}
	
	function centang(){
      $id = JRequest::getInt("id");
      $package_id = JRequest::getInt("package_id");
      $field=JRequest::getVar("field");
$criteria_id =JRequest::getVar('criteria_id');
      $model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
      $criteria_data = $model->getCriteriaData($id);
      $str="package_id=$package_id";
      if($criteria_data->field == "name"){
          $str.= " and firstname = '".$criteria_data->firstname."'";
          $str.= " and lastname = '".$criteria_data->lastname."'";
        }
        if($criteria_data->field == "email"){
            $str.= " and email = '".$criteria_data->email."'";
        }
        if($criteria_data->field == "gender"){
            $str.= " and gender = '".$criteria_data->gender."'";
        }
        if($criteria_data->field == "location"){
            $str.= " and street = '".$criteria_data->street."'";
            $str.= " and city = '".$criteria_data->city."'";
            $str.= " and state = '".$criteria_data->state."'";
            $str.= " and post_code = '".$criteria_data->post_code."'";
            $str.= " and country = '".$criteria_data->country."'";
        }

        $str.=' and is_active=1 and (is_presentation IS NULL or is_presentation=0)';
//        echo $str;die;
        $user_data=$model->getUsersCriteria($str);
        $model->addCriteria($criteria_id,$id);
//        echo $str."<pre>";print_r($user_data);die;
        foreach($user_data as $user){
            if($criteria_data->field == "age"){
                $date = new DateTime($user->birthday);
                $now = new DateTime();
                $interval = $now->diff($date);
                $age= $interval->y;
                if($age>= $criteria_data->from_age && $age<=$criteria_data->to_age ){
                    ViewdbHelper::updatedb("#__ap_useraccounts","is_presentation='1' , pug_id='".JRequest::getVar('criteria_id')."'","ap_account_id=$user->ap_account_id");
                }
            }else{
                ViewdbHelper::updatedb("#__ap_useraccounts","is_presentation='1' , pug_id='".JRequest::getVar('criteria_id')."'","ap_account_id=$user->ap_account_id");
            }
        }
      //unset($_SESSION['listlocation'][$id]);
     // ViewdbHelper::updatedb("#__ap_usergroup","is_presentation='1'","criteria_id=$id");
      //$this->setMessage(JText::_('Check Success!'));
      $this->setRedirect(JRoute::_("index.php?option=com_awardpackage&command=1&criteria_id=".JRequest::getVar('criteria_id')."&field=$field&view=pusergroup&package_id=" . $package_id, false));
    }
    function uncentang(){
      $id = JRequest::getInt("id");
      $package_id = JRequest::getInt("package_id");
      $field=JRequest::getVar("field");
        $criteria_id =JRequest::getVar('criteria_id');
        $model = JModelLegacy::getInstance('pusergroup','AwardpackageModel');
        $criteria_data = $model->getCriteriaData($id);
        $str="package_id=$package_id";
        if($criteria_data->field == "name"){
            $str.= " and firstname = '".$criteria_data->firstname."'";
            $str.= " and lastname = '".$criteria_data->lastname."'";
        }
        if($criteria_data->field == "email"){
            $str.= " and email = '".$criteria_data->email."'";
        }
        if($criteria_data->field == "gender"){
            $str.= " and gender = '".$criteria_data->gender."'";
        }
        if($criteria_data->field == "location"){
            $str.= " and street = '".$criteria_data->street."'";
            $str.= " and city = '".$criteria_data->city."'";
            $str.= " and state = '".$criteria_data->state."'";
            $str.= " and post_code = '".$criteria_data->post_code."'";
            $str.= " and country = '".$criteria_data->country."'";
        }

        $str.=' and is_active=1 and  is_presentation=1 and pug_id="'.$criteria_id.'"';

        $user_data=$model->getUsersCriteria($str);
        $selected_criteria = $model->getSelectedCriteria1($criteria_id,$id);

//        echo "<pre>";
//        print_r($selected_criteria);
//        print_r($user_data);
//        die;

        $count=0;
        foreach($user_data as $user){
            foreach($selected_criteria as $sc){

                if($sc->field == "name"){
                    if($sc->firstname == $user->firstname && $sc->lastname == $user->lastname){
                        $count++;
                    }
                }
                if($sc->field == "email"){
                    if($sc->email == $user->email){
                        $count++;
                    }
                }
                if($sc->field == "gender"){
                    if($sc->gender == $user->gender){
                        $count++;
                    }
                }
                if($sc->field == "location"){
                    if($sc->street == $user->street && $sc->city == $user->city && $sc->state == $user->state && $sc->post_code == $user->post_code && $sc->country == $user->country){
                        $count++;
                    }
                }
                if($sc->field == "age"){
                     $date = new DateTime($user->birthday);
                     $now = new DateTime();
                     $interval = $now->diff($date);
                     $age= $interval->y;
                    if($age>= $sc->from_age && $age<=$sc->to_age ){
                        $count++;
                    }
                }
            }
            if($count==0){
                ViewdbHelper::updatedb("#__ap_useraccounts","is_presentation='0' , pug_id='NULL'","ap_account_id=$user->ap_account_id");
            }
        }


        $model->deleteCriteria($criteria_id,$id);
      //$_SESSION['listlocation'][$id]=$id;
 //     ViewdbHelper::updatedb("#__ap_usergroup","is_presentation=0","criteria_id=$id");
      //$this->setMessage(JText::_('UnCheck Success!'));
      $this->setRedirect(JRoute::_("index.php?option=com_awardpackage&command=1&criteria_id=".JRequest::getVar('criteria_id')."&field=$field&view=pusergroup&package_id=" . $package_id, false));
    }
}