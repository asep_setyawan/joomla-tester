<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');
class AwardpackageControllerFundreceiver extends JControllerLegacy {

	function __construct(){
		parent::__construct();
	}

	public function get_fundreceiver(){	
		$view = $this->getView('fundreceiver', 'html');
		$view->assign('action', 'list');
		$view->display();
	}
	
	public function create_update(){
		$view = $this->getView('fundreceiver', 'html');		
		$view->assign('action', 'create');
		$view->display('create');
	}
	
	public function save_create(){
		$package_id = JRequest::getVar('package_id');
		$id = JRequest::getVar('id');
		$title = JRequest::getVar('title');
		$filter = JRequest::getVar('filter');	
		$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
		if($model->save_create_fund ($id, $package_id, $title, $filter, $receiver)){

			$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar('package_id'), JText::_('MSG_SUCCESS'));
		} else {
			$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar('package_id'), JText::_('Error'));
		}
	}
	
	public function onAddAge(){
		$package_id = JRequest::getVar('package_id');
		$pilih = JRequest::getVar('pilih');
		$title = JRequest::getVar('title');
		$filter = JRequest::getVar('filter');	
		$receiver = JRequest::getVar('receiver');	
		$from_year =JRequest::getVar('from_year');	
		$to_year =JRequest::getVar('to_year');	
		$from_month =JRequest::getVar('from_month');	
		$to_month =JRequest::getVar('to_month');	
		$from_day =JRequest::getVar('from_day');	
		$to_day =JRequest::getVar('to_day');
		$gender = JRequest::getVar('gender');
		$state = JRequest::getVar('state');
		$city = JRequest::getVar('city');
        $street = JRequest::getVar('street');
        $postcode = JRequest::getVar('postcode');
        $country = JRequest::getVar('country');
		
		$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
		$id = JRequest::getVar('id');	

if ($title ==''){
$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"),  JText::_('Empty Title'));
}else {

		if($model->save_create_fund ($id, $package_id, $title, $filter, $receiver)){
		if (empty($id))
		{
			$rows = $model->get_fundreceiver_id();
			foreach ($rows as $row){
				$id = $row->criteria_id;
			}
		}

switch ($pilih){
	case '1':
		$msg = "Fund receiver age setting saved";
		$model->save_update_fundreceiver_age($id, $package_id, $title, $filter, $receiver, $from_year, $to_year, $from_month, $to_month, $from_day, $to_day, $gender, $state, $city, $street, $postcode, $country);
        $tab = '2';
        break;
	case '2':
		$msg = "Fund receiver gender setting saved";
		$model->save_update_fundreceiver_gender($id, $package_id, $title, $filter, $receiver, $from_year, $to_year, $from_month, $to_month, $from_day, $to_day, $gender, $state, $city, $street, $postcode, $country);
        $tab = '3';
        break;
	case '3':
		$msg = "Fund receiver location setting saved";
		$model->save_update_fundreceiver_location($id, $package_id, $title, $filter, $receiver, $from_year, $to_year, $from_month, $to_month, $from_day, $to_day, $gender, $state, $city, $street, $postcode, $country);
        $tab = '4';
        break;
		
		}
		
$id =(!empty(JRequest::getVar("id")) ? JRequest::getVar("id") : $id);	
$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.$id.'&package_id='.JRequest::getVar("package_id").'#tabs-'.$tab,  $msg);
		} 
	}
}
	
	public function onAddGender(){
		$package_id = JRequest::getVar('package_id');
		$title = JRequest::getVar('title');
		$filter = JRequest::getVar('filter');	
		$receiver = JRequest::getVar('receiver');	
		$from_year =JRequest::getVar('from_year');	
		$to_year =JRequest::getVar('to_year');	
		$from_month =JRequest::getVar('from_month');	
		$to_month =JRequest::getVar('to_month');	
		$from_day =JRequest::getVar('from_day');	
		$to_day =JRequest::getVar('to_day');
		$gender = JRequest::getVar('gender');
		$state = JRequest::getVar('state');
		$city = JRequest::getVar('city');
        $street = JRequest::getVar('street');
        $postcode = JRequest::getVar('postcode');
        $country = JRequest::getVar('country');
		$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
		$id = JRequest::getVar('id');	

if ($title ==''){
$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"),  JText::_('Empty Title'));
}else {

		if($model->save_create_fund ($id, $package_id, $title, $filter, $receiver)){
		if (empty($id))
		{
			$rows = $model->get_fundreceiver_id();
			foreach ($rows as $row){
			$id = $row->criteria_id;
						}
		}

		$model->save_update_fundreceiver_gender($id, $package_id, $title, $filter, $receiver, $from_year, $to_year, $from_month, $to_month, $from_day, $to_day, $gender, $state, $city, $street, $postcode, $country);
$id =(!empty(JRequest::getVar("id")) ? JRequest::getVar("id") : $id);	
$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.$id.'&package_id='.JRequest::getVar("package_id"),  JText::_('MSG_SUCCESS'));
		} 
	}
}

	public function onAddLocation(){
		$package_id = JRequest::getVar('package_id');
		$title = JRequest::getVar('title');
		$filter = JRequest::getVar('filter');	
		$receiver = JRequest::getVar('receiver');	
		$from_year =JRequest::getVar('from_year');	
		$to_year =JRequest::getVar('to_year');	
		$from_month =JRequest::getVar('from_month');	
		$to_month =JRequest::getVar('to_month');	
		$from_day =JRequest::getVar('from_day');	
		$to_day =JRequest::getVar('to_day');
		$gender = JRequest::getVar('gender');
		$state = JRequest::getVar('state');
		$city = JRequest::getVar('city');
        $street = JRequest::getVar('street');
        $postcode = JRequest::getVar('postcode');
        $country = JRequest::getVar('country');
		$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
		$id = JRequest::getVar('id');	

if ($title ==''){
$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"),  JText::_('Empty Title'));
}else {

		if($model->save_create_fund ($id, $package_id, $title, $filter, $receiver)){
		if (empty($id))
		{
			$rows = $model->get_fundreceiver_id();
			foreach ($rows as $row){
			$id = $row->criteria_id;
						}
		}

		$model->save_update_fundreceiver_location($id, $package_id, $title, $filter, $receiver, $from_year, $to_year, $from_month, $to_month, $from_day, $to_day, $gender, $state, $city, $street, $postcode, $country);
$id =(!empty(JRequest::getVar("id")) ? JRequest::getVar("id") : $id);	
$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.$id.'&package_id='.JRequest::getVar("package_id"),  JText::_('MSG_SUCCESS'));
		} 
	}
}

	public function publish_list(){
		$return = $this->change_state(1);
		$msg = $return == 1 ? JText::_('MSG_SUCCESS') : ($return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED'));		
		$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar("package_id"), false), $msg);
	}
	
	public function unpublish_list(){
		$return = $this->change_state(0);
		$msg = $return == 1 ? JText::_('MSG_SUCCESS') : ($return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED'));		
		$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar("package_id"), false), $msg);
	}
	
	public function onDeleteAge(){
		$app = JFactory::getApplication();
		$pilih = JRequest::getVar("pilih");
		$ids = $app->input->post->getArray(array('cid'=>'array'));
		if(!empty($ids['cid'])){
			$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
			JArrayHelper::toInteger($ids['cid']);
			if($model->delete_categories_age($ids['cid'])){
						$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-2',  JText::_('MSG_SUCCESS'));
			} else {
				$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-2', false), JText::_('MSG_ERROR'));
			}
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-2', false), JText::_('MSG_NO_ITEM_SELECTED'));
		}
	}
	
	public function onDeleteGender(){
		$app = JFactory::getApplication();
		$pilih = JRequest::getVar("pilih");
		$ids = $app->input->post->getArray(array('cid'=>'array'));
		if(!empty($ids['cid'])){
			$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
			JArrayHelper::toInteger($ids['cid']);
			if($model->delete_categories_gender($ids['cid'])){
						$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-3',  JText::_('MSG_SUCCESS'));
			} else {
				$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-3', false), JText::_('MSG_ERROR'));
			}
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-3', false), JText::_('MSG_NO_ITEM_SELECTED'));
		}
	}

	public function onDeleteLocation(){
		$app = JFactory::getApplication();
		$pilih = JRequest::getVar("pilih");
		$ids = $app->input->post->getArray(array('cid'=>'array'));
		if(!empty($ids['cid'])){
			$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
			JArrayHelper::toInteger($ids['cid']);
			if($model->delete_categories_location($ids['cid'])){
						$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-4',  JText::_('MSG_SUCCESS'));
			} else {
				$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-4', false), JText::_('MSG_ERROR'));
			}
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&id='.JRequest::getVar("id").'&package_id='.JRequest::getVar("package_id").'#tabs-4', false), JText::_('MSG_NO_ITEM_SELECTED'));
		}
	}

	public function delete_fundreceiver(){
		$app = JFactory::getApplication();
		$ids = $app->input->post->getArray(array('cid'=>'array'));
		if(!empty($ids['cid'])){
			$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
			JArrayHelper::toInteger($ids['cid']);
			if($model->delete_categories($ids['cid'])){
				$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar("package_id"), false), JText::_('MSG_SUCCESS'));
			} else {
				$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar("package_id"), false), JText::_('MSG_ERROR'));
			}
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.get_fundreceiver&package_id='.JRequest::getVar("package_id"), false), JText::_('MSG_NO_ITEM_SELECTED'));
		}
	}
	
	public function add_fundreceiver(){
		$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&package_id='.JRequest::getVar("package_id"), null);
	}
	
	public function change_state($state){
		$app = JFactory::getApplication();
		$ids = $app->input->getArray(array('cid'=>'array'));		
		if(!empty($ids['cid'])){				
			$model = & JModelLegacy::getInstance( 'fundreceiver', 'AwardpackageModel' );
			JArrayHelper::toInteger($ids['cid']);
			$id = implode(',', $ids['cid']);
			$ret = $model->set_status($id, $state);
			if($model->set_status($id, $state)){		
				return 1;
			} else {
				return 0;
			}
		}
		
		return -1;
	}
	
	public function save_and_close(){
		$this->setRedirect('index.php?option=com_awardpackage&package_id='.JRequest::getVar("package_id"), null);
	}
	
	public function onaddyear(){
		$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"), null);
	}
	
	public function onaddmonth(){
		$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"), null);
	}
	public function onaddday(){
		$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&from_year='.JRequest::getVar("from_year").'&to_year='.JRequest::getVar("to_year").'&from_month='.JRequest::getVar("from_month").'&to_month='.JRequest::getVar("to_month").'&from_day='.JRequest::getVar("from_day").'&to_day='.JRequest::getVar("to_day").'&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"), null);
	}
	
		public function ongender(){
		$this->setRedirect('index.php?option=com_awardpackage&view=fundreceiver&task=fundreceiver.create_update&title='.JRequest::getVar("title").'&filter='.JRequest::getVar("filter").'&gender='.JRequest::getVar("gender").'&package_id='.JRequest::getVar("package_id").'&id='.JRequest::getVar("id"), null);
	}
}