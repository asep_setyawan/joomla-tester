<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controller library
jimport('joomla.application.component.controller');
require_once JPATH_COMPONENT . '/helpers/awardpackages.php';
/**
 * General Controller of Donation component
 */
class AwardpackageControllerGiftcoderule extends JControllerLegacy
{
	/**
	 * display task
	 *
	 * @return void
	 */
	function __construct(){		
		parent::__construct();	
	}
	 
	function display($cachable = false) 
	{		
		JRequest::setVar('view', JRequest::getCmd('view', 'Giftcoderule'));
		parent::display($cachable);
	}
	
	function addCategory(){

		$this->setRedirect('index.php?option=com_jeemailnotificationmanager&controller=categoryitem');
	}
	
	function edit(){
		
		$model =& JModelLegacy::getInstance('giftcode','AwardpackageModel');
		
		$cids = JRequest::getVar('cid');
		
		foreach($cids as $cid){
			//set redirect page if editing success
			if( $model->edit($cid)){
				$link ='index.php?option=com_awardpackage&view=giftcode&package_id='.JRequest::getVar('package_id');
			}
		}
		
		$this->setRedirect($link);
	}
	
	function save(){
		
/*  		echo "<pre>";
		print_r($_POST); die;  */
	
		if(isset($_POST['rule_id']) && $_POST['rule_id']>0)
		{
				
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$conditions = array(
					$db->quoteName('rule_id') . ' = ' . $db->quote($_POST['rule_id'])
				);
				$query->delete($db->quoteName('#__ap_free_giftcode_rules_settings'));
				$query->where($conditions);
				$db->setQuery($query);
				$result = $db->execute(); 
				if($result)
				{
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					// delete all custom keys for user 1001.
					$conditions = array(
						$db->quoteName('rule_id') . ' = ' . $db->quote($_POST['rule_id'])
					);
					$query->delete($db->quoteName('#__ap_free_giftcode_rules_settings'));
					$query->where($conditions);
					$db->setQuery($query);
					$result = $db->execute(); 
				}

				$db = JFactory::getDbo();
				$query = $db->getQuery(true);

				
				$fields = array(
					$db->quoteName('title') . ' = ' . $db->quote($_POST["title"]),
					$db->quoteName('modified') . ' = ' . $db->quote(date("Y-m-d H:m:s"))
				);
				

				$conditions = array(
					$db->quoteName('id') . ' = ' . $db->quote($_POST['rule_id'])
				);
				
			/*= 	print_r($conditions); die; */
 
				$query->update($db->quoteName('#__ap_free_giftcode_rules'))->set($fields)->where($conditions);
				$db->setQuery($query);
				$db->execute();	
				
				$rule_id = $_POST['rule_id'];
				
				foreach($_POST["weekdays"] as $rule){
					$data_type = "day";
					$query = $db->getQuery(true);
					$columns = array('rule_id','data_type','data_value','giftcodes');
					$values = array($rule_id,$db->quote($data_type),$db->quote(json_encode($rule["day"])),$db->quote(json_encode($rule["giftcode"])));
					$query
					->insert($db->quoteName('#__ap_free_giftcode_rules_settings'))
					->columns($db->quoteName($columns))
					->values(implode(',', $values));			
					$db->setQuery($query);
					$db->execute();		
				}
				
				foreach($_POST["calendar"] as $rule){
					$data_type = "date";
					$query = $db->getQuery(true);
					$columns = array('rule_id','data_type','data_value','giftcodes');
					$values = array($rule_id,$db->quote($data_type),$db->quote(json_encode($rule["date"])),$db->quote(json_encode($rule["giftcode"])));
					$query
					->insert($db->quoteName('#__ap_free_giftcode_rules_settings'))
					->columns($db->quoteName($columns))
					->values(implode(',', $values));			
					$db->setQuery($query);
					$db->execute();		
				}
			
				$msg = "Rule has been updated";
				
				$this->setRedirect('index.php?option=com_awardpackage&view=giftcoderule&package_id='.$_POST["package_id"], $msg);	
			
			
		}
		else{
				$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$columns = array('title','created','package_id');
		$values = array($db->quote($_POST["title"]),$db->quote(date("Y-m-d H:m:s")),$_POST["package_id"]);
		$query
			->insert($db->quoteName('#__ap_free_giftcode_rules'))
			->columns($db->quoteName($columns))
			->values(implode(',', $values));
		$db->setQuery($query);
		$db->execute();
		$rule_id = $db->insertid();
		
		foreach($_POST["weekdays"] as $rule){
			$data_type = "day";
			$query = $db->getQuery(true);
			$columns = array('rule_id','data_type','data_value','giftcodes');
			$values = array($rule_id,$db->quote($data_type),$db->quote(json_encode($rule["day"])),$db->quote(json_encode($rule["giftcode"])));
			$query
			->insert($db->quoteName('#__ap_free_giftcode_rules_settings'))
			->columns($db->quoteName($columns))
			->values(implode(',', $values));			
			$db->setQuery($query);
			$db->execute();		
		}
		
		foreach($_POST["calendar"] as $rule){
			$data_type = "date";
			$query = $db->getQuery(true);
			$columns = array('rule_id','data_type','data_value','giftcodes');
			$values = array($rule_id,$db->quote($data_type),$db->quote(json_encode($rule["date"])),$db->quote(json_encode($rule["giftcode"])));
			$query
			->insert($db->quoteName('#__ap_free_giftcode_rules_settings'))
			->columns($db->quoteName($columns))
			->values(implode(',', $values));			
			$db->setQuery($query);
			$db->execute();		
		}
	
		$msg = "Rule has been saved";
		
		$this->setRedirect('index.php?option=com_awardpackage&view=giftcoderule&package_id='.JRequest::getVar('package_id'), $msg);	
			
		}		
	}
	
	function publish() {
		
		$cid = JRequest::getVar("cid");
		
		switch($cid) {
		
		    case 0 :
			$msg = "No Category Published";
			break;
		    default :
			$msg = count($cid)." Categories Published";
			break;            
		}
		
		foreach ($cid as $id) {
			
		    $model =& JModelLegacy::getInstance('giftcode','AwardpackageModel');
		    
		    $update = $model->published($id);
		}
		
		$this->setRedirect('index.php?option=com_awardpackage&view=giftcode&package_id='.JRequest::getVar('package_id'), $msg);		
	}

	function unpublish() {
	    
	    $cid = JRequest::getVar("cid");
	    
	    switch($cid) {
		
		case 0 :
		    $msg = "No Category Unpublished";
		    break;
		default :
		    $msg = count($cid)." Categories Unpublished";
		    break;            
	    }
	    
	    foreach ($cid as $id) {
		$model =& JModelLegacy::getInstance('giftcode','AwardpackageModel');
		
		$update = $model->unpublished($id);
	    }
	    
	    $this->setRedirect('index.php?option=com_awardpackage&view=giftcode&package_id='.JRequest::getVar('package_id'), $msg);		
	}
	
	public function save_schedule(){
		 $model =& JModelLegacy::getInstance('giftcode','AwardpackageModel');
		 $days = array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
		 $package_id = JRequest::getVar('package_id');
		 $category_id = JRequest::getVar('category_id');
		 foreach($days as $day){
		 	$scheduleDay = JRequest::getVar($day);
			if($scheduleDay){
				$active=1;
			}else{
				$active=0;
			}
		 	$check = $model->checkSchedule($package_id,$category_id);
			if($check){ // update the row
				$save=$model->updateSchedule($package_id,$category_id,$day,$active);
			}else{ //insert 
				$save=$model->insertSchedule($package_id,$category_id,$day,$active);
			}
		 }
		 if($save){
		 	$link = 'index.php?option=com_awardpackage&view=award_category&layout=free&package_id='.$package_id;
			$msg  = 'Schedule has been saved';
			$this->setRedirect($link,$msg);
		 }
	}
	
	public function cancel_schedule(){
		 $package_id = JRequest::getVar('package_id');
		 $link = 'index.php?option=com_awardpackage&view=award_category&layout=free&package_id='.$package_id;
		 $this->setRedirect($link,$msg);
	}


	public function delete(){
		
		echo "Here";print_r($_POST);die;
		
		$db = JFactory::getDbo();
 
		$query = $db->getQuery(true);

		$query->delete($db->quoteName('#__ap_free_giftcode_rules'));
		$query->where($db->nameQuote('id').'='.$db->quote($key)); 
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
	
		$msg = "Rule has been saved";
		
		$this->setRedirect('index.php?option=com_awardpackage&view=giftcoderule&package_id='.JRequest::getVar('package_id'), $msg);	
	}

	
}
