<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controller library
jimport('joomla.application.component.controller');
require_once JPATH_COMPONENT . '/helpers/awardpackages.php';
/**
 * General Controller of Donation component
 */
class AwardpackageControllerGiftcoderulerewardslist extends JControllerLegacy
{
	/**
	 * display task
	 *
	 * @return void
	 */
	function __construct(){	

		parent::__construct();	
	}
	 
	function display($cachable = false) 
	{		
		JRequest::setVar('view', JRequest::getCmd('view', 'Giftcoderulerewardslist'));
		parent::display($cachable);
	}
	
	function addCategory(){

		$this->setRedirect('index.php?option=com_jeemailnotificationmanager&controller=categoryitem');
	}
	
	function edit(){
		
		$model =& JModelLegacy::getInstance('giftcode','AwardpackageModel');
		
		$cids = JRequest::getVar('cid');
		
		foreach($cids as $cid){
			//set redirect page if editing success
			if( $model->edit($cid)){
				$link ='index.php?option=com_awardpackage&view=giftcode&package_id='.JRequest::getVar('package_id');
			}
		}
		
		$this->setRedirect($link);
	}
	
	function create(){
		
/* 		echo "<pre>";
		print_r($_POST); die; */
	
		$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$columns = array('package_id','start_date', 'end_date', 'user_group_id', 'rule_id','created');
		$values = array($_POST['package_id'],$db->quote(date('Y-m-d',strtotime($_POST["start_date"]))),$db->quote(date('Y-m-d',strtotime($_POST["end_date"]))),$_POST["group_id"],$_POST["rule_id"],$db->quote(date("Y-m-d H:m:s")));
		$query->insert($db->quoteName('#__ap_free_gift_code_rewards_list'))
				->columns($db->quoteName($columns))
				->values(implode(',', $values));
		$db->setQuery($query);
		$db->execute();
		$rule_id = $db->insertid();
		if($rule_id){
			$msg = "Record has been saved successfully!!!";	
			$this->setRedirect('index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id'), $msg);
		}		
		
	}
	
	function delete(){
		$db = JFactory::getDbo();
		 
		if(isset($_POST["cid"]) && !empty($_POST["cid"])){
			foreach($_POST["cid"] as $key=>$value){
				$query = $db->getQuery(true);
				 
				// delete all custom keys for user 1001.
				$conditions = array(
					$db->quoteName('id') . ' = '.$value
				);
				 
				$query->delete($db->quoteName('#__ap_free_gift_code_rewards_list'));
				$query->where($conditions);
				 
				$db->setQuery($query);
				 
				$result = $db->execute();				
			}
			$msg = "Record Has Been deleted Successfully!!!";	
			$this->setRedirect('index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id'), $msg);
		}
		else{
			$msg = "Please select any record.";	
			$this->setRedirect('index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id'), $msg,'error');
		}

	}
	
	function publish() {
		$model =& JModelLegacy::getInstance('giftcoderulerewardslist','AwardpackageModel');
		$reward_id = JRequest::getVar("id");
		$status=1;
		$result=$model->change_reward_status($reward_id,$status);
		if($result)
		{
			$this->setRedirect('index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id'), $msg);		
		}
		
	}

	function unpublish() {	 
		$model =& JModelLegacy::getInstance('giftcoderulerewardslist','AwardpackageModel');
	    $reward_id = JRequest::getVar("id");
		
		$status=2;
		$result=$model->change_reward_status($reward_id,$status);
		if($result)
		{
			$this->setRedirect('index.php?option=com_awardpackage&controller=giftcoderulerewardslist&package_id='.JRequest::getVar('package_id'), $msg);		
		}		
	}
	
	
	
	
	public function save_schedule(){
		 $model =& JModelLegacy::getInstance('giftcode','AwardpackageModel');
		 $days = array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
		 $package_id = JRequest::getVar('package_id');
		 $category_id = JRequest::getVar('category_id');
		 foreach($days as $day){
		 	$scheduleDay = JRequest::getVar($day);
			if($scheduleDay){
				$active=1;
			}else{
				$active=0;
			}
		 	$check = $model->checkSchedule($package_id,$category_id);
			if($check){ // update the row
				$save=$model->updateSchedule($package_id,$category_id,$day,$active);
			}else{ //insert 
				$save=$model->insertSchedule($package_id,$category_id,$day,$active);
			}
		 }
		 if($save){
		 	$link = 'index.php?option=com_awardpackage&view=award_category&layout=free&package_id='.$package_id;
			$msg  = 'Schedule has been saved';
			$this->setRedirect($link,$msg);
		 }
	}
	
	public function cancel_schedule(){
		 $package_id = JRequest::getVar('package_id');
		 $link = 'index.php?option=com_awardpackage&view=award_category&layout=free&package_id='.$package_id;
		 $this->setRedirect($link,$msg);
	}	
}
