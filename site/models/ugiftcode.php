<?php
defined('_JEXEC') or die();

jimport('joomla.application.component.modelitem');
class AwardpackageUsersModelUgiftcode extends JModelLegacy {

	function __construct() {
		parent::__construct ();
	}
	
	function getAllGiftcodes($gcid, $limit, $limitstart) {
		$query = "select * from #__giftcode_giftcode where giftcode_category_id ='" .$gcid. "'  AND published= 1 LIMIT ".$limitstart.",".$limit." ";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function getAllGiftcodes_total($gcid) {
		$query = "select * from #__giftcode_giftcode where giftcode_category_id ='" .$gcid. "' AND published= 1 ";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getAllGiftcodes_2() {
		$query = "select * from #__giftcode_giftcode WHERE published = 1 ";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
		function getAllGiftcodes_3($limit, $limitstart) {
		$query = "select * from #__giftcode_giftcode WHERE published = 1 LIMIT ".$limitstart.",". $limit."";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function getAllGiftcodesUser ($gcid,$userid) {
		$query = "SELECT * FROM #__giftcode_giftcode WHERE giftcode_setting_id = '" .$gcid. "' AND giftcode_queue_id = '" .$userid. "'";		
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function getAllUserGiftcodesHistory($package_id,$user_id,$limit, $limitstart){
  		$query = "select COUNT(b.category_name) as total, SUM(a.status) as jml, a.*, b.colour_code, b.category_name, c.giftcode 
		 from  #__gc_recieve_user a
		 LEFT JOIN #__ap_categories b ON b.setting_id = a.category_id
		 LEFT JOIN #__giftcode_giftcode c ON c.id = a.gcid 
		 WHERE a.user_id = '" .$user_id. "'
		 Group by b.category_name 
		 LIMIT ".$limitstart.", ".$limit."
 		  ";
		 //".$limitstart.", ".$limit."' ";
		 $this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	  }
	  
	function getAllUserGiftcodes($gcid, $userid) {
		$query = "select * from #__gc_recieve_user where category_id = '" .$gcid. "'  AND user_id = '" .$userid. "' AND status = 0 ";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function getAllUserGiftcodes_2( $userid) {
		$query = "select * from #__gc_recieve_user where  user_id = '" .$userid. "' AND status = 0 ";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function UpdateUserGiftcodes($giftcode, $giftcodeId,$userid,$status='1') {
        $query = "select count(*) as total from #__gc_recieve_user where category_id = '" .$giftcode. "' AND gcid = '" .$giftcodeId. "'  AND  user_id = '" .$userid. "' AND status = 1 ";
        $this->_db->setQuery ( $query );
        $result = $this->_db->loadObject();
        if($result->total >0){
            $query4 = "UPDATE #__gc_recieve_user
			  SET status = '1'
			  WHERE	category_id = '" .$giftcode. "' AND gcid = '" .$giftcodeId. "'  AND user_id = '" .$userid. "' " ;
            $this->_db->setQuery($query4);
            $this->_db->query();
        }else{
            $query4_1= "insert into #__gc_recieve_user(category_id,gcid,user_id,date_time,status,giftcode_type) values('".$giftcode."','".$giftcodeId."','".$userid."','".date('Y-m-d H:i:s')."','".$status."','0')";
            $this->_db->setQuery($query4_1);
            $this->_db->query();
        }

	
		$query5 = "UPDATE #__giftcode_giftcode
			  SET giftcode_queue_id = '" .$userid. "'
			  WHERE	id = '" .$giftcodeId. "' ";
		$this->_db->setQuery($query5);
		$this->_db->query();
		
		
		
	}
	
	function update_symbol_pieces($id,$userid){
		//$queries = array();
		$id = implode(',', $id);		
		$query = "UPDATE #__symbol_symbol_pieces
			  SET is_lock = '" .$userid. "' 
			  WHERE symbol_pieces_id in (".$id.")";	

		$this->_db->setQuery($query);
		$this->_db->query();
	
		return true;
	}
	
	function InsertSymbolQueue($id, $giftcode, $giftcodeId,$user_id, $prize_id,$symbol_id, $status ){
		//$queries = array();
		$id = implode(',', $id);		
		$date = JFactory::getDate();
		$now = $date;
		$query = "INSERT #__symbol_queue_detail ( symbol_pieces_id, status, symbol_prize_id, userid, gcid, category_id ,queue_id, date_created, date_end ) 
		VALUE 
		('".$id."', '".$status."' , '".$prize_id."','" .$user_id. "','" . $giftcodeId. "','" .$giftcode. "' ,'" .$symbol_id. "', '".$now."', '".$now."' )";
//			  SET is_lock = '" .$userid. "' 
	//		  WHERE symbol_pieces_id in (".$id.")";	

		$this->_db->setQuery($query);
		$this->_db->query();
	
		return true;
	}
	
	function getAllUserSymbol($symbol) {
		$query = "select a.*, b.id as prize_id, c.prize_name, c.prize_value  
		from #__symbol_symbol_pieces a
		INNER JOIN #__symbol_symbol_prize b ON b.symbol_id = a.symbol_id
		INNER JOIN #__symbol_prize c ON c.id = a.symbol_id
		where is_lock = 0 order by symbol_pieces_id asc";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}


					
	function getCategories($package_id){
		$query = "select setting_id, category_id, colour_code, category_name from #__ap_categories
				where package_id = '" .$package_id. "' order by category_id asc ";
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}
	
	function getUserGiftCode($package_id,$userId,$category){
		if ($category =='All'){
		$query = "select *, b.category_name, c.description 
		 from  #__gc_recieve_user a
		 LEFT JOIN #__ap_categories b ON b.setting_id = a.status 
		 LEFT JOIN #__funding_history c ON c.funding_history_id = a.category_id 
		 WHERE a.user_id = '" .$userId. "' ";
		 }else {
		 $query = "select *, b.category_name, c.description 
		 from  #__gc_recieve_user a
		 LEFT JOIN #__ap_categories b ON b.setting_id = a.status 
		 LEFT JOIN #__funding_history c ON c.funding_history_id = a.category_id 
		 WHERE a.user_id = '" .$userId. "' AND a.status='" .$category. "'";
		 }
		$this->_db->setQuery ( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}

public function getSymbolSymbolPrize_2($package_id){
		$query = " SELECT presentations FROM #__selected_presentation WHERE package_id = '".$package_id."' AND is_delete = '0' ";
		$this->_db->setQuery ($query);
		$rSelectedPresentations = $this->_db->loadObjectList();
		$r = "";
		foreach ($rSelectedPresentations as $rSelectedPresentation) {
			$r .= $rSelectedPresentation->presentations . ',';
		}
		$query = "	SELECT a.symbol_prize_id, c.prize_name, c.prize_image, c.prize_value, b.symbol_name, b.symbol_image, b.pieces, b.cols, b.rows, d.presentation_id, d.status
					FROM #__symbol_symbol_prize a 
					LEFT JOIN #__symbol_symbol b ON b.symbol_id = a.symbol_id 
					LEFT JOIN #__symbol_prize c ON c.id = a.id
					INNER JOIN #__symbol_presentation d ON d.presentation_id = a.presentation_id AND d.package_id = '".$package_id."'									 LIMIT 1
				 ";
		$this->_db->setQuery ($query);
		$results = $this->_db->loadObjectList();
		$ss = array();
		foreach ($results as $result) {
			if(strpos($r, $result->presentation_id) === false || $r == ""){
				$ss[] = $result;
			}
		}
		return $results;
	}
	
	function getPresentationCategory($category, $package_id, $prize_id = '0'){
		$query = "
				SELECT pac.`category_id`, pac.`colour_code`, pac.`category_name`, ppc.`presentation_id`,
				pss.`symbol_image`, pss.`pieces`, pss.`rows`, pss.`cols`, ps.`id` as prize_image_id, ps.`prize_image`, ps.`prize_name`
				FROM #__presentation_category ppc
				INNER JOIN #__ap_categories pac ON pac.`category_id` = ppc.`category_id`
				INNER JOIN #__symbol_presentation psp ON psp.`presentation_id` = ppc.`presentation_id`
				INNER JOIN #__symbol_symbol_prize pssp ON pssp.`presentation_id` = psp.`presentation_id`
				INNER JOIN #__symbol_symbol pss ON pss.`symbol_id` = pssp.`symbol_id`
				INNER JOIN #__symbol_prize ps ON ps.`id` = pssp.`id` " .($prize_id == '0' ? "" : " AND ps.`id` = '" .$prize_id. "' "). "
				WHERE ppc.`category_id` = '" .$category. "' AND ppc.`package_id` = '" .$package_id. "'
			";
		$this->_db->setQuery ( $query );
		$rows = $this->_db->loadObjectList();
		if(!empty($rows)) {
			$row = $rows[0];
			$result = new stdClass();
			$result->category_id = $row->category_id;
			$result->colour_code = $row->colour_code;
			$result->category_name = $row->category_name;

			$prs = array();
			foreach ($rows as $row) {
				$pr = new stdClass();
				$pr->presentation_id = $row->presentation_id;
				$pr->symbol_image = $row->symbol_image;
				$pr->pieces = $row->pieces;
				$pr->rows = $row->rows;
				$pr->cols = $row->cols;
				$pr->prize_image_id = $row->prize_image_id;
				$pr->prize_image = $row->prize_image;
				$pr->prize_name = $row->prize_name;
				$prs[] = $pr;			
			}
			$result->presentation = $prs;
			return $result;
		}
		return null;
	}

    public function getTotalRemainingGiftCode(){
        $user = JFactory::getUser();
        $users = AwardPackageHelper::getUserData();
        $db =& JFactory::getDBO();
        $query = "SELECT
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode
					ELSE a.incomplete_giftcode
				END  AS giftcode,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_quantity
					ELSE a.incomplete_giftcode_quantity
				END  AS giftcode_quantity,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_cost_response
					ELSE a.incomplete_giftcode_cost_response
				END  AS cost_response
				FROM #__quiz_question_giftcode a
				LEFT JOIN #__quiz_responses c ON c.quiz_id = a.quiz_id
				LEFT JOIN #__quiz_response_details b ON b.response_id = c.id
				WHERE c.created_by = '".$user->id."' and c.completed = 1 and c.completed !=  '0000-00-00 00:00:00'
				AND a.page_number > 0"  ;
        $db->setQuery($query);
        $quiz_details = $db->loadObjectList();

        $query1= "SELECT
			    CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode
					ELSE a.incomplete_giftcode
				END  AS giftcode,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_quantity
					ELSE a.incomplete_giftcode_quantity
				END  AS giftcode_quantity,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_cost_response
					ELSE a.incomplete_giftcode_cost_response
				END  AS cost_response
				FROM
				#__survey_question_giftcode a
				INNER JOIN #__survey_responses c ON c.survey_id = a.survey_id
				INNER JOIN #__survey_response_details b ON b.response_id = c.id
				WHERE c.created_by =  '".$user->id."' AND a.page_number >0 AND c.completed !=  '0000-00-00 00:00:00'";
        $db->setQuery($query1);
        $survey_details = $db->loadObjectList();
        $total_giftcode = 0;

        $sql = 'SELECT * from #__donation_giftcode WHERE user_id = "'.$user->id.'" AND package_id = "'.$users->package_id.'"';
        $db->setQuery($sql);
        $donationGiftcode = $db->loadObjectList();

        foreach($quiz_details as $quiz){
            $total_giftcode += $quiz->giftcode_quantity;
        }

        foreach($survey_details as $survey){
            $total_giftcode += $survey->giftcode_quantity;
        }

        foreach($donationGiftcode as $donation)
        {
            $total_giftcode += $donation->value;
        }

        $query3="select count(*) from #__gc_recieve_user where user_id = '".$user->id."' and status = '1'";
        $db->setQuery($query3);
        $used_code = $db->loadResult();
        $total_giftcode = $total_giftcode - $used_code;
        return $total_giftcode;
    }

    public function getTotalRemainingGiftCode_category($cat){
        $user = JFactory::getUser();
        $users = AwardPackageHelper::getUserData();
        $db =& JFactory::getDBO();
        $query = "SELECT
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode
					ELSE a.incomplete_giftcode
				END  AS giftcode,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_quantity
					ELSE a.incomplete_giftcode_quantity
				END  AS giftcode_quantity,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_cost_response
					ELSE a.incomplete_giftcode_cost_response
				END  AS cost_response
				FROM #__quiz_question_giftcode a
				LEFT JOIN #__quiz_responses c ON c.quiz_id = a.quiz_id
				LEFT JOIN #__quiz_response_details b ON b.response_id = c.id
				WHERE c.created_by = '".$user->id."' and c.completed = 1 and c.completed !=  '0000-00-00 00:00:00'
				AND a.page_number > 0"  ;
        $db->setQuery($query);
        $quiz_details = $db->loadObjectList();

        $query1= "SELECT
			    CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode
					ELSE a.incomplete_giftcode
				END  AS giftcode,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_quantity
					ELSE a.incomplete_giftcode_quantity
				END  AS giftcode_quantity,
				CASE
					WHEN b.answer_id IS NOT NULL
					THEN a.complete_giftcode_cost_response
					ELSE a.incomplete_giftcode_cost_response
				END  AS cost_response
				FROM
				#__survey_question_giftcode a
				INNER JOIN #__survey_responses c ON c.survey_id = a.survey_id
				INNER JOIN #__survey_response_details b ON b.response_id = c.id
				WHERE c.created_by =  '".$user->id."' AND a.page_number >0 AND c.completed !=  '0000-00-00 00:00:00'";
        $db->setQuery($query1);
        $survey_details = $db->loadObjectList();
        $total_giftcode = 0;

        $colorCode = "SELECT name from #__giftcode_category WHERE id = '".$cat."'";
        $db->setQuery($colorCode);
        $colorName = $db->loadObject();


        $sql = 'SELECT * from #__donation_giftcode WHERE category_id = "'.$cat.'" AND user_id = "'.$user->id.'" AND package_id = "'.$users->package_id.'"';
        $db->setQuery($sql);
        $donationGiftcode = $db->loadObjectList();

        foreach($quiz_details as $quiz){

            if( strtoupper($colorName->name) == strtoupper($quiz->giftcode)){
                $total_giftcode += $quiz->giftcode_quantity;
            }
        }

        foreach($survey_details as $survey){
            if( strtoupper($colorName->name) == strtoupper($survey->giftcode)) {
                $total_giftcode += $survey->giftcode_quantity;
            }
        }

        foreach($donationGiftcode as $donation)
        {
            $total_giftcode += $donation->value;
        }

        $query3="select count(*) from #__gc_recieve_user where user_id = '".$user->id."' and status = '1' and category_id = '".$cat."'";
        $db->setQuery($query3);
        $used_code = $db->loadResult();
        $total_giftcode = $total_giftcode - $used_code;
        return $total_giftcode;
    }

    function getAllUserGiftcodes_3( $userid) {
        $query = "select * from #__gc_recieve_user where  user_id = '" .$userid. "' AND status <>0 ";
        $this->_db->setQuery ( $query );
        $result = $this->_db->loadObjectList();
        return $result;
    }
	
/*********** count free giftcodes assigned to user **********/	
	
	function getFreeGiftCodeQty($user_group_id){
		$quantity = 0;
		$currentDay = date("l");
		$currentDate = date("d F");
		
		//echo $currentDate; die;
		if($user_group_id>0){
			$query = "select rule_id from #__ap_free_gift_code_rewards_list where  user_group_id = '" .$user_group_id. "'";
			$this->_db->setQuery ( $query );
			$result = $this->_db->loadObjectList();
			if(empty($result)){
				return $quantity;
			}
			else{
				$rule_id = $result[0]->rule_id;
				$query = "select * from #__ap_free_giftcode_rules_settings where  rule_id = '" .$rule_id. "'";
				$this->_db->setQuery ( $query );
				$result = $this->_db->loadObjectList();
				if(empty($result)){
					return $quantity;
				}
				else{
					foreach($result as $rec){
						if($rec->data_type == "day"){
							$days = json_decode($rec->data_value);
							if (in_array($currentDay, $days)) {
								$giftcodes = json_decode($rec->giftcodes);
								foreach($giftcodes as $code){
									$code = explode(",",$code);
									$quantity = $quantity+$code[2];
								}
							}
						}
						else{
							$dates = json_decode($rec->data_value);
							if (in_array($currentDate, $dates)) {
								$giftcodes = json_decode($rec->giftcodes);
								foreach($giftcodes as $code){
									$code = explode(",",$code);
									$quantity = $quantity+$code[2];
								}
							}
						}
					}
				}
			}
			return $quantity;
		}
		else{
			return $quantity;
		}
	}
	
}