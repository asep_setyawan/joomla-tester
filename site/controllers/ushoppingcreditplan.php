<?php
defined('_JEXEC') or die();

jimport('joomla.application.component.controller');
require_once JPATH_SITE.'/components/com_cjlib/framework/functions.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/constants.php';
require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/communitysurveys.php';
require_once JPATH_COMPONENT . '/helpers/awardpackage.php';
class AwardPackageControllerUshoppingcreditplan extends JControllerLegacy {
	function __construct(){
		parent::__construct();
	}
	function getMainPage(){
		$user = JFactory::getUser();
		if($user->guest) {
			/*temporary by aditya*/
			$view = $this->getView('ushoppingcreditplan', 'html');
			$view->assign('action', 'main_page');
			$view->display();
		} else {
			$view = $this->getView('ushoppingcreditplan', 'html');
			$view->assign('action', 'main_page');
			$view->display();
		} 
	}
	function showPlan(){
		$view = $this->getView('ushoppingcreditplan', 'html');
		$view->assign('action', 'show_plan');
		$view->display('show_plan');
	}
	
	function getDetail(){
		$view = $this->getView('ushoppingcreditplan', 'html');
		$view->assign('action', 'detail');
		$view->display('detail');
	}
	
	function getDescription(){
		$view = $this->getView('ushoppingcreditplan', 'html');
		$view->assign('action', 'show_description');
		$view->display('description');
	}
	
	function getRange(){
		$view = $this->getView('ushoppingcreditplan', 'html');
		$view->assign('action', 'show_range');
		$view->display('range');		
	}
	
	function getGraph(){
		$view = $this->getView('ushoppingcreditplan', 'html');
		$view->assign('action', 'show_graph');
		$view->display('graph');		
	}
	
	function getProgressDetail(){
		$view = $this->getView('ushoppingcreditplan', 'html');
		$view->assign('action', 'progress_plan_detail');
		$view->display('progress_plan_detail');		
	}
}