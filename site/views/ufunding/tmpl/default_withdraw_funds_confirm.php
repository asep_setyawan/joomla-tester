<?php
defined('_JEXEC') or die();
?>
<script type="text/javascript">
function onBack(){
	window.location = "index.php?option=com_awardpackage&view=ufunding&task=ufunding.withdrawFunds";
}
</script>
<form id="myForm">
<div id="cj-wrapper">
	<div class="container-fluid no-space-left no-space-right surveys-wrapper">
		<div class="row-fluid">			
			<table>
				<tr>
					<td valign="top">
						<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
					</td>
					<td valign="top">
						<div class="span12">
							<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo 'Funds remain : $ ' . number_format($this->remain,2); ?>
								</h2>	
							</div>
						</div>			
						
						<div class="span12" style="text-align:center;padding-right:10px;">				
							<button type="button" class="btn btn-primary btn-invite-reg-groups"
										id="btn" onclick="onBack();"><i></i> <?php echo JText::_('Back');?></button>				
							<br/>
							<br/>
						</div>
					</td>
				</tr>
			</table>			
		</div>
	</div>
</div>
</form>
