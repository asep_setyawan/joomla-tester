<?php
defined('_JEXEC') or die();
?>
<script type="text/javascript">
function onSelectCategory(category,categoryid){
	jQuery('#category').val(category);
	jQuery('#categoryid').val(categoryid);
	jQuery('#task').val('ugiftcode.onSelectCategory');
	jQuery('form#adminForm').submit();	
}

function myFunction() {
    var x;
    if (confirm("Unlock this Giftcode!") == true) {
        jQuery('#task').val('ugiftcode.doSave');
		jQuery('form#adminForm').submit();
    } 
}

function open_modal_window(e, index, id){		
	var row = jQuery(e).parent().parent().parent().parent().parent().parent().parent().parent().parent().index();
	jQuery('#rowQuestionText').val(row);
	jQuery('#giftcode').val(index);
	jQuery('#giftcodeId').val(id);	
	jQuery('#loadSymbol').modal('show');	
}

function close_modal_window(){	
	jQuery('#category').val(category);
	jQuery('#categoryid').val(categoryid);
	jQuery('#task').val('ugiftcode.update_symbol');
	jQuery('form#adminForm').submit();	
}

function discard_symbol(){	
	jQuery('#loadSymbol').modal('toggle');		
}

function openModalSymbol(){
	jQuery('#presentationModalWindow').modal('show');
}

function onCloseSymbolFilledModalWindow(e){			
		jQuery('#presentationModalWindow').modal('toggle');				    			    
}

function showHide(){
  if(document.getElementById('expanderContent_1').style.display == 'none'){
    document.getElementById('expanderContent_1').style.display = 'block';
    document.getElementById('expanderContent_2').style.display = 'block';
    document.getElementById('expanderContent_3').style.display = 'block';
    document.getElementById('expanderContent_4').style.display = 'block';
    document.getElementById('expanderContent_5').style.display = 'block';
    document.getElementById('expanderContent_6').style.display = 'block';
    document.getElementById('expanderContent_7').style.display = 'block';
	
  }else{
    document.getElementById('expanderContent_1').style.display = 'none'; 
    document.getElementById('expanderContent_2').style.display = 'none'; 
    document.getElementById('expanderContent_3').style.display = 'none'; 
    document.getElementById('expanderContent_4').style.display = 'none'; 
    document.getElementById('expanderContent_5').style.display = 'none'; 
    document.getElementById('expanderContent_6').style.display = 'none'; 
    document.getElementById('expanderContent_7').style.display = 'none'; 

  }
}

$(document).ready(function(){
	$("#expanderHead_1").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});


});

</script>
<form id="adminForm" name="adminForm" action="<?php echo JRoute::_('index.php?option=com_awardpackage&view=ugiftcode&task=ugiftcode.onSelectCategory&category='.$this->category.'&categoryid='.$this->categoryid.'')?>" method="post">
<input type="hidden" name="category" id="category" value="<?php echo $this->category; ?>">
<input type="hidden" name="categoryid" id="categoryid" value="<?php echo $this->categoryid; ?>">	
<input type="hidden" name="package_id" id="package_id" value="<?php echo $this->package_id; ?>">	
<input type="hidden" name="task" id="task" value="ugiftcode.onSelectCategory">
<div id="cj-wrapper">	
	<table width="100%">
		<tr>
			<td width="10%" valign="top">
				<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
			</td>
			<td valign="top">
				<div class="container-fluid no-space-left no-space-right surveys-wrapper">
					<div class="row-fluid">			
						<div class="span12">
							<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo JText::_('Giftcode'); ?>
								</h2>	
                                 <nav class="navigation" role="navigation">
                                <ul class="nav menu nav-pills">
								<li class="active"><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=ugiftcode&task=ugiftcode.getMainPage");?>">Home</a></li>
                              <li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=ugiftcode&task=ugiftcode.getHistoryGiftcode");?>">Giftcode History</a></li>	
                              </ul>
                              </nav>	
                                Unlocked Giftcode <?php echo ' ( Category '.$this->categoryid.' ) :	';	
								$total_giftcode = count($this->usercollection);
								//$total = count($this->collects); 
								//echo $this->categoryid;
								echo $total_giftcode.' / '.count($this->total);
								?>
                                <br/>
                                Remaining giftcode for <?php echo ' ( Category '.$this->tot_symbol.' ) : '. $this->total_value_giftcode?>
							</div>
						</div>		
						<div class="span12">
							<table class="table table-hover table-striped">
								<tr>
									<?php 
									foreach ($this->categories as $row){ 
                                    ?>
									<td class="hidden-phone">
<a style="text-decoration: none !important;" href="index.php?option=com_awardpackage&view=ugiftcode&task=ugiftcode.onSelectCategory&category=<?php echo $row->setting_id ?>&categoryid=<?php echo $row->category_id; ?>" >
										<table>
											<tr>
												<td style="padding-top:14px;width:40px;height:
														30px;text-align:center;background-color:<?php echo $row->colour_code;?>" valign="center">
												<font color="white" size="5"><b><?php echo $row->category_id; ?></b></font>
												</td>
											</tr>
										</table>
                                        	</a>					

										</div>
									</td>
									<?php } ?>
								</tr>
							</table>	
                            <!--<div style="text-align:right;padding:0 20px;"><?php /*echo $this->pagination->getLimitBox(); */?></div>-->
                            <ul style="padding:10px;">
            			<?php
                        $t=1;
            				foreach ($this->collects as $i => $item) {
							//foreach ($giftcode as $row)
							
						   $total_giftcode = count($this->usercollection);
						 //  $total_user_giftcode = count($this->collect_user);
//                                $t = $tmp+1;
//						   if ($tmp++ < $total_giftcode ){
                                if(!in_array($item->id,$this->taken_giftcode_gcid)){
                                    if($this->total_value_giftcode >0 ){
//                                        $t++;
                                        ?>
                                   <li style="padding:5px;float:right; list-style-type:none;">
                                       <?php echo '<button type="button" class="btn btn-default" style="width:63px; " onclick="open_modal_window(this, '.$item->giftcode_category_id.', '.$item->id.' ); return false;">';
                                       echo $item->giftcode; ?>
                                       </button>

                                   </li>
                               <?php } else { ?>
                                   <li style="padding:5px;float:right; list-style-type:none;">
                                       <button type="button" class="btn btn-default" style="width:63px; "
                                               id="addNewPrizeBtn" disabled="disabled"><i></i> <?php echo $item->giftcode;?></button>

                                   </li>


                           <?php }
                           }else { ?>
                           <li style="padding:5px;float:right; list-style-type:none;"> 
                                <button type="button" class="btn btn-default" style="width:63px; "
												id="addNewPrizeBtn" disabled="disabled"><i></i> <?php echo $item->giftcode;?></button>
                                                
                                	</li>
                                    
                                    <?php } } ?>
                          
	      				</ul>	
                        
						</div>			
					</div>
				</div>
                <div class="pagination" style="text-align:right;padding:0 20px;">
    <?php //$pagthisination = new JPagination($total, $p_start, 2);
	
echo $this->pagination->getListFooter();
echo '<br/>'. $this->pagination->getPagesCounter(); ?>
        </div>			
			</td>
		</tr>
	</table>	
    
    	
</div>

 <?php 

 echo '<div id="loadSymbol" class="modal hide fade" style="height:500px; width:900px;padding:5px;left: 67% !important;">';?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<!--	<h3>--><?php //echo JText::_('Symbol List');?><!--</h3>-->
    <h3><?php echo JText::_('You have received the following Award Symbol pieces:');?></h3>
</div>

<ul style="padding:5px;">
<table class="table table-hover table-striped">
<tr>
<?php 
$i=0;
foreach ($this->usersymbol as $rows) {
$total_symbol = $this->categoryid;
if ($tts++ < $total_symbol) {
$i = $i+1;
echo '<td style="padding:8px;float:left; list-style-type:none;border:1px solid #ccc;text-align:center;">';
//echo '<li style="padding:8px;float:left; list-style-type:none;border:1px solid #ccc;text-align:center;"> ';
$filename = $rows->symbol_pieces_image;
$file = '/administrator/components/com_awardpackage/asset/symbol/pieces/'.$filename; ?>
<img src="./administrator/components/com_awardpackage/asset/symbol/pieces/<?php echo $rows->symbol_pieces_image; ?>" width="80"><br/>
<?php
echo '<input type="hidden" value="'.$rows->symbol_pieces_id.'" name="setting_id[]">';	
echo '<input type="hidden" name="prizeId" id="prizeId" value="'.$rows->prize_id.'">';
echo '<input type="hidden" name="SymbolId" id="SymbolId" value="'.$rows->symbol_id.'">';	 
//echo JHtml::_('grid.id', $i, $rows->symbol_pieces_id);
echo '<hr />';
echo '<div id="expanderContent_'.$i.'" style="display:none;text-align:left;">';
echo 'Prize : '.$rows->prize_name;
echo '<br/>Prize Value : $'.$rows->prize_value; 
echo '</div>';
echo '</td>';
 }
}
?>

</tr>
</table>
</ul>

<div style="display:inline-table;padding:10px;margin:20px;">
<input type="hidden" name="giftcode" id="giftcode" value="<?php echo $item->giftcode_category_id;?>">
<input type="hidden" name="giftcodeId" id="giftcodeId" value="<?php echo $item->id;?>">
<!--<button type="button" class="btn" onclick="showHide();" id="addNewProcessBtn"><i></i> --><?php //echo JText::_('Check Prize');?><!--</button>-->
<!--<input type="submit" class="btn btn-default" style="width:70px;" id="button_check" name="button_check" value="Discard" onclick="discard_symbol();" />-->
<input type="submit" class="btn btn-default" style="width:70px;" id="button_check" name="button_check" value="Close" onclick="close_modal_window();" />
</div>
													
</div>
</form>
<div id="presentationModalWindow" class="modal hide fade" style="height:500px; width:600px;padding:10px;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_('Prize');?></h3>
	</div>
	<div style="overflow:scroll; height:450px; width:100%;">
		<table class="table table-striped" id="prizeTable"
			style="border: 1px solid #ccc;">
			<thead>
				<tr style="background-color:#CCCCCC">
					<th width="5%">#</th>
					<th ><?php echo JText::_( 'Presentation' ); ?></th>			
                    					<th ><?php echo JText::_( 'Prize Name' ); ?></th>													
								<th class="hidden-phone"><?php echo JText::_( 'Prize Image' ); ?></th>
								<th><?php echo JText::_( 'Prize Value' ); ?></th>	
                                <th><?php echo JText::_( 'Symbol Set' ); ?></th>								
								<th class="hidden-phone"><?php echo JText::_( 'Symbol Piece' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 0; 
				foreach ($this->symbolPrizes as $symbolPrizes){ 
				?>
				<tr>
					<td>
						<?php echo $i+1; ?>
					</td>
					<td style="text-align:center;">
						<?php echo JText::_( $symbolPrizes->presentation_id ); ?>
						<input type="hidden" name="presentationName" value="<?php echo $symbolPrizes->presentation_id; ?>" />
					</td>
                    <td>					
						<?php echo (empty($symbolPrizes->prize_name) ? '' : JText::_($symbolPrizes->prize_name )); ?>						
					</td>
					<td><img
				src="./administrator/components/com_awardpackage/asset/prize/<?php echo $symbolPrizes->prize_image; ?>"
				style="width: 100px;" />
					</td>
					<td>					
						<?php echo (empty($symbolPrizes->prize_value) ? '' : JText::_($symbolPrizes->prize_value )); ?>						
					</td>
                    <td><img
				src="./administrator/components/com_awardpackage/asset/symbol/<?php echo $symbolPrizes->symbol_image; ?>"
				style="width: 100px;" />
					</td>
                    <td>					
						<?php echo (empty($symbolPrizes->pieces) ? '' : JText::_($symbolPrizes->pieces )); ?>						
					</td>
				</tr>		
				<?php
				$i++;
				} 
				?>
			</tbody>
		</table>
	</div>		
</div>

<div id="prizewonModal" class="modal hide fade" style="height:200px; width:200px;padding:10px;margin: 0px">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo JText::_('You have won: ');?></h3>
    </div>
    <div style="overflow:auto; height:200px; width:100%;">
        <h4><?php if(isset($this->PrizeName)){ echo $this->PrizeName; } ?></h4>
        <p>You may go to the prize page to claim your prize!</p>
        <button class="btn btn-default" id="closeModal" type="button">Close</button>
    </div>
</div>

<script type="application/javascript">
    jQuery('#closeModal').click(function(){
        jQuery('#prizewonModal').modal('hide');
    });

    var prizeName = "<?php if(isset($this->PrizeName)){ echo $this->PrizeName; } ?>";

    if(prizeName != '' )
    {
        jQuery('#prizewonModal').modal('show');
    }else{
        jQuery('#prizewonModal').modal('hide');
    }
</script>