<?php
/**
 * @version		$Id: view.html.php 01 2013-01-13 11:37:09Z maverick $
 * @package		CoreJoomla.Survey
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2013 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );
class AwardpackageViewUshoppingcreditplan extends JViewLegacy {	
	
	function display($tpl = null) {		
		CommunitySurveysHelper::initiate();
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$user = JFactory::getUser();
		$model =& JModelLegacy::getInstance('shoppingcreditplan','AwardpackageUsersModel');
		$model_categories = & JModelLegacy::getInstance( 'shoppingcreditcategory', 'AwardpackageUsersModel' );	
		$model_donation = & JModelLegacy::getInstance( 'udonation', 'AwardpackageUsersModel' );
		$pathway = $app->getPathway();
		$users = AwardPackageHelper::getUserData();

		$packageId = $users->package_id;
		$packagedate = AwardPackageHelper::getPackageId($packageId);
		foreach ($packagedate as $row ){
		$enddate = date("Y-m-d", strtotime($row->end_date));
		}
		$today = date("Y-m-d"); 
					if ($today > $enddate) {
					$expired = 1;
					}
		$this->assignRef('expired', $expired);
		switch($this->action){
			case 'main_page':
			
	   
		$items = $this->get('Items');		
				$this->assign('page_id', 8);
				// breadcrumbs
				$pathway->addItem('Shopping Credits', JRoute::_($this->page_url.'&view=ushoppingcreditplan&task=ushoppingcreditplan.getMainPage'));		
				// add to pathway
				$pathway->addItem($page_heading);				

  		        $description = $model->get_donation_history($user->id);
				foreach ($description as $desc){
					$user_description = $desc->description;
					$amount = $desc->debit;
					$date_donation = $desc->created_date;
				}
				
				//$result = $model->get_shopping_credit_plan($packageId);
				$sc_plan = $model->getShoppingCredit($packageId);
				$shoppings = !empty($result['shoppings']) ? $result['shoppings'] : array();
				foreach ($sc_plan as $sc){
					$fee = $sc->fee;
					$refund = $sc->refund;
					$unlock = $sc->unlock;
					$expire = $sc->expire;
					$uniq_id = $sc->uniq_key;
					$start_date = $sc->start_date;									
					$end_date = $sc->end_date;	
					$contrib_radio = $sc->contribution_range;			
					$progress_radio = $sc->progress_check;							

				}

				//$plans = $model->get_shopping_credit_plan_detail(JRequest::getVar('id'));
				
				$this->assignRef('shoppings', $shoppings);
				$this->assignRef('pagination', $result['pagination']);
				$this->assignRef('lists', $result['lists']);
				$username = $users->username;		
				$this->assignRef('user_description', $user_description);	
				$this->assignRef('amount', $amount);	
				$this->assignRef('date_donation', $date_donation);	
				
				$this->assignRef('fee', $fee);	
				$this->assignRef('refund', $refund);	
				$this->assignRef('unlock', $unlock);	
				$this->assignRef('expire', $expire);	
				$this->assignRef('sc_plan', $sc_plan);	
				$this->assignRef('start_date', $start_date);	
				$this->assignRef('end_date', $end_date);	
					$this->assignRef('contrib_radio', $contrib_radio);
					$this->assignRef('progress_radio', $progress_radio);				
				
				
				$categories = $model_categories->list_categories();
				$result = $model->get_list_contribution_range($uniq_id,$packageId);
				$contribution_ranges = !empty($result['contribution_range']) ? $result['contribution_range'] : array();				
				$this->assignRef('contribution_ranges', $contribution_ranges);
				$this->assignRef('pagination_contribution_range', $result['pagination_contribution_range']);

				$result = $model->get_list_progress_check($uniq_id,$packageId);
				$progress_checkes = !empty($result['progress_check']) ? $result['progress_check'] : array();
				$this->assignRef('progress_checkes', $progress_checkes);
				$this->assignRef('pagination_progress_check', $result['pagination_progress_check']);
				
				break;

			case 'show_range':
				$sc_plan = $model->getShoppingCredit($packageId);
				$shoppings = !empty($result['shoppings']) ? $result['shoppings'] : array();

				foreach ($sc_plan as $sc){
					$fee = $sc->fee;
					$refund = $sc->refund;
					$unlock = $sc->unlock;
					$expire = $sc->expire;
					$uniq_id = $sc->uniq_key;
					$start_date = $sc->start_date;									
					$end_date = $sc->end_date;	
					$contrib_radio = $sc->contribution_range;			
					$progress_radio = $sc->progress_check;							
					$selisih = floor((strtotime($end_date)  - strtotime($start_date)) / 86400);
				}

				$this->assignRef('shoppings', $shoppings);
				$this->assignRef('pagination', $result['pagination']);
				$this->assignRef('lists', $result['lists']);
				$username = $users->username;		
				$this->assignRef('user_description', $user_description);	
				$this->assignRef('amount', $amount);	
				$this->assignRef('date_donation', $date_donation);					
				$this->assignRef('fee', $fee);	
				$this->assignRef('refund', $refund);	
				$this->assignRef('unlock', $unlock);	
				$this->assignRef('expire', $expire);	
				$this->assignRef('sc_plan', $sc_plan);	
				$this->assignRef('selisih', $selisih);	
		
				$categories = $model_categories->list_categories();
				$result = $model->get_list_contribution_range($uniq_id,$packageId);
				$contribution_ranges = !empty($result['contribution_range']) ? $result['contribution_range'] : array();				
				$this->assignRef('contribution_ranges', $contribution_ranges);
				$this->assignRef('pagination_contribution_range', $result['pagination_contribution_range']);

				$result = $model->get_list_progress_check($uniq_id,$packageId);
				$progress_checkes = !empty($result['progress_check']) ? $result['progress_check'] : array();
				$this->assignRef('progress_checkes', $progress_checkes);
				$this->assignRef('pagination_progress_check', $result['pagination_progress_check']);
								
				$user = AwardPackageHelper::getUserData();
				$userId = $user->id;
				$packageId = $user->package_id;
				$model_account = JModelLegacy::getInstance( 'ufunding', 'AwardpackageUsersModel' );
			    $listHistory = $model_account->getHistorySC($userId, $packageId,'REFUND');
				$this->assignRef('listHistory', $listHistory );
				foreach($this->listHistory as $ls){
										$tanggal = $ls->created_date;
										}
				$this->assignRef('tanggal', $tanggal );

				break;	
			case 'progress_plan_detail' :
				$plans = $model->get_shopping_credit_plan_detail(JRequest::getVar('id'));
				if(!empty($plans)) {
					$plan = $plans[0];
					$uniq_id = $plan->uniq_key;
					$start_date = $plan->start_date;
					$end_date = $plan->end_date;					
					$start = strtotime($start_date);
					$end = strtotime($end_date);					
					$selisih = floor(($end - $start) / 86400);
					$contribution_range_value = $plan->contribution_range_value;
					$progress_check_every = $plan->progress_check_every;
					$progress_check_type = $plan->progress_check_type;
					$this->assignRef('start_date', $start_date);
					$this->assignRef('end_date', $end_date);	
					$this->assignRef('contribution_range_value', $contribution_range_value);
					$this->assignRef('progress_check_every', $progress_check_every);
					$this->assignRef('progress_check_type', $progress_check_type);
					$this->assignRef('start', $start);
					$this->assignRef('selisih', $selisih);
				}		
				break;				
			case 'show_description':
				$sc_plan = $model->getShoppingCredit($packageId);
				$shoppings = !empty($result['shoppings']) ? $result['shoppings'] : array();

				foreach ($sc_plan as $sc){
					$fee = $sc->fee;
					$refund = $sc->refund;
					$unlock = $sc->unlock;
					$expire = $sc->expire;
					$uniq_id = $sc->uniq_key;
					$start_date = $sc->start_date;									
					$end_date = $sc->end_date;	
					$contrib_radio = $sc->contribution_range;			
					$progress_radio = $sc->progress_check;							
				}
				
				$this->assignRef('shoppings', $shoppings);
				$this->assignRef('pagination', $result['pagination']);
				$this->assignRef('lists', $result['lists']);
				$username = $users->username;		
				$this->assignRef('user_description', $user_description);	
				$this->assignRef('amount', $amount);	
				$this->assignRef('date_donation', $date_donation);	
				
				$this->assignRef('fee', $fee);	
				$this->assignRef('refund', $refund);	
				$this->assignRef('unlock', $unlock);	
				$this->assignRef('expire', $expire);	
				$this->assignRef('sc_plan', $sc_plan);	
				$this->assignRef('start_date', $start_date);	
				$this->assignRef('end_date', $end_date);	
				
				$user = AwardPackageHelper::getUserData();
				$userId = $user->id;
				$packageId = $user->package_id;
				$model_account = JModelLegacy::getInstance( 'ufunding', 'AwardpackageUsersModel' );
			    $listHistory = $model_account->getHistorySC($userId, $packageId,'REFUND');
				$totalEarn = $model_account->getShoppingCreditEarns($userId);
				foreach ($listHistory as $lh){
					 $total_earn1 = $total_earn1+$lh->credit;
				}
				
				foreach ($totalEarn as $te){
				$total_earn2 = $te->amount;
				}
				$this->assignRef('listHistory', $listHistory);	
				$total_earn = $total_earn1+$total_earn2;
				$this->assignRef('total_earn', $total_earn);
				$this->assignRef('total_earn1', $total_earn1);	
					
				break;		
			case 'show_graph':
				$sc_plan = $model->getShoppingCredit($packageId);
				$shoppings = !empty($result['shoppings']) ? $result['shoppings'] : array();

				foreach ($sc_plan as $sc){
					$fee = $sc->fee;
					$refund = $sc->refund;
					$unlock = $sc->unlock;
					$expire = $sc->expire;
					$uniq_id = $sc->uniq_key;
					$start_date = $sc->start_date;									
					$end_date = $sc->end_date;	
					$contrib_radio = $sc->contribution_range;			
					$progress_radio = $sc->progress_check;							
					$selisih = floor((strtotime($end_date)  - strtotime($start_date)) / 86400);
					$contribution_range_value = '$'.$sc->min_amount.' to $'.$sc->max_amount;
				}

				$this->assignRef('shoppings', $shoppings);
				$this->assignRef('pagination', $result['pagination']);
				$this->assignRef('lists', $result['lists']);
				$username = $users->username;		
				$this->assignRef('user_description', $user_description);	
				$this->assignRef('amount', $amount);	
				$this->assignRef('date_donation', $date_donation);					
				$this->assignRef('fee', $fee);	
				$this->assignRef('refund', $refund);	
				$this->assignRef('unlock', $unlock);	
				$this->assignRef('expire', $expire);	
				$this->assignRef('sc_plan', $sc_plan);	
				$this->assignRef('selisih', $selisih);	
				$this->assignRef('contribution_range_value', $contribution_range_value);	
				$this->assignRef('start_date', $start_date);	
				$this->assignRef('end_date', $end_date);	
				
				$categories = $model_categories->list_categories();
				$result = $model->get_list_contribution_range($uniq_id,$packageId);
				$contribution_ranges = !empty($result['contribution_range']) ? $result['contribution_range'] : array();				
				$this->assignRef('contribution_ranges', $contribution_ranges);
				$this->assignRef('pagination_contribution_range', $result['pagination_contribution_range']);

				$result = $model->get_list_progress_check($uniq_id,$packageId);
				$progress_checkes = !empty($result['progress_check']) ? $result['progress_check'] : array();
				$this->assignRef('progress_checkes', $progress_checkes);
				$this->assignRef('pagination_progress_check', $result['pagination_progress_check']);
								
				$user = AwardPackageHelper::getUserData();
				$userId = $user->id;
				$packageId = $user->package_id;
				$model_account = JModelLegacy::getInstance( 'ufunding', 'AwardpackageUsersModel' );
			    $listHistory = $model_account->getHistorySC($userId, $packageId,'REFUND');
				$this->assignRef('listHistory', $listHistory );
				foreach($this->listHistory as $ls){
										$tanggal = $ls->created_date;
										}
				$this->assignRef('tanggal', $tanggal );
				break;								
		}			
		parent::display($tpl);
	}	
}