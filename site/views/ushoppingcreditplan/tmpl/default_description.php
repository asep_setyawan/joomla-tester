<?php
defined('_JEXEC') or die();
?>
<script type="text/javascript">

function onGraph(){
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
	var everi = jQuery('#everi').val();
	var tipe = jQuery('#tipe').val();	
	window.location='index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getgraph&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to;
//			jQuery('form#adminForm').attr('action', 'index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getGraph&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to);
			//jQuery('form#adminForm').submit();
}

function on_select_contribution_range(from, to ){
	var everi = jQuery('#everi').val();
	var tipe = jQuery('#tipe').val();
	jQuery("#from").val(from);
	jQuery("#to").val(to);	
$("#contribRange").html('<a target="_blank" href="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getRange&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to+'"> $'+from+' to $'+to+'</a>');
}

function on_select_progress_check( everi, tipe){
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
	jQuery("#everi").val(everi);
	jQuery("#tipe").val(tipe);
$("#Progress").html(everi+' '+tipe);
$("#contribRange").html('<a target="_blank" href="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getRange&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to+'"> $'+from+' to $'+to+'</a>');
}

function showHide(div){
  if(document.getElementById(div).style.display == 'none'){
    document.getElementById(div).style.display = 'block';
  }else{
    document.getElementById(div).style.display = 'none'; 
  }
}

$(document).ready(function(){
	$("#expanderHead").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead2").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead3").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead4").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
});

function disableForm() {
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
    	inputs[i].disabled = true;
    }
    var selects = document.getElementsByTagName("select");
    for (var i = 0; i < selects.length; i++) {
    	selects[i].disabled = true;
    }
    var textareas = document.getElementsByTagName("textarea");
    for (var i = 0; i < textareas.length; i++) {
    	textareas[i].disabled = true;
    }
    var buttons = document.getElementsByTagName("button");
    for (var i = 0; i < buttons.length; i++) {
    	buttons[i].disabled = true;
    }
}
</script>

<div id="cj-wrapper">
	<div class="container-fluid no-space-left no-space-right surveys-wrapper" >
		<div class="row-fluid">
			<table width="100%">
				<tr>
					<td width="10%" valign="top">
						<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
					</td>
					<td valign="top">

<?php 
if (!empty($this->expired)) { 
echo '<div class="is-disabled">';
 }else{  
echo '<div class="span12">';
} ?>	<br/>								
<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo JText::_('Shopping Credit Account - Descriptions'); ?>
								</h2>		
							</div>
						
                       <div style="padding: 10px;">
<form id="adminForm" action="<?php echo JRoute::_('index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getGraph');?>" method="post" name="adminForm">       
                                                                            <input type="hidden" id="from" name="from" value="<?php echo JRequest::getVar('from'); ?>"/>
                                                                            <input type="hidden" id="to" name="to" value="<?php echo JRequest::getVar('to'); ?>"/>
                                                                            <input type="hidden" id="everi" name="everi" value="<?php echo JRequest::getVar('everi'); ?>"/>
                                                                            <input type="hidden" id="tipe" name="tipe" value="<?php echo JRequest::getVar('tipe'); ?>"/>                                                                            
                      <table class="table table-bordered">
                          <tr>
                             <td>
<table class="table table-striped table-bordered">
<tr><td><?php echo JText::_('Total shopping credits earned');?></td><td>:</td><td><?php echo $this->total_earn; ?></td><td width="60%"></td></tr>
<tr><td><?php echo JText::_('Total shopping credits spent');?></td><td>:</td><td><?php echo $this->total_earn1; ?></td><td width="60%"></td></tr>
<tr><td><?php echo JText::_('Total shopping credits expired');?></td><td>:</td><td><?php 
$expire = $this->total_earn1 - $this->total_earn;
echo ( ($expire = 0) ? 0 : $expire); ?></td><td width="60%"></td></tr>
                                    </table>
                                    </td>
                           </tr>
                           <tr>
                             <td>
                             <table class="table table-striped table-bordered">
																	<thead>
								<tr>
									<th style="text-align:center;"><?php echo JText::_( 'Shopping Credit'); ?></th>									
									<th style="text-align:center;"><?php echo JText::_( 'Awarded On'); ?></th>
									<th style="text-align:center;"><?php echo JText::_( 'Contribution Range'); ?></th>
									<th style="text-align:center;"><?php echo JText::_( 'Progress Check'); ?></th>
									<th style="text-align:center;"><?php echo JText::_( 'Expired On'); ?></th>
									<th style="text-align:center;"><?php echo JText::_( 'Days to Expire'); ?></th>
									<th style="text-align:center;"><?php echo JText::_( 'Status'); ?></th>
                                    
								</tr>
							</thead>
																	<tbody>
                                    	<?php 
										foreach ($this->sc_plan as $y){
										$min_amount = $y->min_amount;
										$max_amount = $y->max_amount;
										$everi = $y->every;
										$tipe = $y->type;
										$expire = $y->expire;
										}
										
										foreach ($this->listHistory as $x){
										/*for ($i=0, $n=count( $this->listHistory ); $i < $n; $i++){
										$x =& $this->listHistory[$i];
										$y =& $this->sc_plan[$i];
										var_dump($x);*/
										?>
										<tr>
																			<td style="text-align:center;"><?php echo $x->credit; ?></td>
																			<td style="text-align:center;"><?php echo $x->created_date; ?></td>
																			<td style="text-align:center;"><?php echo 'from '.$min_amount.' to '.$max_amount; ?></td>
																			<td style="text-align:center;"><?php echo  $everi.' '.$tipe; ?></td>
																			<td style="text-align:center;"><?php 
																			
																			 //$stop_date = $x->created_date;
																			 //$stop_date = date('Y-m-d H:i:s', strtotime($stop_date . ' +  1'));

																			 
																			 $stop_date = $x->created_date;
																			 $stop_date = date('Y-m-d H:i:s', strtotime($stop_date . ' +'.$expire.' day'));

																			 echo  $stop_date; ?></td>
																			<td style="text-align:center;"><?php 
$datetime1 = new Datetime();
$datetime2 = new Datetime($stop_date);	
$interval = $datetime1->diff($datetime2);
echo $interval->format('%R%a days');
																			//echo  $expire.' days'; ?></td>
																			<td style="text-align:center;"><?php echo ($datetime2 > $datetime1 ? 'Not Expired' : 'Expired'); ?></td>
                                                                            
                                                                            
								</tr>
                                                                        <?php } ?>
																																																					</tbody>
																</table></td>
                           </tr>
                         </table>
                       
					   
            

					  </div>
       </td>
       </tr>
       </table>
        		</div>

	</div>
</div>
			
</form>
