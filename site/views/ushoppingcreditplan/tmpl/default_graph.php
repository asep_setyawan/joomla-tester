<?php
defined('_JEXEC') or die();
?>
<script type="text/javascript">

function onGraph(){
window.location="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getgraph";
}

function on_select_contribution_range(from, to ){
	var everi = jQuery('#everi').val();
	var tipe = jQuery('#tipe').val();
	jQuery("#from").val(from);
	jQuery("#to").val(to);	
$("#contribRange").html('<a target="_blank" href="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getRange&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to+'"> $'+from+' to $'+to+'</a>');
}

function on_select_progress_check( everi, tipe){
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
	jQuery("#everi").val(everi);
	jQuery("#tipe").val(tipe);
$("#Progress").html(everi+' '+tipe);
$("#contribRange").html('<a target="_blank" href="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getRange&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to+'"> $'+from+' to $'+to+'</a>');
}

function showHide(div){
  if(document.getElementById(div).style.display == 'none'){
    document.getElementById(div).style.display = 'block';
  }else{
    document.getElementById(div).style.display = 'none'; 
  }
}

$(document).ready(function(){
	$("#expanderHead").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead2").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead3").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead4").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
});

function disableForm() {
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
    	inputs[i].disabled = true;
    }
    var selects = document.getElementsByTagName("select");
    for (var i = 0; i < selects.length; i++) {
    	selects[i].disabled = true;
    }
    var textareas = document.getElementsByTagName("textarea");
    for (var i = 0; i < textareas.length; i++) {
    	textareas[i].disabled = true;
    }
    var buttons = document.getElementsByTagName("button");
    for (var i = 0; i < buttons.length; i++) {
    	buttons[i].disabled = true;
    }
}
</script>

<div id="cj-wrapper">
	<div class="container-fluid no-space-left no-space-right surveys-wrapper" >
		<div class="row-fluid">
			<table width="100%">
				<tr>
					<td width="10%" valign="top">
						<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
					</td>
					<td>

<?php 
if (!empty($this->expired)) { 
echo '<div class="is-disabled">';
 }else{  
echo '<div class="span12">';
} ?>	<br/>								
<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo JText::_('Progress check graph'); ?>
								</h2>		
							</div>
						
                       <div class="clearfix">
					<div class="span12" style="border: 1px solid #ccc; padding: 10px;left:0px;height:auto;width:auto;">
						<table width="100%">
							<tr>
								<td align="left" width="83%">
									<span style="font-size:12px;"><?php echo JText::_('From:');?>
									&nbsp;
									<?php echo $this->start_date; ?>
									&nbsp;
									<?php echo JText::_('To:');?> 
									&nbsp;
									<?php echo $this->end_date; ?>
									&nbsp;&nbsp;&nbsp;
									<?php echo JText::_('Contribution Range:'); ?>
									&nbsp;
									<?php echo $this->contribution_range_value; ?>
									</span>
								</td>
								<td align="left">
									<table>
										<tr>
											<td>
												<div style="width:20px;height:20px;border:1px solid #000;background-color:red"></div>
											</td>
											<td>
												<?php echo JText::_('Checked'); ?>
											</td>
										</tr>
									</table>					
								</td>								
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td align="left">
									<table>
										<tr>
											<td>
												<div style="width:20px;height:20px;border:1px solid #000;background-color:yellow"></div>
											</td>
											<td>
												<?php echo JText::_('Not Checked'); ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td align="left">
									<table>
										<tr>
											<td>
												<div style="width:20px;height:20px;border:1px solid #000;background-color:green"></div>
											</td>
											<td>
												<?php echo JText::_('Benefits Awarded'); ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<?php foreach ($this->progress_checkes as $pc){  
						               $checkit = 0;
										foreach($this->listHistory as $ls){
										$tanggal = $ls->created_date;
											if (($this->tanggal > $this->start_date) && ($this->tanggal < $this->end_date)){
												$checkit = $checkit + 1;
												}				
											}	
										
										switch($pc->type){	
										case 'min':
											{
											$total_checks = (($pc->every * $this->selisih) * 24) * 60;
											$already_checked = $checkit;
											$not_checked = $total_checks - $already_checked;
												break;
											}
										case 'hour':
											{
											$total_checks = ($pc->every * $this->selisih) * 24;
											$already_checked = $checkit;
											$not_checked = $total_checks - $already_checked;

												break;
											}
										case 'day':
											{
											$total_checks = $pc->every * $this->selisih;
											$already_checked = $checkit;
											$not_checked = $total_checks - $already_checked;
											break;
											}
										}			
				?>
				<br/>
       <div style="width:755px; overflow:auto;">
                                    <table class="table table-striped table-bordered" >
							<thead>
								<tr>
									<th colspan="<?php echo $total_checks; ?>" nowrap><?php echo 'Progress Check : ' .$pc->every. ' ' .  $pc->type; 
									?>
                                    </th>									
								</tr>
								<tr>
									<?php for($j = 0; $j < $already_checked; $j++) { 
									//date('Y-m-d H:i:s', strtotime($stop_date . ' +'.$expire.' day'));
									$date = new DateTime($this->start_date);
									$now_date = $date->format('Y-m-d');
											$progress_date = date('Y-m-d', strtotime($now_date. ' +'.($j).' days'));
											//strtotime("+".($j + 1)." days", $this->start_date);											
    										//$progress_date =  date("Y-m-d", $this->start_date);
    										$last_date = $progress_date;
    								?>
    								<th><?php echo  $progress_date;//($j + 1).' '.$pc->type; ?></th>
									<?php } ?>
									<?php for($j = 0; $j < $not_checked; $j++) { ?>	
									<?php 	
											//$date = strtotime("+".($j + 1)." days", $last_date);
    										//$progress_date =  date("Y-m-d", $date);
											$progress_date = date('Y-m-d', strtotime($last_date. ' +'.($j+1).' days'));

    								?>
    								<th><?php echo  $progress_date; ?></th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php for($j = 0; $j < $already_checked; $j++) { ?>	
									<?php 											
																			$row = $this->listHistory[$j];

											$date = strtotime("+".($j + 1)." days", $this->start);											
    										$progress_date =  date("Y-m-d", $date);
    										$last_date = $date;
    								?>
    								<td style="background-color:red;"><?php echo '$'.$row->credit;//JText::_( '$0' );  ?></td>
									<?php } ?>
									<?php for($j = 0; $j < $not_checked; $j++) { ?>	
									<?php 	
											$date = strtotime("+".($j + 1)." days", $last_date);
    										$progress_date =  date("Y-m-d", $date);
    								?>
    								<td style="background-color:yellow;"></td>
									<?php } ?>
								</tr>
							</tbody>
						</table>
				</div>
				<?php } ?>	
                
       </td>
       </tr>
       </table>
        		</div>

	</div>
</div>
		
