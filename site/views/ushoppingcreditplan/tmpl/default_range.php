<?php
defined('_JEXEC') or die();
?>
<script type="text/javascript">

function onGraph(){
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
	var everi = jQuery('#everi').val();
	var tipe = jQuery('#tipe').val();	
//	window.location='index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getgraph&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to;
	window.open('index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getgraph','_blank');
}

function on_select_contribution_range(from, to ){
	var everi = jQuery('#everi').val();
	var tipe = jQuery('#tipe').val();
	jQuery("#from").val(from);
	jQuery("#to").val(to);	
$("#contribRange").html('<a target="_blank" href="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getRange&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to+'"> $'+from+' to $'+to+'</a>');
}

function on_select_progress_check( everi, tipe){
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
	jQuery("#everi").val(everi);
	jQuery("#tipe").val(tipe);
$("#Progress").html(everi+' '+tipe);
$("#contribRange").html('<a target="_blank" href="index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getRange&everi='+everi+'&tipe='+tipe+'&from='+from+'&to='+to+'"> $'+from+' to $'+to+'</a>');
}

function showHide(div){
  if(document.getElementById(div).style.display == 'none'){
    document.getElementById(div).style.display = 'block';
  }else{
    document.getElementById(div).style.display = 'none'; 
  }
}

$(document).ready(function(){
	$("#expanderHead").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead2").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead3").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
	$("#expanderHead4").click(function(){
		$("#expanderContent").slideToggle();
		if ($("#expanderSign").text() == "+"){
			$("#expanderSign").html("-")
		}
		else {
			$("#expanderSign").text("+")
		}
	});
});

function disableForm() {
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
    	inputs[i].disabled = true;
    }
    var selects = document.getElementsByTagName("select");
    for (var i = 0; i < selects.length; i++) {
    	selects[i].disabled = true;
    }
    var textareas = document.getElementsByTagName("textarea");
    for (var i = 0; i < textareas.length; i++) {
    	textareas[i].disabled = true;
    }
    var buttons = document.getElementsByTagName("button");
    for (var i = 0; i < buttons.length; i++) {
    	buttons[i].disabled = true;
    }
}
</script>

<div id="cj-wrapper">
	<div class="container-fluid no-space-left no-space-right surveys-wrapper" >
		<div class="row-fluid">
			<table width="100%">
				<tr>
					<td width="10%" valign="top">
						<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
					</td>
					<td valign="top">

<?php 
if (!empty($this->expired)) { 
echo '<div class="is-disabled">';
 }else{  
echo '<div class="span12">';
} ?>	<br/>								
<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo JText::_('Progress check'); ?>
								</h2>		
							</div>
						
                       <div style="padding: 10px;">
<form id="adminForm" action="<?php echo JRoute::_('index.php?option=com_awardpackage&view=ushoppingcreditplan&task=ushoppingcreditplan.getGraph');?>" method="post" name="adminForm">       
                                                                            <input type="hidden" id="from" name="from" value="<?php echo JRequest::getVar('from'); ?>"/>
                                                                            <input type="hidden" id="to" name="to" value="<?php echo JRequest::getVar('to'); ?>"/>
                                                                            <input type="hidden" id="everi" name="everi" value="<?php echo JRequest::getVar('everi'); ?>"/>
                                                                            <input type="hidden" id="tipe" name="tipe" value="<?php echo JRequest::getVar('tipe'); ?>"/>                                                                            
                      <table class="table table-bordered">
                          <tr>
                             <td>
                             <table class="table table-striped table-bordered" >
                             <?php 								
foreach ($this->sc_plan as $row){ 
$everi =  JRequest::getVar('everi');
$tipe =  JRequest::getVar('tipe');
?>
<tr><td colspan="2">
</td>
</tr>
<tr><td colspan="2">
From: <?php echo $row->start_date; ?> to <?php echo $row->end_date; ?>
<span style="padding:0 0 0 20px;">
Contribution range: 
	 $<?php echo JRequest::getVar('from'); ?> to $<?php echo JRequest::getVar('to'); ?>
</a>
</span>
</td>
</tr>
<tr><td>
<span style="float:right;">
<button type="button" class="btn btn-primary btn-invite-reg-groups"
										id="btn" onclick="onGraph();"><i></i> <?php echo JText::_('Graph');?></button>
</span>
</td>
</tr>
                                                                        <?php } ?>



            </table></td>
                           </tr>
                           <tr>
                             <td>
                             <table class="table table-striped table-bordered">
																	<thead>
																		
																		<tr>
																			<th style="text-align:center;">Progress check</th>
																			<th style="text-align:center;">Total checks</th>
																			<th style="text-align:center;">Already checked</th>
																			<th style="text-align:center;">Not checked</th>
																			<th style="text-align:center;">Benefit awarded</th>
																			<th style="text-align:center;">Status</th>
                                                                            
																		</tr>																		
																	</thead>
																	<tbody>
								<?php foreach ($this->progress_checkes as $pc){ 
										$checkit = 0;
										foreach($this->listHistory as $ls){
										$tanggal = $ls->created_date;
											if (($this->tanggal > $row->start_date) && ($this->tanggal < $row->end_date)){
												$checkit = $checkit + 1;
												}				
											}	
										
										switch($pc->type){	
										case 'min':
											{
											$total_checks = (($pc->every * $this->selisih) * 24) * 60;
											$already_checked = $checkit;
											$not_checked = $total_checks - $already_checked;
												break;
											}
										case 'hour':
											{
											$total_checks = ($pc->every * $this->selisih) * 24;
											$already_checked = $checkit;
											$not_checked = $total_checks - $already_checked;

												break;
											}
										case 'day':
											{
											$total_checks = $pc->every * $this->selisih;
											$already_checked = $checkit;
											$not_checked = $total_checks - $already_checked;

											break;
											}
										}			
										
										
								?>									
								<tr>
									<td><?php echo 'Every ' . $pc->every. ' ' .  $pc->type; ?></td>
									<td><?php echo number_format($total_checks,0) ; ?> </td>									
									<td><?php echo $checkit;//JText::_( $already_checked ); ?> </td>
									<td><?php 
									echo number_format( $not_checked,0 ); ?> </td>
									<td><?php echo count($this->listHistory); ?> </td>
									<td style="text-align:center;"><?php 
									$datetime1 = new Datetime();
									$datetime2 = new Datetime($row->end_date);	
									echo ($datetime2 < $datetime1 ? 'Not Expired' : 'Expired'); ?></td>
                                    
								</tr>
								<?php }?>
							</tbody>
																</table></td>
                           </tr>
                         </table>
                       
					   
            

					  </div>
       </td>
       </tr>
       </table>
        		</div>

	</div>
</div>
			
</form>
