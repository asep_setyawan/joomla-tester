<?php
defined('_JEXEC') or die();
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
?>
<script type="text/javascript">
 
function onSelectFilter(){
	var filter = jQuery('#cbfilter').val();
	jQuery('#filter').val(filter);
	jQuery('form#adminForm').attr('action', 'index.php?option=com_awardpackage&view=uaccount');
	jQuery('form#adminForm').submit();	
}

function onFilter(){
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
	jQuery('form#adminForm').attr('action', 'index.php?option=com_awardpackage&view=uaccount&task=uaccount.getShoppingCredit&from='+from+'&to='+to);
	jQuery('form#adminForm').submit();	
}

</script>	
<form name="adminForm" action="index.php?option=com_awardpackage&view=uaccount&task=uaccount.getShoppingCredit" id="adminForm" method="post">

<div id="cj-wrapper">
<div class="container-fluid no-space-left no-space-right surveys-wrapper">
<div class="row-fluid">
			<table>
				<tr>
					<td valign="top">
						<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
					</td>
					<td valign="top">
						<div class="span12" style="margin:0;">
							<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo JText::_('Account'); ?>
								</h2>				
								
								<nav class="navigation" role="navigation">
                                <ul class="nav menu nav-pills">
								<li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getProfile");?>">Profile</a></li>
                               <li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getFunds");?>">Funds</a>	</li>
                                <li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getDonation");?>">Donation</a>	</li>
								<li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getAwardSymbol");?>">Award Symbol</a>	</li>                                
                                <li  class="active"><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getShoppingCredit");?>">Shopping Credit</a>	</li>
                                <li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getPrize");?>">Prize Claimed</a>		</li>	<br />
</ul>
</nav>                                             
							</div>
						</div>

<?php 
if (!empty($this->expired)) { 
echo '<div class="is-disabled">';
 }else{  
echo '<div class="span12" style="margin:0;">';
} ?>
<nav class="navigation" role="navigation">
                                <ul class="nav menu nav-pills">
								<li class="active"><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getShoppingCredit");?>">Personal</a></li>
                              <li><a href="<?php echo JRoute::_("index.php?option=com_awardpackage&view=uaccount&task=uaccount.getShoppingCreditBusiness");?>">Business</a></li>	
                              </ul>
                              </nav>	
<table class="table table-hover table-striped" width="100%" >                              
                                <tr><td>            
                                   <div class="control-label"> From :  <input type="text" name="from" id="from" alt="date" class="IP_calendar" title="Y-m-d">          To    <input type="text" name="to" id="to" alt="date" class="IP_calendar" title="Y-m-d">       
                                   <button type="button" class="btn btn-primary" onclick="onFilter();">Go</button>
								</div>
                                    </td>
                                    <td><?php echo $this->pagination->getLimitBox(); ?>
                                    </td>
    </tr>
    <tr>
										<td align="center" colspan="2">
											<div style="border: 1px solid #ccc; padding: 10px;">
												<b><?php echo 'Transaction List - '.$this->user->firstname.' '.$this->user->lastname.' '; ?></b>
											</div>
                                            </td>
</tr>
    <tr>
    <td colspan="2">
    
  
 <table class="table table-hover table-striped">
							<thead>
								<tr>
									<th width="20%"><?php echo JText::_( 'Date'); ?></th>									
									<th width="20%"><?php echo JText::_( 'Description'); ?></th>
									<th width="20%"><?php echo JText::_( 'Amount'); ?></th>
									<th width="20%"><?php echo JText::_( 'Total (excl locked)'); ?></th>
									<th width="20%"><?php echo JText::_( 'Total (incl locked)'); ?></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo JText::_( '20-07-2014'); ?></td>
									<td><?php echo JText::_( 'Shopping credits - earned and locked'); ?></td>
									<td><?php echo JText::_( '+$40'); ?></td>
									<td><?php echo JText::_( '$0'); ?></td>
									<td><?php echo JText::_( '+$40'); ?></td>
								</tr>
								<tr>
									<td><?php echo JText::_( '22-07-2014'); ?></td>
									<td><?php echo JText::_( 'Shopping credits - earned and unlocked'); ?></td>
									<td><?php echo JText::_( '+$10'); ?></td>
									<td><?php echo JText::_( '+$10'); ?></td>
									<td><?php echo JText::_( '+$50'); ?></td>
								</tr>
								<tr>
									<td><?php echo JText::_( '22-07-2014'); ?></td>
									<td><?php echo JText::_( 'Shopping credits - expired'); ?></td>
									<td><?php echo JText::_( '-$5'); ?></td>
									<td><?php echo JText::_( '+$5'); ?></td>
									<td><?php echo JText::_( '+$45'); ?></td>
								</tr>
								<tr>
									<td><?php echo JText::_( '30-07-2014'); ?></td>
									<td><?php echo JText::_( 'Shopping credits - spent - purchased goods from amazon'); ?></td>
									<td><?php echo JText::_( '-$10'); ?></td>
									<td><?php echo JText::_( '-$5'); ?></td>
									<td><?php echo JText::_( '+$35'); ?></td>
								</tr>
							</tbody>
						</table>
 
    </td>
    
    </tr>
    
    <tr><td colspan="2" style="text-align:right;" >                                    
                                   <div class="pagination">
    <?php //$pagthisination = new JPagination($total, $p_start, 2);
	
echo $this->pagination->getListFooter();
echo '<br/><br/>'. $this->pagination->getPagesCounter(); ?>
        </div>
                                    </td>
                                   
    </tr>
    </table>
</div>
</td>
</tr>
</table>	                     
									
</div>
</div>
</div>
</form>
