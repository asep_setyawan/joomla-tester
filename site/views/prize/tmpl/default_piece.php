<?php
defined('_JEXEC') or die();
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
?>
<script type="text/javascript">
function onBack(){
	window.location='http://localhost/awardnew/index.php?option=com_awardpackage&view=prize&task=prize.getMainPage';
}
</script>
<div id="cj-wrapper">
	<div class="container-fluid no-space-left no-space-right surveys-wrapper">
		<div class="row-fluid">
			<table width="100%">
				<tr>
					<td valign="top" width="150px;">
						<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'main_header.php';?>
					</td>
					<td valign="top">
						<div class="well">
								<h2 class="page-header margin-bottom-10 no-space-top">
									<?php echo JText::_('Award symbol pieces'); ?>
                                <span style="float:right;"		>
<button type="button" class="btn btn-primary btn-invite-reg-groups"
										id="btn" onclick="onBack();"><i></i> <?php echo JText::_('Back');?></button>
                                        </span>

								</h2>	
							</div>

					
						<table class="table table-hover table-striped table-bordered" style="width:60%">
<thead>
	<tr>
		<th valign="top" style="text-align:center"><?php echo JText::_('Prize'); ?></th>
		<th valign="top" style="text-align:center"><?php echo JText::_('Prize value'); ?></th>
		<th valign="top" style="text-align:center"><?php echo JText::_('Award symbol set'); ?></th>
	</tr>
</thead>
<tbody>
 <?php foreach ($this->symbolPrizesId as $row):
						?>
	<tr>
		<td> <img
										src="<?php echo PRIZE_IMAGES_URI . $row->prize_image; ?>"
										style="width: 150px;" /></td>
		<td style="text-align:center"><?php echo '$'.$row->prize_value;?></td>
		<td><img
										src="<?php echo SYMBOL_IMAGES_URI . $row->symbol_image; ?>"
										style="width: 150px;padding:10px 0;" /></td>
	</tr>
							<?php endforeach;?>
</tbody>
</table>


<table class="table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th valign="top" style="text-align:center"><?php echo JText::_('No'); ?></th>
					<th valign="top" style="text-align:center"><?php echo JText::_('Award symbol piece'); ?></th>
					<th valign="top" style="text-align:center"><?php echo JText::_('Quantity'); ?></th>
					<th valign="top" style="text-align:center"><?php echo JText::_('Total price'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php 
	for ($k=0, $n=$this->jml; $k < $n; $k++){
		$row =& $this->SymbolList[$k];
		$j=$k+1;
												?>
				<tr>
					<td class="hidden-phone" style="text-align:center"><?php echo $j; ?></td>					
					<td class="hidden-phone" style="text-align:center"><?php 
									

											$filename = $row->symbol_pieces_image;//substr($row->symbol_image,0,strlen( $row->symbol_image) - 4).$rownya.$colnya.".png";
											$file = SYMBOL_IMAGES_PIECES_URI .$filename;
											echo '<a href="index.php?option=com_awardpackage&view=prize&task=prize.getPieceQuantity&urut='.$k.'&symbolid='.$row->symbol_pieces_id.'&id='.$this->id.'&jml='.$this->jml.'" target="_blank">';
											echo '<img id="image'.$i.'" style="padding:3px; width: 50px;" alt="" src="'.$file.'?timestamp='.time().'"/>';
											echo '</a>';
											 ?></td>
					<td class="hidden-phone" style="text-align:center"><?php
echo '<a href="index.php?option=com_awardpackage&view=prize&task=prize.getPieceQuantity&urut='.$k.'&symbolid='.$row->symbol_pieces_id.'&id='.$this->id.'&jml='.$this->jml.'" target="_blank">';
					echo count($row->symbol_pieces_id); 
					echo '</a>';
					?></td>
					<td class="hidden-phone" style="text-align:center"><?php echo '$'.number_format($this->amount,0); ?></td>
				</tr>
				<?php }
				//}
			 ?>
			</tbody>
			
		</table>
		
						</div>
					</td>
				</tr>
			</table>						
		</div>
	</div>
</div>
