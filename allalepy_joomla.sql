-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2016 at 07:00 PM
-- Server version: 5.6.23-72.1-log
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `allalepy_joomla`
--

-- --------------------------------------------------------

--
-- Table structure for table `yoz6q_ap_free_giftcode_groups`
--

CREATE TABLE IF NOT EXISTS `yoz6q_ap_free_giftcode_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `package_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `yoz6q_ap_free_giftcode_groups`
--

INSERT INTO `yoz6q_ap_free_giftcode_groups` (`id`, `title`, `package_id`, `created`, `modified`) VALUES
(36, 'new bannner', 8, '2016-02-12 04:32:00', '2016-02-12 04:32:01'),
(37, 'my custom group01', 8, '2016-02-12 04:32:53', '2016-02-12 04:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `yoz6q_ap_free_giftcode_rules`
--

CREATE TABLE IF NOT EXISTS `yoz6q_ap_free_giftcode_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `yoz6q_ap_free_giftcode_rules`
--

INSERT INTO `yoz6q_ap_free_giftcode_rules` (`id`, `package_id`, `title`, `created`, `modified`) VALUES
(1, 6, 'Banner2012', '0000-00-00 00:00:00', '2016-02-10 05:32:00'),
(3, 6, 'new rule 2017', '2016-02-10 06:32:46', '2016-02-10 06:32:30'),
(4, 6, 'custom rule', '2016-02-10 06:32:48', '0000-00-00 00:00:00'),
(11, 9, 'Test Rule1', '2016-02-12 18:02:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `yoz6q_ap_free_giftcode_rules_settings`
--

CREATE TABLE IF NOT EXISTS `yoz6q_ap_free_giftcode_rules_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `data_type` enum('day','date') NOT NULL,
  `data_value` varchar(255) NOT NULL,
  `giftcodes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `yoz6q_ap_free_giftcode_rules_settings`
--

INSERT INTO `yoz6q_ap_free_giftcode_rules_settings` (`id`, `rule_id`, `data_type`, `data_value`, `giftcodes`) VALUES
(43, 1, 'day', '["Saturday"]', '["Green,2"]'),
(44, 1, 'day', '["Sunday"]', '["Red,3"]'),
(45, 1, 'date', '["24 February"]', '["Blue,2"]'),
(46, 1, 'date', '["02 March"]', '["Dark Blue,2"]'),
(49, 3, 'day', '["Saturday"]', '["Dark Blue,3"]'),
(50, 3, 'date', '["29 February"]', '["Yellow,2"]'),
(51, 4, 'day', '["Sunday"]', '["Orange,5"]'),
(60, 11, 'day', '["Friday"]', '["Green,5"]'),
(61, 11, 'date', '["23 February"]', '["Green,2"]');

-- --------------------------------------------------------

--
-- Table structure for table `yoz6q_ap_free_giftcode_usergroup`
--

CREATE TABLE IF NOT EXISTS `yoz6q_ap_free_giftcode_usergroup` (
  `criteria_id` int(20) NOT NULL AUTO_INCREMENT,
  `package_id` int(20) NOT NULL DEFAULT '0',
  `population` tinyint(3) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `from_age` tinyint(3) NOT NULL,
  `to_age` tinyint(3) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `street` varchar(150) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(50) NOT NULL,
  `post_code` varchar(10) NOT NULL,
  `country` varchar(50) NOT NULL,
  `field` varchar(30) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '1',
  `free_usergroup_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `group_name` varchar(250) DEFAULT NULL,
  `is_presentation` tinyint(1) DEFAULT NULL,
  `var_id` int(11) NOT NULL,
  `parent_usergroup` bigint(20) DEFAULT NULL,
  `usergroup_id` int(11) NOT NULL,
  `useraccount_id` int(11) NOT NULL,
  `filter` varchar(255) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`criteria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `yoz6q_ap_free_giftcode_usergroup`
--

INSERT INTO `yoz6q_ap_free_giftcode_usergroup` (`criteria_id`, `package_id`, `population`, `firstname`, `lastname`, `birthday`, `email`, `from_age`, `to_age`, `gender`, `street`, `city`, `state`, `post_code`, `country`, `field`, `group_id`, `free_usergroup_id`, `status`, `group_name`, `is_presentation`, `var_id`, `parent_usergroup`, `usergroup_id`, `useraccount_id`, `filter`, `created_time`) VALUES
(23, 8, 100, '', '', NULL, '', 0, 0, 'M', '', '', '', '', '', 'gender', 1, NULL, 1, '', 1, 2, NULL, 0, 0, '', '2016-02-12 09:14:48'),
(24, 8, 100, '', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 2, NULL, 0, 0, '', '2016-02-12 09:14:48'),
(35, 8, 100, 'prince', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, 36, 1, '', 1, 3, NULL, 0, 0, '', '2016-02-12 09:19:00'),
(36, 8, 100, '', '', NULL, '', 12, 0, '', '', '', '', '', '', 'age', 1, 37, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 09:19:53'),
(37, 8, 100, '', '', NULL, '', 0, 0, 'F', '', '', '', '', '', 'gender', 1, 37, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 09:19:53'),
(38, 8, 100, '', '', NULL, '', 0, 0, '', '', '', '', '343', 'Afghanistan', 'location', 1, 37, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 09:19:53'),
(40, 8, 100, 'prince', 'suneja', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, 37, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 09:19:53'),
(41, 8, 100, 'nagesh', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 0, NULL, 0, 0, '', '2016-02-12 09:40:45'),
(42, 8, 100, 'nagesh', 'suneja', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 0, NULL, 0, 0, '', '2016-02-12 09:40:54'),
(43, 8, 100, 'nagesh', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 09:50:44'),
(46, 8, 100, '', '', NULL, '', 0, 0, 'M', '', '', '', '', '', 'gender', 1, NULL, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 09:58:21'),
(47, 8, 100, '', '', NULL, '', 45, 0, '', '', '', '', '', '', 'age', 1, NULL, 1, '', 1, 3, NULL, 0, 0, '', '2016-02-12 09:58:51'),
(48, 8, 100, '', '', NULL, 'sunejaprince@gmail.com', 0, 0, '', '', '', '', '', '', 'email', 1, NULL, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 11:43:05'),
(49, 8, 100, '', '', NULL, 'sunejaprince1@gmail.com', 0, 0, '', '', '', '', '', '', 'email', 1, NULL, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 11:55:11'),
(50, 8, 100, '', '', NULL, 'sunejaprince5@gmail.com', 0, 0, '', '', '', '', '', '', 'email', 1, NULL, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 11:57:33'),
(51, 8, 100, 'prince', 'suneja1', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 4, NULL, 0, 0, '', '2016-02-12 11:58:18'),
(53, 8, 100, 'nagesh', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 3, NULL, 0, 0, '', '2016-02-12 12:00:59'),
(54, 8, 100, 'prince', 'aaa', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 3, NULL, 0, 0, '', '2016-02-12 12:01:30'),
(56, 9, 100, 'test fname', 'test lname', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 1, NULL, 0, 0, '', '2016-02-12 18:46:21'),
(57, 9, 100, 'test fname', 'test lname', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 2, NULL, 0, 0, '', '2016-02-12 18:48:13'),
(58, 8, 100, 'test fname', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 5, NULL, 0, 0, '', '2016-02-12 18:49:29'),
(59, 8, 100, '', 'test lname', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 6, NULL, 0, 0, '', '2016-02-12 18:52:02'),
(60, 8, 100, 'test', '', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 7, NULL, 0, 0, '', '2016-02-12 18:54:16'),
(61, 8, 100, 'test', 'tedt', NULL, '', 0, 0, '', '', '', '', '', '', 'name', 1, NULL, 1, '', 1, 7, NULL, 0, 0, '', '2016-02-12 18:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `yoz6q_ap_free_gift_code_rewards_list`
--

CREATE TABLE IF NOT EXISTS `yoz6q_ap_free_gift_code_rewards_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT ' 1 for published, 2 for unpublish,3 for expired,',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `yoz6q_ap_free_gift_code_rewards_list`
--

INSERT INTO `yoz6q_ap_free_gift_code_rewards_list` (`id`, `package_id`, `start_date`, `end_date`, `user_group_id`, `rule_id`, `created`, `modified`, `status`) VALUES
(5, 7, '2016-02-20', '2016-02-27', 1, 5, '2016-02-12 07:02:34', '2016-02-12 06:18:34', '1'),
(8, 9, '2016-02-14', '2016-02-19', 38, 7, '2016-02-12 17:02:08', '2016-02-12 17:39:08', '1');

-- --------------------------------------------------------

--
-- Table structure for table `yoz6q_ap_useraccounts`
--

CREATE TABLE IF NOT EXISTS `yoz6q_ap_useraccounts` (
  `ap_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `paypal_account` varchar(100) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_presentation` tinyint(1) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `free_usergroup_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ap_account_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `yoz6q_ap_useraccounts`
--

INSERT INTO `yoz6q_ap_useraccounts` (`ap_account_id`, `id`, `firstname`, `lastname`, `birthday`, `gender`, `street`, `city`, `state`, `post_code`, `country`, `phone`, `paypal_account`, `package_id`, `email`, `is_active`, `is_presentation`, `category_id`, `free_usergroup_id`) VALUES
(1, 763, 'jatin', 'kumar', '0000-00-00', 'M', '', NULL, '123456', NULL, 'Afghanistan', '', 'suneja@gmail.com', 8, 'jatin@gmail.com', 1, NULL, NULL, 37),
(2, 764, 'kashish', 'kumar', '0000-00-00', 'M', '', NULL, '123456', NULL, 'Afghanistan', '', 'rajan@pal.com', 8, 'kashish@gmail.com', 1, NULL, NULL, 7),
(3, 765, 'rajesh', 'kumar', '0000-00-00', 'M', '', NULL, '123456', NULL, 'Afghanistan', '', 'varunsuneja@ymail.com', 8, 'rajesh@gmail.com', 1, NULL, NULL, 7);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
